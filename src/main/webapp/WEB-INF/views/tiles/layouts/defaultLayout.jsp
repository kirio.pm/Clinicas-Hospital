<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="custom_css" />
<tiles:importAttribute name="custom_js" />



<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Clinica">
<meta name="author" content="BAD115">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>

<title><tiles:getAsString name="title" /></title>
<c:forEach var="css" items="${stylesheets}">
	<link rel="stylesheet" type="text/css"
		href='<c:url value="${css}"></c:url>'>
</c:forEach>

<c:forEach var="css" items="${custom_css}">
	<link rel="stylesheet" type="text/css"
		href='<c:url value="${css}"></c:url>'>
</c:forEach>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

	<header id="header">
		<tiles:insertAttribute name="header" />
	</header>

	<section id="page_content">
		<tiles:insertAttribute name="body" />
	</section>

	<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Desea salir del sistema?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">presione el boton salir para finalizar sesion sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href='<c:url value="/logout" />'>Salir</a>
          </div>
        </div>
      </div>
    </div>

	<c:forEach var="script" items="${javascripts}">
		<script type="text/javascript" src='<c:url value="${script}"></c:url>'></script>
	</c:forEach>

	<c:forEach var="script" items="${custom_js}">
		<script type="text/javascript" src='<c:url value="${script}"></c:url>'></script>
	</c:forEach>


</body>

</html>