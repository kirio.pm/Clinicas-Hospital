
<!--  
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="sidemenu">
	<ul id="sidemenu-ul" class="sidebar-nav navbar-nav">
	
	
		<li><a href='<c:url value="/" />'><i class="fa fa-home"></i> Inicio</a></li>
		
		
		<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
			<li><a href='<c:url value="/user/" />'><i class="fa fa-users">
			</i> Gestión de personal de ventanilla</a></li>
		</sec:authorize>
		
		
		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY')">
			<li><a href='<c:url value="/request/document-review" />'><i class="fa fa-folder">
			</i> Solicitudes de permisos sanitarios</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY')">
			<li><a href='<c:url value="/procedure/" />'><i
					class="fa fa-table fa-fw"></i> Gestión de Tramite y Control</a></li>
		</sec:authorize>
	

		<sec:authorize access="hasRole('JURIDICAL') or hasRole('SECRETARY')">	
			<li><a href='<c:url value="/legal"/>'><i class="fa fa-university">
			</i>Solicitudes area Juridica</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('JURIDICAL') or hasRole('SECRETARY')">	
			<li><a href='<c:url value="/legalVehicle"/>'><i class="fa fa-university">
			</i>Solicitudes area Juridica para Vehiculos</a></li>
		</sec:authorize>

		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY') or hasRole('COORDINATOR')">
			<li class="nav-item"><a class="nav-link" href='<c:url value="/document-control/" />'>
			<i class="fa fa-clone"></i>Nueva hoja de revisión de requerimientos</a></li>
		</sec:authorize>



		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY') or hasRole('COORDINATOR')">
			<li class="nav-item"><a class="nav-link" href='<c:url value="/document-control/control-document-completed-list" />'>
			<i class="fa fa-file-text"></i>Revisión previa de la documentación</a></li>
		</sec:authorize>


		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY') or hasRole('COORDINATOR')">
			<li class="nav-item"><a class="nav-link" href='<c:url value="/casefile/" />'>
			<i class="fa fa-building"></i>Expedientes del área de establecimientos</a>
		</sec:authorize>

		<sec:authorize access="hasRole('COORDINATOR') or hasRole('TECHNICAL') or hasRole('SECRETARY')">
			<li class="nav-item"><a href='<c:url value="/establishment" />'>
			<i class="fa fa-industry" aria-hidden="true"></i>Establecimientos</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('COORDINATOR')">
			<li class="nav-item"><a href='<c:url value="/coordinator/resolutions/pending-list" />'>
			<i class="fa fa-tasks" aria-hidden="true"></i>Resoluciónes de dictamen jurídico</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('COORDINATOR') or hasRole('TECHNICAL') or hasRole('SECRETARY')">
			<li class="nav-item"><a href='<c:url value="/payment-procedure/pending-creation" />'>
			<i class="fa fa-credit-card" aria-hidden="true"></i>Mandamientos de pago</a></li>
		</sec:authorize>

		<sec:authorize access="hasRole('COORDINATOR')">
			<li class = "nav-item"><a class="nav-link" href='<c:url value="/sibasi/" />'>
			<i class="fa fa-square"></i>SIBASI</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('COORDINATOR')">
			<li class = "nav-item"><a class="nav-link" href='<c:url value="/coordinator/resolutions/pending-list" />'>
			<i class="fa fa-gavel" aria-hidden="true"></i>Resoluciones de dictamen</a></li>
		</sec:authorize>
		

		<sec:authorize access="hasRole('COORDINATOR')">
			<li class="nav-item"><a href='<c:url value="/document-configuration/" />'> 
			<i class="fa fa-clipboard"></i>Configuración de hojas de requerimiento</a></li>
		</sec:authorize>
		

		<sec:authorize access="hasRole('JURIDICAL')">
			<li class="nav-item"><a href='<c:url value="/opinion/" />'> 
			<i class="fa fa-clipboard"></i>Opinion Juridica</a></li>
		</sec:authorize>

		<sec:authorize access="hasRole('TECHNICAL') or hasRole('SECRETARY') or hasRole('COORDINATOR')">
			<li class="nav-item"><a class="nav-link" href='<c:url value="/tracing/" />'>
			<i class="fa fa-building"></i>Seguimiento</a>
		</sec:authorize>
		
		<sec:authorize access="hasRole('COORDINATOR')">
			<li class="nav-item"><a href='<c:url value="/email/" />'> 
			<i class="fa fa-clipboard"></i>envio de correos</a></li>
		</sec:authorize>
		


		<li class="nav-item"><a class="nav-link" href='<c:url value="/help" />'>
		<i class="fa fa-info-circle"></i>Ayuda</a></li>
		
		
	</ul>
</div>
-->