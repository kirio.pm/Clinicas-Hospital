<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html">SISMEFRON</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-address-card"></i>
            <span class="nav-link-text">Gestion Expedientes</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <sec:authorize access="hasRole('SECRETARY') or hasRole('ADMIN') or hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('NURSE')">
            <li>
              <a href="<c:url value='/expediente/' />">Listar Expedientes</a>
            </li>
            </sec:authorize>
             </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#paciente" data-parent="#exampleAccordion">
            <i class="fa fa-users"></i>
            <span class="nav-link-text">Gestion Pacientes</span>
          </a>
          <ul class="sidenav-second-level collapse" id="paciente">
            <sec:authorize access="hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('NURSE')">
            <li>
              <a href="<c:url value='/paciente/' />">Listar Pacientes</a>
            </li>
            </sec:authorize>
            <sec:authorize access="hasRole('RECEPCTION')">
            <li>
              <a href="<c:url value='/paciente/nuevoPaciente' />">Crear Paciente </a>
            </li>
            </sec:authorize>
            <sec:authorize access="hasRole('RECEPCTION')">
            <li>
              <a href="<c:url value='/hclinico/' />">Historiales Clinicos</a>
            </li>
            </sec:authorize>
          </ul>
        </li>
        <sec:authorize access="hasRole('RECEPCTION') or hasRole('SECRETARY')">	
       <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">Gestion Citas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
          
            <li>
              <a href="<c:url value='/cita/' />">Citas Medicas</a>
            </li>
            
          </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('RECEPCTION') or hasRole('NURSE') or hasRole('DOCTOR')">
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#consul" data-parent="#exampleAccordion">
            <i class="fa fa-users"></i>
            <span class="nav-link-text">Gestion Consultas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="consul">
            
            <li>
              <a href="<c:url value='/consultaMedica/' />">Listar Consultas Medicas</a>
            </li>
             <li>
              <a href="<c:url value='/signosVitales/' />">Listar Signos Vitales</a>
            </li>
             <li>
              <a href="<c:url value='/Sintomatologia/' />">Listar Sintomas</a>
            </li>
            <li>
              <a href="<c:url value='/diagnostico/' />">Listar Diagnostico</a>
            </li>
            
            
            </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('RECEPCTION') or hasRole('NURSE') or hasRole('DOCTOR')">	
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Hp" data-parent="#exampleAccordion">
            <i class="fa fa-hospital-o"></i>
            <span class="nav-link-text">Hospitalizaciones</span>
          </a>
          <ul class="sidenav-second-level collapse" id="Hp">
            
            <li>
              <a href="<c:url value='/hospitalizacion/' />">Listar Hispitalizaciones</a>
            </li>
            
         </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('RECEPCTION') or hasRole('NURSE') or hasRole('DOCTOR')">
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Cirugia" data-parent="#exampleAccordion">
            <i class="fa fa-ambulance"></i>
            <span class="nav-link-text">Cirugias</span>
          </a>
          <ul class="sidenav-second-level collapse" id="Cirugia">
            
            <li>
              <a href="<c:url value='/cirugia/nuevaCirugia' />">Crear Cirugia</a>
            </li>
            <li>
              <a href="<c:url value='/cirugia/' />">Listar Cirugias</a>
            </li>
            
         </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN')">
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#doctores" data-parent="#exampleAccordion">
            <i class="fa fa-users"></i>
            <span class="nav-link-text">Gestion Doctores</span>
          </a>
          <ul class="sidenav-second-level collapse" id="doctores">
            
            <li>
              <a href="<c:url value='/doctor/' />">Listar Doctores</a>
            </li>
            <li>
              <a href="<c:url value='/usuarios/nuevoUsuario' />">Crear Usuario</a>
            </li>
            
          </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN')">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-circle"></i>
            <span class="nav-link-text">Gestion Clinica</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            
            <li>
              <a href="<c:url value='/empleados/' />">Listar Empleados</a>
            </li>
            <li>
              <a href="<c:url value='/usuarios/' />">Listar Usuarios</a>
            </li>
            <li>
              <a href="<c:url value='/hospital/' />">Listar Hospital</a>
            </li>
            <li>
              <a href="<c:url value='/Clinica/' />">Listar Clinica</a>
            </li>
            <li>
              <a href="<c:url value='/usuarios/' />">Listar Usuarios</a>
            </li>
            
          </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN')">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Mantenimientos" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-circle"></i>
            <span class="nav-link-text">Soporte Clinica</span>
          </a>
          <ul class="sidenav-second-level collapse" id="Mantenimientos">
              <li>
              <a href="<c:url value='/empleados/nuevoEmpleados' />">Crear Empleado</a>
            </li>
            <li>
              <a href="<c:url value='/usuarios/nuevoUsuario' />">Crear Usuario</a>
            </li>
	        <li>
              <a href="<c:url value='/hospital/nuevoHospital' />">Crear Hospital</a>
            </li>
            <li>
              <a href="<c:url value='/Clinica/nuevaClinica' />">Crear Clinica</a>
            </li> 
            <li>
              <a href="<c:url value='/sala/nuevaSala' />">Crear Sala</a>
            </li>      
	        
         </ul>
        </li>
          </sec:authorize>
         	
        <sec:authorize access="hasRole('ADMIN')">
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#log" data-parent="#exampleAccordion">
            <i class="fa fa-users"></i>
            <span class="nav-link-text">Log del Sistema</span>
          </a>
          <ul class="sidenav-second-level collapse" id="log">
            
            <li>
              <a href="<c:url value='/log/' />">Log</a>
            </li>
            
           </ul>
        </li>
        </sec:authorize>	
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Cerrar Sesion</a>
        </li>
      </ul>
    </div>
  </nav>