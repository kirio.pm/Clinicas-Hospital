<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="medicamento">
					
					<div class="card">
						<h3 class="card-header">Nuevo Medicamento</h3>
						<div class="card-block" style="magin:5px;">
							<div class="col-md-8">
								<label for="example-text-input" class=" col-form-label">Docis:</label>								
								<form:input class="form-control" type="text" path="docisMed" />																							
							</div>							
															
							<div class="row col-md-8">
								<label for="example-text-input" class="col-form-label">Catalogo de medicamento:</label>								
								<form:select class="form-control" path="idCatMedi.idCatMedi">
								 	<!--<form:options  itemLabel="correlativoCon" items="${listaCon}" itemValue="idConsulta" />-->
								 	<c:forEach items="${listaCMed}" var="item">
								 		<option value="${item.idCatMedi}">
								 		${item.nombreMedi} -- $${item.costoMed}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>	
								
							
							
							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/medicamento/' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>