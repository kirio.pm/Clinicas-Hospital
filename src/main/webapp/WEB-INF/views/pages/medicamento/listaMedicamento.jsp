<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista de Medicamentos Recetados</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						<th>Docis</th>
						<th>Medicamento</th>
						<th>Precio</th>
						<th>Tratamiento</th>
						<th>Frecuencia</th>
						<th>Acciones</th>						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="medi">
					<tr>
						<td align="center">${medi.docisMed}</td>
						<td>${medi.idCatMedi.nombreMedi}</td>
						<td>${medi.idCatMedi.costoMed}</td>
						<td>${medi.idTratamiento.tipoTra}</td>
						<td>${medi.idTratamiento.frecuenciaTra}</td>						
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/medicamento/editarMed-${medi.idMedicamento}' />">
								<i class="fa fa-edit"></i>
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-primary" href="<c:url value='/medicamento/nuevoMedicamento' />">
				Nuevo Medicamento
				</a>
				<a class="btn btn-success" href="<c:url value='/tratamiento/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
