<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h5 class="card-header">Historial Clinico</h5>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-bordered" width="100%" id="dataTable"	cellspacing="0">
								<thead>
									<tr>										
										<th>Paciente</th>
										<th>Genero </th>
										<th>Enfermedades Padecidas</th>
										<th>Tratamiento Usados</th>
										<th> </th>
										<th>Nombres Responsable</th>
										<th>Histotial Enfermedades</th>
																			
									</tr>
								</thead>
								<tbody>
								  <c:forEach items="${historiales}" var="historiales">
									<tr>
										<td align="center">${historiales.idPaciente.nombrePac} ${historiales.idPaciente.apellidoPac} </td>
										<td align="center">${historiales.idPaciente.generoPac}</td>
										<td align="center">${historiales.enfermedadesPadecidas}</td>
										<td align="center">${historiales.tratamientoPreventivo}</td>
										<td align="center"></td>
										
										<td align="center">${historiales.idDatosFamiliares.nombrePadre}</td>
										<td align="center">${historiales.idDatosFamiliares.historialDeEnfermedades}</td>
																																				
									</tr>
									</c:forEach>
								</tbody>								
								
							</table>
						</div><!-- table responsive -->
						<div class="box-footer" style="margin:5px;">
							<a class="btn btn-primary" href="<c:url value='/paciente/nuevoPaciente' />">Nuevo Paciente </a>
							<a class="btn btn-secondary" href="<c:url value='/index/' />">Regresar </a>			
						</div>		
					</div><!-- card block -->
				</div><!-- card  -->
			</div><!-- col -->
		</div> <!-- row -->
	</div><!-- fluid -->
</div> <!-- content -->