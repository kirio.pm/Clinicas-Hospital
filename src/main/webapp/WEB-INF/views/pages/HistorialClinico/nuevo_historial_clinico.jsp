<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="historial">			
					<div class="card">
						<h3 class="card-header">Nuevo Historial clinico</h3>
						<div class="card-block">
							
							<div class="form-group">
								
							<br>
							<div class="form-row">
								<div class="col-md-12" class="row" style="padding-left: 500px;">
									<div class="col-10">							
									<!-- form:hidden  path="idPaciente"   Value="${px}"/-->
									<form:label  path="idPaciente"   Value="${px}"/>
									</div>	

								</div>
								</div><!-- form row -->
						</div><!-- form gruop-->
							
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Nombres Padre o Madre:</label>
									<form:input class="form-control" type="text" path="idDatosFamiliares.nombrePadre" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Apellido Padre o Madre:</label>
									<form:input class="form-control" type="text" path="idDatosFamiliares.apellidoPadre" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Dirreccion Padres:</label>
									<form:input class="form-control" type="text" path="idDatosFamiliares.direccion" />
								</div>
								<div class="col-md-6">
									<label for="direccion">FechaNacimiento Padre o Madre:</label>
									<form:input class="date form-control" type="text" path="idDatosFamiliares.fechaNacimiento" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Enfermedades padecidas(Pacientes)</label>
									<form:input class="form-control" type="text" path="enfermedadesPadecidas" />
								</div>
								<div class="col-md-6">
									<label for="nombre">Tratamientos Usados</label>
									<form:input class="form-control" type="text" path="tratamientoPreventivo" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						
						<br>
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-12">
									<label for="nombre">Historial Enfermedades (Padres):</label>
									<form:input class="form-control" type="text" path="idDatosFamiliares.historialDeEnfermedades" />
								</div>
							</div><!-- form row --> 
							<br>
							
							<div class="row" >
								<div class="col-md-6 center-block">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/' />">Cancelar</a>
								</div>
							</div>
						</div><!-- form gruop-->
																			
							</div>	
							</div>
							</div>					
					
				</form:form>


			</div>
		</div>
	</div>
</div>
