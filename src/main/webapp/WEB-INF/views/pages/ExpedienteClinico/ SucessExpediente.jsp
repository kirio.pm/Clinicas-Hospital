<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="page-content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Centro de mensajeria APS</h3>
					<div class="card-block">
	      
	       <div id="successMessage">
	        <strong>Se ha registrado el empleado</strong>
	    </div>
	    <div id="emailFormDiv">
	    	<a id="emailFormPage" href="<c:url value='/employee/' />">Regresar </a>
	    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
l>
