<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Expedientes</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						<th>Codigo Expediente</th>
						<th>Nombre </th>
						<th>Apellido </th>
						<th>Genero </th>
						<th>E-mail</th>						
						<th>Acciones</th>
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${Lista}" var="expediente">
					<tr>
						<td align="center">${expediente.codExpediente}</td>
						<td>${expediente.idPaciente.nombrePac}</td>
						<td>${expediente.idPaciente.apellidoPac}</td>
						<td>${expediente.idPaciente.generoPac}</td>
						<td>${expediente.idPaciente.emailPac}</td>
						<td align="center"><sec:authorize
											access="hasRole('RECEPCTION') or hasRole('SECRETARY') or hasRole('ADMIN') or hasRole('DOCTOR')">
							<a class="btn btn-secondary" data-toggle="tooltip" title="Ver"  href="<c:url value='/expediente/detalles-${expediente.idExpediente}' />">
								<i class="fa fa-eye"></i>
							</a>
							</sec:authorize>
							<sec:authorize
											access="hasRole('RECEPCTION') or hasRole('DOCTOR')">
							<a class="btn btn-secondary" data-toggle="tooltip" title="Crear Consulta" href="<c:url value='/consultaMedica/nuevaConsulta/${expediente.idExpediente}' />">
							<i class="fa fa-user-md" aria-hidden="true"></i>
							</a>
							</sec:authorize>
						</td>
						
					
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		
					</div>
					<div class="box-footer" style="margin:5px;">
							<a class="btn btn-primary" href="<c:url value='/paciente/nuevoPaciente' />">Nuevo Paciente </a>
							<a class="btn btn-secondary" href="<c:url value='/index/' />">Regresar </a>			
						</div>
				</div>
			</div>
		</div>
    </div>
</div>
