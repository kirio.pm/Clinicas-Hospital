<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="GET" modelAttribute="ExpedienteUnico">
				<div class="card">
					<h5 class="card-header">Detalle Expediente</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Codigo Expediente</label>
								<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="codExpediente" />	
								
								</div>
								<div class="col-md-6">
									<label for="direccion">Fecha Creacion</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="fechaCreacion" />
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
				</div>
				
				
				<div class="card">
				<h8 class="card-header"> Paciente</h8		>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Nombre Paciente</label>
									<form:input class="form-control" id="empresa" type="text" aria-describedby="empresa" value="" placeholder="" disabled="true" required="true" path="idPaciente.nombrePac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Apellido Paciente</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idPaciente.apellidoPac" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="direccion">Genero</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idPaciente.generoPac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Estado Civil</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idPaciente.generoPac" />
								</div>
								
							</div>
						</div>
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Direccion</label>
									<form:input class="form-control" id="empresa" type="text" aria-describedby="empresa" value="" placeholder="" disabled="true" required="true" path="idPaciente.direccionPac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Telefono</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idPaciente.telefonoPac" />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="telefono">Resonsable Por Emergencia</label>
									<form:input class="form-control" id="telefono" type="text" aria-describedby="telefono" value="" placeholder="" disabled="true" required="true" path="idPaciente.responsablePorEmergenciaPac" />
								</div>
								<div class="col-md-6">
									<label for="correo">Correo Electronico</label>
									<form:input class="form-control" id="correo" type="email" aria-describedby="correo" value="" placeholder="" disabled="true" required="true" path="idPaciente.emailPac" />
								</div>
							</div>
						</div>
						                            
						</div>
						</div>
					</div>
				</div>
				
				<div class="box-footer" style="margin:5px;">
							<a class="btn btn-secondary" href="<c:url value='/index/' />">Regresar </a>			
						</div>	
				</form:form>
			</div>
		</div>
    </div>
</div>
