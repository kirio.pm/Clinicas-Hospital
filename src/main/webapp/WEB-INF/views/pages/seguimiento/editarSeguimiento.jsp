<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="segUnico">
				<div class="card">
					<h5 class="card-header">Editar Seguimiento Post Cirugia</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">									
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idSeguimiento" />								
								</div>							
								
								<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Seguimiento</label>								
								<form:textarea class="form-control" type="text" path="tipoSeguimiento" />																							
							</div>
							
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Paciente:</label>								
								<form:select class="form-control" path="idCirugia.idCirugia">
								 	<c:forEach items="${listaCir}" var="item">
								 		<option value="${item.idCirugia}">
								 		${item.idCatCiru.nombreCiru}: ${item.idCatCiru.procediemientoCiru} $${item.idCatCiru.costoCirugia}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
								
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/seguimiento/' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>
