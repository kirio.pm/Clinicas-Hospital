<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="seguimiento">
					
					<div class="card">
						<h3 class="card-header">Seguimiento Post Cirugia</h3>
						<div class="card-block" style="magin:5px;">
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Seguimiento</label>								
								<form:textarea class="form-control" type="text" path="tipoSeguimiento" />																							
							</div>
							
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Paciente:</label>								
								<form:select class="form-control" path="idCirugia.idCirugia">
								 	<c:forEach items="${listaCir}" var="item">
								 		<option value="${item.idCirugia}">
								 		${item.idCatCiru.nombreCiru}: ${item.idCatCiru.procediemientoCiru} $${item.idCatCiru.costoCirugia}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/seguimiento/' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>