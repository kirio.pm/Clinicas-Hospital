<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="clinica">
					
					<div class="card">
						<h3 class="card-header">Nueva Clinica</h3>
						<div class="card-block" style="margin: 5px;">
							<div class=" row col-md-8">
								<label for="example-text-input" class="col-form-label">Nombre Clinca:</label>								
								<form:input class="form-control"  type="text" path="nombreCli"/>																							
							</div>
							<div class=" row col-md-8">
								<label for="example-text-input" class="col-form-label">Direccion Clinica</label>								
								<form:input class="form-control" type="text" path="direccionCli"/>																							
							</div>
							<div class=" row col-md-8">
								<label for="example-text-input" class="col-form-label">Telefono</label>								
								<form:input class="form-control" type="text" path="telelfonoCli"/>																							
							</div>
							<div class="row col-md-8">
								<label for="example-text-input" class="col-form-label">Region</label>								
								<form:select class="form-control" path="idRegion.idRegion">
								 	<form:options  itemLabel="pais" items="${listaReg}" itemValue="idRegion" />
								</form:select>																					
							</div>							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/Clinica/listaClinica' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>