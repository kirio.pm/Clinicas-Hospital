<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h5 class="card-header">Datos Familiares<h9>(Padre o Madre)</h9></h5>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-bordered" width="100%" id="dataTable"	cellspacing="0">
								<thead>
									<tr>										
										<th>Nombres </th>
										<th>Apellido </th>
										<th>Dirreccion </th>
										<th>FechaNacimiento</th>
										<th>Fecha Nacimiento</th>
										<th>Historial Enfermedades:</th>
										</tr>
								</thead>								
								
							</table>
						</div><!-- table responsive -->
						<div class="box-footer" style="margin:5px;">
							<a class="btn btn-primary" href="<c:url value='/paciente/nuevoPaciente' />">Nuevo Paciente </a>
							<a class="btn btn-secondary" href="<c:url value='/index/' />">Regresar </a>			
						</div>		
					</div><!-- card block -->
				</div><!-- card  -->
			</div><!-- col -->
		</div> <!-- row -->
	</div><!-- fluid -->
</div> <!-- content -->