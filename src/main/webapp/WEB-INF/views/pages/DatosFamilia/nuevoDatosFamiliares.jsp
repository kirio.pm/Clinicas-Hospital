<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">			
				<form:form method="POST" modelAttribute="familia">
				<div class="card">
					<h5 class="card-header">Registro de un nuevo paciente</h5>
					<div class="card-block" style="margin: 5px;">									
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Nombres Padre o Madre:</label>
									<form:input class="form-control" type="text" path="nombrePadre" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Apellido Padre o Madre:</label>
									<form:input class="form-control" type="text" path="apellidoPadre" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Dirreccion Padre o Madre:</label>
									<form:input class="form-control" type="text" path="direccion" />
								</div>
								<div class="col-md-6">
									<label for="direccion">FechaNacimiento Padre o Madre:</label>
									<form:input class="date form-control" type="text" path="fechaNacimiento" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<br>
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-12">
									<label for="nombre">Historial Enfermedades:</label>
									<form:input class="form-control" type="text" path="historialDeEnfermedades" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						</div><!-- card block -->	
				</div><!-- card  -->
				<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/index/' />">Cancelar</a>
								</div>
							</div>
						</div><!-- card block -->
					</div><!-- card  -->
				</form:form>
    		</div><!-- col -->
		</div> <!-- row -->
	</div><!-- fluid -->
</div> <!-- content -->