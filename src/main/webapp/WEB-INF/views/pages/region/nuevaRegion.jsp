<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="region">
					
					<div class="card">
						<h3 class="card-header">Nueva Region</h3>
						<div class="card-block" style="margin: 5px;">
							<div class=" row col-md-8">
								<label for="example-text-input" class="col-form-label">Continente</label>								
								<form:input class="form-control"  type="text" path="continente"/>																							
							</div>
							<div class=" row col-md-8">
								<label for="example-text-input" class="col-form-label">Pais</label>								
								<form:input class="form-control" type="text" path="pais"/>																							
							</div>							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/region/listaRegion' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>