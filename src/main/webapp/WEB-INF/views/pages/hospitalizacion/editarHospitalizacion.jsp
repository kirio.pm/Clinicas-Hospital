<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="hosUnico">
				<div class="card">
					<h5 class="card-header">Editar Hospitalizacion</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">									
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idHospitalizacion" />								
								</div>
								<div class="col-md-8">
									<label for="direccion">Fecha Inicio</label>
									<form:input class="date form-control" type="text" path="fechaInicioHosp" />
								</div>						
								<div class="col-md-8">
									<label for="direccion">Fecha Finalizacion:</label>
									<form:input class="date form-control" type="text" path="fechaFinHosp" />
								</div>
								<div class="row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Paciente</label>								
								<form:select class="form-control" path="idPaciente.idPaciente">
								 	<form:options  itemLabel="nombrePac" items="${listaPac}" itemValue="idPaciente" />
								</form:select>																					
								</div>
								<div class="row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Doctor</label>								
								<form:select class="form-control" path="idDoctor.idDoctor">
								 	<form:options  itemLabel="idEmpleado.nombreEmpleado" items="${listaDoc}" itemValue="idDoctor" />
								</form:select>																					
								</div>
								<div class="row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Sala</label>								
								<form:select class="form-control" path="idSala.idSala">
								 	<form:options  itemLabel="habitacionSal" items="${listaSal}" itemValue="idSala" />
								</form:select>																					
								</div>
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/tratamiento/listaTratamiento' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>

															
							
