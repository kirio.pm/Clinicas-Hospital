<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="hospitalizacion">
					
					<div class="card">
						<h3 class="card-header">Nueva Hospitalizacion</h3>
						<div class="card-block" style="margin: 5px;">
							<div class="col-md-8">
									<label for="direccion">Fecha Inicio</label>
									<form:input class="date form-control" type="text" path="fechaInicioHosp" />
								</div>
								<div class="col-md-8">
									<label for="direccion">Fecha Finalizacion:</label>
									<form:input class="date form-control" type="text" path="fechaFinHosp" />
								</div>
								
							<div class="row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Doctor</label>								
								<form:select class="form-control" path="idDoctor.idDoctor">
								 	<form:options  itemLabel="idEmpleado.nombreEmpleado" items="${listaDoc}" itemValue="idDoctor" />
								</form:select>																					
							</div>								
							<div class="row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Sala</label>								
								<form:select class="form-control" path="idSala.idSala">
								 	<form:options  itemLabel="habitacionSal" items="${listaSal}" itemValue="idSala" />
								</form:select>																					
							</div>
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/diagnostico/listaDiagnostico' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>