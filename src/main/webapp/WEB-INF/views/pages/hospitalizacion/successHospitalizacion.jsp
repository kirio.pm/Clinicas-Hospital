<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">INFORMACION DE HOSPITALIZACION</h3>
					<div class="card-block">
	      
	       <div id="successMessage" style="margin: 5px;">
	       <br>
	        <strong>Se ha registrado la hospitalizacion del paciente y se han guardado</strong>
	    </div >
	    <div id="emailFormDiv">
	    	<a class="btn btn-success" href="<c:url value='/hospitalizacion/listaHospitalizacion' />">Regresar </a>
	    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>