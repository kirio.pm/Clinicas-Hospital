<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista Empleado</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						
						<th>Nombre </th>
						<th>Apellido </th>
						<th>DUI</th>
						<th>E-mail</th>
						<th>Telefono</th>
						<th>Acciones</th>
						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="empleado">
					<tr>
						
						<td>${empleado.nombreEmpleado}</td>
						<td>${empleado.apellidoEmpleado}</td>
						<td>${empleado.duiEmp}</td>
						<td>${empleado.emilEmp}</td>
						<td>${empleado.telefonoEmp}</td>
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/empleados/editarEmp-${empleado.idEmpleado}' />">
								<i class="fa fa-edit"></i> 
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-primary" href="<c:url value='/empleados/nuevoEmpleados' />">
				Nuevo empleado 
				</a>
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
