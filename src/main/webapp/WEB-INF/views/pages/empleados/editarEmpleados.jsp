<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="empUnico">
				<div class="card">
					<h5 class="card-header">Editar Empleado</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">
									
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idEmpleado" />	
								
								</div>
								<div class="row col-md-8">
									<label for="direccion">Nombre:</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="nombreEmpleado" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Apellido:</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="apellidoEmpleado" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Dui:</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="duiEmp" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Telefono:</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="telefonoEmp" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Correo:</label>
									<form:input class="form-control" id="direccion" type="email" aria-describedby="direccion" value="" placeholder="" path="emilEmp" />
								</div>
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/empleados/listaEmpleados' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>
