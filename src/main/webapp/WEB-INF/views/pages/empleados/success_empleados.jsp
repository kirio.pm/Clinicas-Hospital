<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">INFORMACION  DE EMPLEADO</h3>
					<div class="card-block">
	      
	       <div id="successMessage">
	        <br>
	        <strong>Se ha registrado de empleado guardado</strong>
	    </div>
	    <br>
	    <div >
	    	<a class="btn btn-success" href="<c:url value='/empleados/listaEmpleados' />">Regresar</a>
	    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>