<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="empleado">
					
					<div class="card">
						<h3 class="card-header">Nuevo Empleado</h3>
						<div class="card-block" style="margin:5px;">
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Nombre:</label>								
								<form:input class="form-control" type="text" path="nombreEmpleado" />																							
							</div>
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Apellido:</label>								
								<form:input class="form-control" type="text" path="apellidoEmpleado" />																				
							</div>
							<div class="form-group row col-md-8">
								<label for="example-text-input" class="col-form-label">Dui:</label>								
								<form:input class="form-control" type="text" path="duiEmp" />																					
							</div>
							<div class="form-group row col-md-8">
								<label for="example-text-input" class="col-form-label">Email:</label>								
								<form:input class="form-control" type="email" path="emilEmp" />																				
							</div>
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Telefono:</label>								
								<form:input class="form-control" type="text" path="telefonoEmp" />																				
							</div>							
						</div>
					</div>					
					<div class="card">
						<div class="card-block" >
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-secondary" href="<c:url value='/empleados/listaEmpeados' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>