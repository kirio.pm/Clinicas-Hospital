<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2>Lista de exames</h2>
					</div>
					<div class="card-body">
						<table class="table table-bordered" width="100%" id="dataTable"
							cellspacing="0">
							<thead>
								<tr>
									<th>N�</th>
									<th>Nombre examen</th>
									<th>Categoria examen</th>
									<th>Precio del examen</th>
									<th>Fecha de recepci�n</th>
									<th>Imagenes/Videos</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${examenes}" var="item" varStatus="loop">
									<tr>
										<td>${loop.index + 1}</td>
										<td>${item.idCatExamen.nombreExamen }</td>
										<td>${item.idCatExamen.tipoExamen }</td>
										<td>$ ${item.idCatExamen.costoExamen }</td>
										<td>${item.fechaRececionExa }</td>
										<td class="text-center">
											<c:if test="${empty item.nombreOriginal }">
											Vacio
											</c:if>
											<c:if test="${not empty item.nombreOriginal }">
											<a href='<c:url value='/examen/${item.idExamen}/media/${item.nombreOriginal}'></c:url>'>${item.nombreOriginal }</a>
											</c:if>
										</td>
										<td><a class="btn btn-info"
											href="<c:url value='/examen/${id_consulta}/examen/${item.idExamen }/add-media' />">
												<i class="fa fa-picture-o" aria-hidden="true"></i>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="card-footer text-muted">
						<a class="btn btn-primary"
							href='<c:url value='/examen/${id_consulta}/nuevo-examen'></c:url>'
							role="button">Crear nuevo examen</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>