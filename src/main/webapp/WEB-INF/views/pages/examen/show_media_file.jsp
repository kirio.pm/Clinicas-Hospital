<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2>Multimedia</h2>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Informaci�n</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nombre:</td>
											<td>${file_info.nombreOriginal }</td>
										</tr>
										<tr>
											<td>Guardado en:</td>
											<td>${file_info.rutaArchivo }</td>
										</tr>
										<tr>
											<td>Tama�o:</td>
											<td>${file_info.tamanio }KB</td>
										</tr>

									</tbody>
								</table>
							</div>
							<div class="col-sm-6" style="display: block; margin-left: auto; margin-right: auto; width: 50%;">
								<c:if test="${file_info.formato == 'jpg' or file_info.formato == 'png' or file_info.formato == 'gif' or file_info.formato == 'jpeg' or file_info.formato == 'bmp'}">
								<img alt="https://unamo.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png" src='<c:url value='/examen/files/${file_info.nombreOriginal}'></c:url>'>
								</c:if>
								
								<c:if test="${file_info.formato == 'mp4' or file_info.formato == 'wmv' or file_info.formato == 'flv' or file_info.formato == 'mkv'}">
								<video width="520" height="360" controls>
								<source src="<c:url value='/examen/files/${file_info.nombreOriginal}'></c:url>" type="video/mp4">
								</video>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
