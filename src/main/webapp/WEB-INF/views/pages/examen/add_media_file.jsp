<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2>Agregar archivos multimedia</h2>
					</div>
					<div class="card-body">
						<form action="<c:url value='/examen/${id_consulta}/examen/${id_examen}/add-media?${_csrf.parameterName}=${_csrf.token}' />" method="POST" enctype="multipart/form-data">
						
							<div class="form-group">
								<label for="exampleInputFile">Subir Archivo Multimedia</label> <input
									type="file" class="form-control-file" id="InputFile"
									aria-describedby="fileHelp" name="file"> <small id="fileHelp"
									class="form-text text-muted">Ingrese un archivo imagen o de video no mayor a 50MB</small>
							</div>
							<button type="submit" class="btn btn-primary btn-lg">Guardar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>