<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2>Nuevo Examen</h2>
					</div>
					<div class="card-body">
						<form:form modelAttribute="examen" method="POST">
							<form:hidden path="idExamen" />
							<form:hidden path="idConsultaMedica.idConsulta"/>
							<form:hidden path="nombreOriginal"/>
							<form:hidden path="formato"/>
							<form:hidden path="rutaArchivo"/>
							<form:hidden path="tamanio"/>
							<div class="form-group row">
								<div class="col-10">
									<h3 class="form-text text-muted text center">Examen
										N�mero: ${numero_examen}</h3>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-2 col-form-label">Fecha de Examen</label>
								<div class="col-2">
									<form:input class="form-control date" type="text"
										path="fechaRececionExa" />
								</div>
							</div>
							
								<h5>Tipo de Examen</h5>
								<div class="custom-controls-stacked">
									<c:forEach items="${catalogo}" var="item">
										<label class="custom-control custom-radio"> 
										<input id="idCatExamen${item.idCatExamen}" name="idCatExamen.idCatExamen" type="radio" value="${item.idCatExamen}"
											class="custom-control-input"> <span
											class="custom-control-indicator"></span> <span
											class="custom-control-description">${item.nombreExamen}</span>
										</label>
									</c:forEach>
								</div>					

							<br>
							<br>
							<button type="submit" class="btn btn-primary">Guardar</button>
							<a type="button" href='<c:url value="/examen/${id_consulta}"></c:url>' class="btn btn-secondary">Cancelar</a>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
