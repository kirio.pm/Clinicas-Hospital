<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista de Tratamiento</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						<th>Frecuencia</th>
						<th>Tipo Tratamiento</th>
						<th>Enfermedad Diagnostico</th>
						<th>Acciones</th>						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="tratamiento">
					<tr>
						<td align="center">${tratamiento.frecuenciaTra}</td>
						<td>${tratamiento.tipoTra}</td>
						<td>${tratamiento.idDiagnostico.idCataDiag.nombreEnf}</td>						
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/tratamiento/editarTra-${tratamiento.idTratamiento}' />">
								<i class="fa fa-edit"></i>
							</a>
							<a class="btn btn-info" data-toggle="tooltip" title="Nuevo medicamento" href="<c:url value='/medicamento/nuevoMedicamento/${tratamiento.idTratamiento}' />">
								<i class="fa fa-fighter-jet"></i> 
							</a>
							<a class="btn btn-info" data-toggle="tooltip" title="Nuevo tratamiento" href="<c:url value='/terapia/nuevaTerapia/${tratamiento.idTratamiento}' />">
								<i class="fa  fa-user-md"></i> 
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-success" href="<c:url value='/diagnostico/' />">
				Regresar a Diagnosticos
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
