<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Ingreso</title>

<!-- Bootstrap Core CSS -->
<link
	href='<c:url value="/static/bootstrap/bootstrap4/css/bootstrap.min.css"></c:url>'
	rel="stylesheet">

<!-- siaps static resources-->
<link href='<c:url value="/static/css/login.css"></c:url>'
	rel="stylesheet">

</head>

<body  background='<c:url value="/static/images/medicas.jpg"></c:url>'>	
	<div class="container">
	
	<div class="row">
    <div class="col-sm-4 col-md-offset-4"></div>
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                
                    <div id="nombre"><strong>Sistema Clinico</strong></div>
                    	<c:url var="loginUrl" value="/login" />
                <form class="form-signin" action="${loginUrl}" method="post">
                <c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION != null}">
						<div class="alert alert-danger text-center">
							<p>${sessionScope.SPRING_SECURITY_LAST_EXCEPTION}</p>
						</div>
				</c:if>
				<c:if test="${param.logout != null}">
						<div class="alert alert-success text-center">
							<p>Su sesion se ha cerrrado correctamente.</p>
						</div>
				</c:if>
				<div class="form-group">
				<input type="text" class="form-control"id="username" placeholder="Usuario" name="username" required autofocus>
				</div>
                <div class="form-group">
                <input type="password" class="form-control" placeholder="Contraseña" id="password" name="password" value="" required>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Ingresar</button>
                <label class="checkbox pull-left">
                    <input name="remember-me" id="remember-me" type="checkbox">Recordarme
                </label>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
            </div>
            
        </div>
        <div class="col-sm-4 col-md-offset-4"></div>
    </div>
</div>
</body>

</html>
