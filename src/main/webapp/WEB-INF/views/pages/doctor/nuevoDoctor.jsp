<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="doc">
					
					<div class="card">
						<h3 class="card-header">Nuevo Doctor</h3>
						<div class="card-block" style="magin:5px;">
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Especialidad:</label>								
								<form:input class="form-control" type="text" path="especialidadDoc" />																							
							</div>
							
							<div class="form-group row col-md-8">
								<label for="example-text-input" class=" col-form-label">Nombre Empleado:</label>								
								<form:select class="form-control" path="idEmpleado.idEmpleado">
								<form:options itemLabel="nombreEmpleado" items="${listaEmp}" itemValue="idEmpleado" />
								</form:select>							
							</div>
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/doctor/listaDoctor' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>