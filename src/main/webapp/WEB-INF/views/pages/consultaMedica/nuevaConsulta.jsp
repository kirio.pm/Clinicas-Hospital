<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="consulta">
					
					<div class="card" >
						<h3 class="card-header">Nuevo Consulta</h3>
						<div class="card-block" style="margin: 5px;">
							<div class="form-group row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Descripcion</label>								
								<form:textarea class="form-control" type="text" path="descripcionCon" />																							
							</div>							
							<div class="form-group row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Categoria:</label>								
								<form:select path="idCatCon.idCatCon" class="form-control">
								 	<form:options itemLabel="tipoCon" items="${listaCat}" itemValue="idCatCon" />
								</form:select>																					
							</div>
							<div class="form-group row col-md-8">
								<label for="example-text-input" class="col-2 col-form-label">Doctor:</label>								
								<form:select path="idDoctor.idDoctor" class="form-control">
								 	<form:options itemLabel="idEmpleado.nombreEmpleado" items="${listaDoc}" itemValue="idDoctor" />
								</form:select>																				
							</div>
							
						</div>
					</div>					
					<div class="card">
						<div class="card-block" style="margin: 5px;">
							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>