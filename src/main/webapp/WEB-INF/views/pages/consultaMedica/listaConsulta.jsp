<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h5 class="card-header">Lista de Consultas</h5>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-bordered" width="100%" id="dataTable"	cellspacing="0">
								<thead>
									<tr>
									    <th>Codigo Expediente</th>
										<th>Descripcion</th>
										<th>Fecha Consulta</th>
										<th>Tipo</th>
										<th>Nombre Del Doctor</th>
										<th>Opciones</th> 
									</tr>
								</thead>
				
				<tbody>
				  <c:forEach items="${listaCon}" var="consulta">
					<tr>
						<td>${consulta.idExpediente.codExpediente}</td>			
						<td>${consulta.descripcionCon}</td>
						<td>${consulta.fechaCon}</td>
						<td>${consulta.idCatCon.tipoCon}</td>
						<td>${consulta.idDoctor.idEmpleado.nombreEmpleado}</td>
											
						<td >
							<a class="btn btn-info" data-toggle="tooltip" title="Editar" href="<c:url value='/consultaMedica/editarCon-${consulta.idConsulta}' />">
								<i class="fa fa-edit"></i>
							</a>
							<a class="btn btn-info" data-toggle="tooltip" title="Tomar Signos Vistales" href="<c:url value='/signosVitales/signosVitales/${consulta.idConsulta}' />">
								<i class="fa fa-heartbeat"></i>
							</a>
							<a class="btn btn-info" data-toggle="tooltip" title="Tomar Sintomas Pacientes" href="<c:url value='/Sintomatologia/nuevoSintoma/${consulta.idConsulta}' />">
								<i class="fa fa-stethoscope"></i>
							</a>
							<a class="btn btn-info" data-toggle="tooltip" title="Tomar Diagnostico" href="<c:url value='/diagnostico/nuevoDiagnostico/${consulta.idConsulta}' />">
								<i class="fa fa-medkit"></i>
							</a>  
							<a class="btn btn-info"  data-toggle="tooltip" title="Tomar Examen" href="<c:url value='/examen/${consulta.idConsulta }' />">
								<i class="fa fa-h-square"></i>
							</a>

						</td> 
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="margin:5px;">
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>		
			</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
