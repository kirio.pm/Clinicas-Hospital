<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="costo">
					
					<div class="card">
						<h3 class="card-header">Nuevo Costo</h3>
						<div class="card-block" style="magin:5px;">
							<div class="row col-md-8">
									<label for="direccion">Fecha de Registro</label>
									<form:input class="date form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="fechaCosto" />
								</div>
								
								<div class="row col-md-8">
									<label for="direccion">Unidad Monetaria</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="unidadMonetaria" />
								</div>
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/costo/listaCosto' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>