<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista de Terapia</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Medicamento</th>
						<th>Precio</th>
						<th>Tratamiento</th>
						<th>Frecuencia</th>
						<th>Acciones</th>						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="tera">
					<tr>
						<td align="center">${tera.fechaDeTera}</td>
						<td>${tera.idCatTera.nombreTera}</td>
						<td>${tera.idCatTera.costoTera}</td>
						<td>${tera.idTratamiento.tipoTra}</td>
						<td>${tera.idTratamiento.frecuenciaTra}</td>						
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/terapia/editarTer-${tera.idTerapia}' />">
								<i class="fa fa-edit"></i>
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-primary" href="<c:url value='/terapia/nuevaTerapia' />">
				Nueva Terapia
				</a>
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
