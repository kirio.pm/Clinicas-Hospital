<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="terUnico">
				<div class="card">
					<h5 class="card-header">Editar Terapia</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">									
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idTerapia" />								
								</div>
								
								<div class="row col-md-8">
									<label for="direccion">Fecha</label>
									<form:input class="date form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="fechaDeTera" />
								</div>								
							
								
							<div class="row col-md-8">
								<label for="example-text-input" class="col-form-label">Catalogo de medicamento:</label>								
								<form:select class="form-control" path="idCatTera.idCatTera">
								 	<!--<form:options  itemLabel="correlativoCon" items="${listaCon}" itemValue="idConsulta" />-->
								 	<c:forEach items="${listaCTer}" var="item">
								 		<option value="${item.idCatTera}">
								 		${item.nombreTera} -- $${item.costoTera}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>	
							
							<div class="row col-md-8">
								<label for="example-text-input" class="col-form-label">Tratamiento:</label>								
								<form:select class="form-control" path="idTratamiento.idTratamiento">								 	
								 	<c:forEach items="${listaTra}" var="item">
								 		<option value="${item.idTratamiento}">
								 		${item.tipoTra} -- $${item.frecuenciaTra}   
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
								
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/terapia/' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>
