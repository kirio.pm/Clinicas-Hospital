<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="terapia">
					
					<div class="card" style="magin:10px;">
						<h3 class="card-header">Nuevo Terapia</h3>
						<div class="card-block" style="magin:10px;">
							<div class="row col-md-8">
									<label for="direccion">Fecha</label>
									<form:input class="date form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="fechaDeTera" />
								</div>								
							
								
							<div class="row col-md-8">
								<label for="example-text-input" class="col-form-label">Catalogo de terapias:</label>								
								<form:select class="form-control" path="idCatTera.idCatTera">
								 	<!--<form:options  itemLabel="correlativoCon" items="${listaCon}" itemValue="idConsulta" />-->
								 	<c:forEach items="${listaCTer}" var="item">
								 		<option value="${item.idCatTera}">
								 		${item.nombreTera} -- $${item.costoTera}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/terapia/' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>