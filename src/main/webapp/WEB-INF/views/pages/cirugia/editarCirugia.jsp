<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="cirUnico">
				<div class="card">
					<h5 class="card-header">Editar Cirugia</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">									
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idCirugia" />								
								</div>
								
								<div class="row col-md-8">
									<label for="direccion">Fecha</label>
									<form:input class=" date form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="fechaCiru" />
								</div>	
								<div class="row col-md-8">
									<label for="direccion">Hora</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="horaCiru" />
								</div>
															
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Paciente:</label>								
								<form:select class="form-control" path="idBitacora.idBitacora">
								 	<c:forEach items="${listaBit}" var="item">
								 		<option value="${item.idBitacora}">
								 		${item.idHospitalizacion.idPaciente.nombrePac}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Cirugia/Costo:</label>								
								<form:select class="form-control" path="idCatCiru.idCatCiru">
								 	<c:forEach items="${listaCatCi}" var="item">
								 		<option value="${item.idCatCiru}">
								 		${item.nombreCiru} // $ ${item.costoCirugia}
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
							<div class="row col-md-8">
								<label for="direccion">Procedimiento Extra</label>
								<form:textarea class="form-control" rows="3" id="direccion" type="text" 
								aria-describedby="direccion" value="" placeholder="" path="procesoCiru" />
							</div>
					
														
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/cirugia/' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>
