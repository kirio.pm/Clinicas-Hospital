<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="cirugia">
					
					<div class="card" >
						<h3 class="card-header">Nueva Cirugia</h3>
						<div class="card-block" style="magin:5px;">
							<div class="row col-md-8">
									<label for="direccion">Fecha</label>
									<form:input class=" date form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="fechaCiru" />
								</div>	
								<div class="row col-md-8">
									<label for="direccion">Hora</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="horaCiru" />
								</div>
															
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Paciente:</label>								
								<form:select class="form-control" path="idBitacora.idBitacora">
								 	<c:forEach items="${listaBit}" var="item">
								 		<option value="${item.idBitacora}">
								 		${item.idHospitalizacion.idPaciente.nombrePac}  
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
							<div class="row col-md-8">
								<label for="example-text-input" class=" col-form-label">Cirugia/Costo:</label>								
								<form:select class="form-control" path="idCatCiru.idCatCiru">
								 	<c:forEach items="${listaCatCi}" var="item">
								 		<option value="${item.idCatCiru}">
								 		${item.nombreCiru} // $ ${item.costoCirugia}
								 		</option>
								 	</c:forEach>
								</form:select>																					
							</div>
							
							<div class="row col-md-8">
								<label for="direccion">Procedimiento Extra</label>
								<form:textarea class="form-control" rows="3" id="direccion" type="text" 
								aria-describedby="direccion" value="" placeholder="" path="procesoCiru" />
							</div>
							
						</div>
					</div>					
					<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px; margin:5px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar" class="btn btn-primary custom-width" /> 
									<a class="btn btn-success" href="<c:url value='/cirugia/' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>