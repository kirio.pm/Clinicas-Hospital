<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista de Medicamentos Recetados</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Hora</th>
						<th>Paciente</th>
						<th>Cirugia</th>
						<th>Precio</th>
						<th>Acciones</th>						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="ciru">
					<tr>
						<td align="center">${ciru.fechaCiru}</td>
						<td>${ciru.horaCiru}</td>
						<td>${ciru.idBitacora.idHospitalizacion.idPaciente.nombrePac}</td>
						<td>${ciru.idCatCiru.nombreCiru}</td>
						<td>${ciru.idCatCiru.costoCirugia}</td>
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/cirugia/editarCir-${ciru.idCirugia}' />">
								<i class="fa fa-edit"></i>
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-primary" href="<c:url value='/cirugia/nuevaCirugia' />">
				Nueva Cirugia
				</a>
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
