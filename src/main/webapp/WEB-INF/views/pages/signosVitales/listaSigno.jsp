<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Lista de la toma de Signos Vitales</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
					    <th>Consulta</th>
						<th>Altura (mts)</th>
						<th>Frecuencia Cardiaca </th>
						<th>Peso (Lbs)</th>
						<th>Presion Arterial (mmHg)</th>
						<th>Temperatura (ºC)</th>
						
						<th>Acciones</th>
						
					</tr>
				</thead>
				
				<tbody>
				  <c:forEach items="${lista}" var="signo">
					<tr>
						<td>${signo.idConsulta.idExpediente.codExpediente}</td>
						<td align="center">${signo.alturaSiv}</td>
						<td>${signo.frecuenciaCardiacaSiv}</td>
						<td>${signo.pesoSiv}</td>
						<td>${signo.presionArterialSiv}</td>
						<td>${signo.temperaturaSiv}</td>
						
						<td align="center">
							<a class="btn btn-info" href="<c:url value='/signosVitales/editarSig-${signo.idSignosVitales}' />">
								<i class="fa fa-edit"></i>
							</a>
						</td>				
						
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-success" href="<c:url value='/consultaMedica/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
