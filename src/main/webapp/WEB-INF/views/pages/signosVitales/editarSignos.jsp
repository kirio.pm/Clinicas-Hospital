<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
			<form:form method="POST" modelAttribute="sigUnico">
				<div class="card">
					<h5 class="card-header">Editar Signos Vitales</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
							<div class="form-row" style="margin:5px;">
								<div class="row col-md-8">
								<form:input class="form-control" id="direccion" type="hidden" aria-describedby="direccion" value="" placeholder="" disabled="true" required="true" path="idSignosVitales" />	
								</div>
								<div class="row col-md-8">
									<label for="direccion">Altura</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="alturaSiv" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Frecuencia Cardiaca</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="frecuenciaCardiacaSiv" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Peso</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="pesoSiv" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Presion Arterial</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="presionArterialSiv" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Temperatura</label>
									<form:input class="form-control" id="direccion" type="text" aria-describedby="direccion" value="" placeholder="" path="temperaturaSiv" />
								</div>
								<div class="row col-md-8">
									<label for="direccion">Consulta</label>
									
									<form:select class="form-control" path="idConsulta.idConsulta" itemLabel="correlativoCon"   itemValue="idConsulta" items="${listaCon}">
									 	
									</form:select>
								</div>
								
							</div>
							<input type="submit" value="Editar" class="btn btn-primary custom-width" /> 
							<a class="btn btn-secondary" href="<c:url value='/signosVitales/listaSigno' />">Cancelar</a>
						</div>
						</div>
						</div>
					</div>
				</div>
			
				</form:form>
			</div>
		</div>
    </div>
</div>
