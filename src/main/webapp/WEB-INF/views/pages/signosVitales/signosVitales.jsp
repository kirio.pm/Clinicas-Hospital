<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="signos">
					
					<div class="card">
						<h3 class="card-header">Nueva toma de signos vitales</h3>
						<div class="">
							
							<div class="form-group ">
								<div class="col-4">
									<label for="example-text-input" class=" col-form-label">Altura (mts):</label>
									<form:input class="form-control" type="text" path="alturaSiv" />
								</div>															
							</div>
							<div class="form-group ">
								<div class="col-4">
									<label for="example-text-input" class="col-form-label">Frecuencia Cardiaca(LPM):</label>
									<form:input class="form-control" type="text" path="frecuenciaCardiacaSiv" />
								</div>													
							</div>
							<div class="form-group ">
								<div class="col-4">
									<label for="example-text-input" class="col-form-label">Peso(Lb):</label>
									<form:input class="form-control" type="text" path="pesoSiv" />
								</div>													
							</div>
							<div class="form-group ">								
								<div class="col-4">
									<label for="example-text-input" class="col-form-label">Presion Arterial(mmgh):</label>
									<form:input class="form-control" type="text" path="presionArterialSiv" />
								</div>													
							</div>
							<div class="form-group ">
								<div class="col-4">
									<label for="example-text-input" class=" col-form-label">Temperatura(F):</label>
									<form:input class="form-control" type="text" path="temperaturaSiv" />
								</div>													
							</div>
							</div>
					</div>					
					<div class="card">
						<div class="" style="margin:5px;">


							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/signosVitales/listaSigno' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>

				</form:form>


			</div>
		</div>
	</div>
</div>