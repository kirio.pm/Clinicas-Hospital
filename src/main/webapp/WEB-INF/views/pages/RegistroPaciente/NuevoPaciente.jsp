<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">			
				<form:form method="POST" modelAttribute="paciente">
				<div class="card">
					<h5 class="card-header">Registro de un nuevo paciente</h5>
					<div class="card-block" style="margin: 5px;">									
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="nombre">Nombre:</label>
									<form:input class="form-control" type="text" path="nombrePac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Apellido:</label>
									<form:input class="form-control" type="text" path="apellidoPac" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Genero:</label>
										<form:select class="form-control" path="generoPac">
   											<form:option value="NONE" label="--- Seleccionar ---"/>
   											<form:option value="Hombre" label="Hombre"/>
   											<form:option value="Mujer" label="Mujer"/>
										</form:select>
								</div>
								<div class="col-md-6">
									<label for="direccion">Fecha nacimiento:</label>
									<form:input class="date form-control" type="text" path="fechaNacimientoPac" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Apellido Casada: </label>
									<form:input class="form-control" type="text" path="apellidoDeCasadoPac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Estado civil:</label>
									<form:select class="form-control" path="estadoCivilPac">
   											<form:option value="NONE" label="--- Seleccionar ---"/>
   											<form:option value="Soltero/a" label="Soltero/a"/>
   											<form:option value="Casado/a" label="Casado/a"/>
   											<form:option value="Viudo/a" label="Viudo/a"/>
   											<form:option value="Acompaņado" label="Acompaņado"/>
   											<form:option value="Divorciado" label="Divorciado"/>
										</form:select>
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->	
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Direccion:</label>
									<form:input class="form-control" type="text" path="direccionPac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Telefono:</label>
									<form:input class="form-control" type="text" path="telefonoPac" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">Responsable por alguna emergencia</label>
									<form:input class="form-control" type="text" path="responsablePorEmergenciaPac" />
								</div>
								<div class="col-md-6">
									<label for="direccion">Email</label>
									<form:input class="form-control" type="text" path="emailPac" />
								</div>
							</div><!-- form row -->
						</div><!-- form gruop-->
						
						<div class="form-group">
							<div class="form-row">
								<div class="col-md-6">
									<label for="empresa">DUI</label>
									<form:input class="form-control" type="text" path="numeroIdentificacion" />
								</div>
								
							</div><!-- form row -->
						</div><!-- form gruop-->																									
					</div><!-- card block -->	
				</div><!-- card  -->
				<div class="card">
						<div class="card-block">
							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar"
										class="btn btn-primary custom-width" /> <a
										class="btn btn-secondary"
										href="<c:url value='/index/' />">Cancelar</a>
								</div>
							</div>
						</div><!-- card block -->
					</div><!-- card  -->
				</form:form>
    		</div><!-- col -->
		</div> <!-- row -->
	</div><!-- fluid -->
</div> <!-- content -->