<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h5 class="card-header">Lista de Pacientes</h5>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-bordered" width="100%" id="dataTable"	cellspacing="0">
								<thead>
									<tr>										
										<th>Nombre </th>
										<th>Apellido </th>
										<th>Genero </th>
										<th>Estado Civil</th>
										<th>Fecha Nacimiento</th>
										<th>DUI </th>
										<th>Email</th>
										<th>Telefono</th>
										<th>Acciones</th>									
									</tr>
								</thead>								
								<tbody>
								  <c:forEach items="${pacientes}" var="paciente">
									<tr>
										<td align="center">${paciente.nombrePac}</td>
										<td align="center">${paciente.apellidoPac}</td>
										<td align="center">${paciente.generoPac}</td>
										<td align="center">${paciente.estadoCivilPac}</td>
										<td align="center">${paciente.fechaNacimientoPac}</td>
										<td align="center">${paciente.numeroIdentificacion}</td>
										<td align="center">${paciente.emailPac}</td>
										<td align="center">${paciente.telefonoPac}</td>
										<td align="center">
											<a class="btn btn-secondary" data-toggle="tooltip" title="Editar" href="<c:url value='/paciente/editarPx-${paciente.idPaciente}' />">
												<i class="fa fa-edit"></i> 
											</a>
											<a class="btn btn-secondary" data-toggle="tooltip" title="Nuevo Historial clinico" href="<c:url value='/hclinico/nuevohclinico/${paciente.idPaciente}' />">
												<i class="fa fa-address-card"></i>  
											</a>
											<a class="btn btn-secondary" data-toggle="tooltip" title="Crear Cita" href="<c:url value='/cita/nuevaCita/${paciente.idPaciente}' />">
												<i class="fa fa-calendar"></i> 
											</a>
											<a class="btn btn-secondary" data-toggle="tooltip" title="Hospitalizar" href="<c:url value='/hospitalizacion/nuevaHospitalizacion/${paciente.idPaciente}' />">
												<i class="fa fa-ambulance"></i> 
											</a>
										</td>																											
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div><!-- table responsive -->
						<div class="box-footer" style="margin:5px;">
							<a class="btn btn-primary" href="<c:url value='/paciente/nuevoPaciente' />">Nuevo Paciente </a>
							<a class="btn btn-secondary" href="<c:url value='/index/' />">Regresar </a>			
						</div>		
					</div><!-- card block -->
				</div><!-- card  -->
			</div><!-- col -->
		</div> <!-- row -->
	</div><!-- fluid -->
</div> <!-- content -->