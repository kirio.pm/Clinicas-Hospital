<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">
				<form:form method="POST" modelAttribute="cita">
				<div class="card">
					<h5 class="card-header">Nueva Cita</h5>
					<div class="card-block">
						<div class="table-responsive">
						<div class="container">
						<div class="form-group">
						
							<div class="row col-md-8">
									<label for="direccion">Medio Registro:</label>
									<form:input class="form-control" type="text" path="medioRegistro" />
								</div>
								<br>
								<div class="row col-md-8">
							
								<label for="example-text-input" >Doctor:</label>
								<form:select class="form-control" path="idDoctor.idDoctor">
									 	<form:options class="form-control" itemLabel="idEmpleado.nombreEmpleado" items="${listaDoc}" itemValue="idDoctor" />
									</form:select>
								</div>
								<br>
							
							<div class="row col-md-8">
								<label for="example-text-input" >Tipo Especialidad:</label>
								
									<form:select class="form-control" path="idCatalogoCita.idCatalogoCita">
									 	<form:options itemLabel="nombreCita" items="${ListaConsultas}" itemValue="idCatalogoCita" />
									</form:select>
							</div>
						<br>	
								<div class="row col-md-8">
									<label for="direccion">Fecha Cita:</label>
									<form:input class="date form-control" type="text" path="fechaCita" />
								</div>
								<br>
								<div class="row col-md-8">
									<label for="direccion">Hora Cita:</label>
									<form:input type="text" class="form-control" path="horaCita" />
								</div>
								<br>
						</div>
						</div>
						</div>
					</div>
				</div>	
				<div class="card">
						<div class="card-block">


							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<input type="submit" value="Guardar Cita" class="btn btn-primary custom-width" /> 
									<a class="btn btn-secondary" href="<c:url value='/empleados/listaEmpeados' />">Cancelar</a>
								</div>
							</div>
						</div>
					</div>
			</form:form>
			</div>
		</div>
	</div>
</div>
