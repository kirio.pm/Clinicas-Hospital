<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Log Del Sistema</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						
						<th>Accion </th>
						<th>Usuario Accionador</th>
						<th>Fecha</th>
						<th>Tabla Afectada</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${Lista}" var="citas">
					<tr>
					
					<td align="center">${citas.accion} </td>
					<td align="center">${citas.usuario} </td>
					<td align="center">${citas.fechaLog} </td>
					<td align="center">${citas.tablasAfectadas} </td>
					
					
					</c:forEach>
					</tbody>
				
				
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
