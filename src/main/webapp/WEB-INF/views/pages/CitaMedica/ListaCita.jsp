<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="content-wrapper">
    <div class="container-fluid">
    	<div class="row">
			<div class="col">
				<div class="card">
					<h3 class="card-header">Citas Medicas</h3>
					<div class="card-block">
						<div class="table-responsive">
			<table class="table table-bordered" width="100%" id="dataTable"
				cellspacing="0">
				<thead>
					<tr>
						
						<th>Paciente</th>
						<th>Numero telefonico </th>
						<th>E-mail</th>
						<th>Fecha Cita</th>
						<th>Medio Registro</th>
						<th>Hora Cita</th>
						<th>Acciones</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${Lista}" var="citas">
					<tr>
					<td align="center">${citas.idPaciente.nombrePac} ${citas.idPaciente.apellidoPac}</td>
					<td align="center">${citas.idPaciente.telefonoPac} </td>
					<td align="center">${citas.idPaciente.emailPac} </td>
					<td align="center">${citas.medioRegistro} </td>
					<td align="center">${citas.fechaCita} </td>
					<td align="center">${citas.horaCita} </td>
					<td align="center">
						<a class="btn btn-secondary" data-toggle="tooltip" title="Editar" href="<c:url value='/cita/EditarCita/${citas.idCita}' />">
												<i class="fa fa-edit"></i> 
											</a>
						</td>
					</tr>
					</c:forEach>
					</tbody>
				
				
			</table>
			<br>
			<div class="box-footer" style="margin:5px;">
				<a class="btn btn-success" href="<c:url value='/index/' />">
				Regresar 
				</a>			
			</div>
			
		</div>
		
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
