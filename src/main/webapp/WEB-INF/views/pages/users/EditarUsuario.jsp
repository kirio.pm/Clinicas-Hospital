<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="content-wrapper">
    <div class="container-fluid">
		<div class="row">
			<div class="col">


				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<c:url value='/' />">Inicio</a></li>
					<li class="breadcrumb-item"><a href="<c:url value='/user/' />">Gesti�n
							de personal</a></li>
					<li class="breadcrumb-item active">
								Editar Usuario
							</li>
				</ol>

				<div class="card">
					<h3 class="card-header">Editar Usuario</h3>
					<div class="card-block">
						<form:form method="POST" modelAttribute="user"
							class="form-horizontal">
							<form:input type="hidden" path="id" />
							<form:input type="hidden" path="empleado.idEmpleado" />
<%-- 							<form:input type="hidden" path="joining_date" /> --%>
<%-- 							<form:input type="hidden" path="last_login" /> --%>
							<p>
							<h4>Informaci�n de usuario</h4>
							</p>
							<div class="row">
								<div class="form-group col-md-12">


									<label class="col-md-3 control-lable">Nombre
										de usuario</label>

									<div class="col-md-3">
		
												<form:input type="text" path="username" id="username"
													name="username" class="form-control" />
												<span id="Musername"></span>
												<div class="form-control-feedback">
													<form:errors path="username" class="help-inline" />
												</div>
											
									</div>

								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">

									<label class="col-md-3 control-lable" for="password">Contrase�a</label>
									<div class="col-md-7">
										<form:input type="password" path="password" id="password"
											class="form-control input-sm" />

										<div class="has-error">
											<form:errors path="password" class="help-inline" />
										</div>
									</div>

								</div>
							</div>
														<div class="row">
								<div class="form-group col-md-12">

									<label class="col-md-3 control-lable" for="enabled">Estado</label>
									<div class="col-md-7">
										<form:checkbox path="enabled" id="enabled"
											class="form-control input-sm" />

										<div class="has-error">
											<form:errors path="enabled" class="help-inline" />
										</div>
									</div>

								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable" >Roles</label>
									<div class="col-md-7">
										<form:select path="profiles" items="${roles}" multiple="false"
											itemValue="id" itemLabel="type" class="form-control input-sm" />
										<div class="has-error">
											<form:errors path="profiles" class="help-inline" />
										</div>
									</div>
								</div>
							</div>

							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
										
											<input type="submit" value="Actualizar"
												class="btn btn-primary" />
											<a class="btn btn-danger" href="<c:url value='/usuarios/' />">Cancelar</a>
										
										
								</div>
							</div>
						</form:form>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>