<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="page-content-wrapper">
	<div class="container">
		<div class="panel panel-default">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href='<c:url value = "/" ></c:url>'>Home</a></li>
				<li class="breadcrumb-item"><a href='<c:url value = "/user/" ></c:url>'>Gesti�n de usuarios</a></li>
				<li class="breadcrumb-item active">Detalles de usuario</li>
			</ol>
			<div class="panel-heading">
				<h1 class="page-header">Detalle del Usuario</h1>
			</div>
			<p>
				<br>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="card">
					<fieldset disabled>
						<div class="card-block">
							<h3 class="card-title">Informacion Personal</h3>
							<p>
								<label class="col-5 col-form-label" for="firstname">Nombre</label>
								<input type="text" class="form-control"
									value="${personal.firstname}" readonly>
							</p>
							<p>
								<label class="col-5 col-form-label" for="lastname">Apellido</label>
								<input type="text" class="form-control"
									value="${personal.lastname}" readonly>
							</p>
							<p>
								<label class="col-5 col-form-label" for="carnet">Carnet</label>
								<input type="text" class="form-control"
									value="${personal.carnet}" readonly>
							</p>
							<p>
								<label class="col-5 col-form-label" for="charge">Cargo</label> <input
									type="text" class="form-control" value="${personal.charge}"
									readonly>
							</p>
							<p>
								<label class="col-5 col-form-label" for="email">Correo
									Electronico</label> <input type="text" class="form-control"
									value="${personal.email}" readonly>
							</p>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card">
					<fieldset disabled>
						<div class="card-block">
							<h3 class="card-title">Informacion de Usuario</h3>
							<p>
								<label class="col-5 col-form-label" for="username">Nombre
									de Usuario</label> <input type="text" class="form-control"
									value="${user.username}" readonly>
							</p>
							<p>
								<label class="col-md-3 control-lable" for="prifiles">Rol</label>
								<c:forEach var="item" items="${profiles}">
									<ul>
										<li>${item.type}</li>
									</ul>
								</c:forEach>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>