<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="page-content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col">


				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<c:url value='/' />">Inicio</a></li>
					<li class="breadcrumb-item"><a href="<c:url value='/user/' />">Gesti�n
							de personal</a></li>
					<li class="breadcrumb-item active"><c:choose>
							<c:when test="${edit}">
								Editar Usuario
							</c:when>
							<c:otherwise>
								Nuevo Usuario
							</c:otherwise>
						</c:choose></li>
				</ol>

				<div class="card">
					<h3 class="card-header">
						<c:choose>
							<c:when test="${edit}">
								Editar Usuario
							</c:when>
							<c:otherwise>
								Nuevo Usuario
							</c:otherwise>
						</c:choose>
					</h3>
					<div class="card-block">
						<form:form method="POST" modelAttribute="user"
							class="form-horizontal">
							<form:input type="hidden" path="id" />
							<form:input type="hidden" path="personal.id" />
							<form:hidden path="joining_date" />
							<form:hidden path="last_login" />
							<p>
							<h4>Informaci�n Personal</h4>
							</p>
							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable" for="firstname">Nombre</label>
									<div class="col-md-7">							
										<form:input type="text" path="personal.firstname"
											id="firstname" class="form-control input-sm form-input" cssErrorClass="form-control form-control-danger"/>
										<div class="form-group has-danger">
											<div class="form-control-feedback">
												<form:errors path="personal.firstname" class="help-inline" />
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable" for="lastname">Apellidos</label>
									<div class="col-md-7">
										<form:input type="text" path="personal.lastname" id="lastname"
											class="form-control input-sm form-input" />
										<div class="has-error">
											<form:errors path="personal.lastname" class="help-inline" />
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable">Denominaci�n</label>
									<div class="col-md-4">
										<form:select path="personal.designation"
											class="form-control" >
												<form:option value="">Ninguno</form:option>
												<form:option value="Sr.">Se�or</form:option>
												<form:option value="Sra.">Se�ora</form:option>
												<form:option value="Srta.">Se�orita</form:option>
												<form:option value="Lic.">Licenciado / Licenciada</form:option>
												<form:option value="Ing.">Ingeniero / Ingeniera</form:option>
												<form:option value="Prof.">Profesor</form:option>
												<form:option value="Dir.">Director</form:option>										
											</form:select>									
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable">Cargo</label>
									<div class="col-md-7">
										<form:input type="text" path="personal.charge" id="lastname"
											class="form-control input-sm form-input" />
										<div class="has-error">
											<form:errors path="personal.charge" class="help-inline" />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable">Carnet</label>
									<div class="col-md-7">
										<form:input type="text" path="personal.carnet" id="lastname"
											class="form-control input-sm form-input" />
										<div class="has-error">
											<form:errors path="personal.carnet" class="help-inline" />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable" for="email">Correo
										Electronico</label>
									<div class="col-md-7">
										<form:input type="text" path="personal.email" id="email"
											class="form-control input-sm" />
										<div class="has-error">
											<form:errors path="personal.email" class="help-inline" />
										</div>
									</div>
								</div>
							</div>

							<p>
							<h4>Informaci�n de usuario</h4>
							</p>
							<div class="row">
								<div class="form-group col-md-12">


									<label class="col-md-3 control-lable">Nombre
										de usuario</label>

									<div class="col-md-3">
										<c:choose>
											<c:when test="${edit}">
												<form:input type="text" path="username" id="disabledInput"
													class="form-control input-sm" disabled="true" />
											</c:when>
											<c:otherwise>
												<form:input type="text" path="username" id="username"
													name="username" class="form-control" />
												<span id="Musername"></span>
												<div class="form-control-feedback">
													<form:errors path="username" class="help-inline" />
												</div>
											</c:otherwise>
										</c:choose>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">

									<label class="col-md-3 control-lable" for="password">Contrase�a</label>
									<div class="col-md-7">
										<form:input type="password" path="password" id="password"
											class="form-control input-sm" />

										<div class="has-error">
											<form:errors path="password" class="help-inline" />
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label class="col-md-3 control-lable" >Roles</label>
									<div class="col-md-7">
										<form:select path="profiles" items="${roles}" multiple="false"
											itemValue="id" itemLabel="type" class="form-control input-sm" />
										<div class="has-error">
											<form:errors path="profiles" class="help-inline" />
										</div>
									</div>
								</div>
							</div>

							<div class="row" style="padding-left: 40px;">
								<div class="form-actions floatRight">
									<c:choose>
										<c:when test="${edit}">
											<input type="submit" value="Actualizar"
												class="btn btn-primary" />
											<a class="btn btn-danger" href="<c:url value='/user/list' />">Cancel</a>
										</c:when>
										<c:otherwise>
											<input type="submit" value="Registrar"
												class="btn btn-primary" />
											<a class="btn btn-danger" href="<c:url value='/user/list' />">Cancel</a>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</form:form>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>

