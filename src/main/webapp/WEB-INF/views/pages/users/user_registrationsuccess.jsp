<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="content-wrapper">
    <div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">MENSAJE DE CONFIRMACION</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="alert alert-success lead">${success}</div>

				<span class="well floatRight"> Regresar al <a
					href="<c:url value='/usuarios/' />">Panel principal</a>
				</span>
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
</div>