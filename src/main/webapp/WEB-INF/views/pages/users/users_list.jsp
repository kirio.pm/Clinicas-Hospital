<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="content-wrapper">
    <div class="container-fluid">

		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href='<c:url value = "/"></c:url>'>Inicio</a></li>
		  <li class="breadcrumb-item active">Gesti�n de personal </li>
		</ol>

		<h1 class="page-header">Gesti�n de personal</h1>
		
		
		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-table"></i> Usuarios Registrados en el Sistema
			</div>
			
			<div class="card-block">
				<div class="table-responsive">
					<table class="table table-bordered" width="100%" id="dataTable"
						cellspacing="0">
						<thead>
							<tr>
								<th>Usuario</th>
								<th>Apellidos</th>
								<th>Nombre</th>
								<th>Email</th>
								<th>Fecha de inicio</th>
								<th>Ultimo ingreso</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Usuario</th>
								<th>Apellidos</th>
								<th>Nombre</th>
								<th>Email</th>
								<th>Fecha de inicio</th>
								<th>Ultimo ingreso</th>
								<th>Acciones</th>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach items="${users}" var="user">
								<tr>
									<td align="center">${user.username}</td>
									<td>${user.empleado.apellidoEmpleado}</td>
									<td>${user.empleado.nombreEmpleado}</td>
									<td>${user.empleado.emilEmp}</td>
									<td>${user.joining_date}</td>
									<td>${user.last_login}</td>
									<td align="center"><sec:authorize
											access="hasRole('ADMIN')">

											<a href="<c:url value='editarUsuario-${user.id}' />"
												data-toggle="tooltip" title="Editar"><i
												class="fa fa-pencil-square-o fa-2x"></i></a>
										</sec:authorize><sec:authorize
											access="hasRole('ADMIN') or hasRole('DBA')">
										</sec:authorize>  <sec:authorize access="hasRole('ADMIN')">
											<c:if test="${user.username != loggedinuser}">
												<a href="<c:url value='eliminarUsuario-${user.username}' />"
													data-toggle="tooltip" title="Eliminar"><i
													class="fa fa-trash fa-2x"></i></a>
											</c:if>
										</sec:authorize></td>

								</tr>
							</c:forEach>


						</tbody>

					</table>
				</div>
				 
				 <sec:authorize access="hasRole('ADMIN')">
			<div class="well">
				<a class="btn btn-primary " href="<c:url value='/usuarios/nuevoUsuario' />">Crear Nuevo
					Usuario</a> 
						</div>
		</sec:authorize>

			</div>

		</div>
	</div>
</div>
