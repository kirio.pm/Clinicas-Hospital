<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div class="page-content-wrapper">
	<div class="container-fluid">
	<div class="col-lg-12">
	
	<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href='<c:url value = "/"></c:url>'>Inicio</a></li>
  <li class="breadcrumb-item"><a href='<c:url value = "/user/"></c:url>'>Gesti�n de personal de ventanilla</a></li>
  <li class="breadcrumb-item active">Lista de personal sin usuario</li>
</ol>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Gesti�n de Personal</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-table"></i> Personal sin Usuario
			</div>
		<div class="card-block">
				<div class="table-responsive">
					<table class="table table-bordered" width="100%" id="dataTable"
						cellspacing="0">
			<thead>
			<tr>
				<th>ID</th>
				<th>Carnet</th>
				<th>Cargo</th>
				<th>email</th>
				<th>Nombre</th>
				<th>Apellido</th>
			</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach items="${personals}" var="personal">
					<tr>
						<td align="center">${personal.id}</td>
						<td>${personal.carnet}</td>
						<td>${personal.charge}</td>
						<td>${personal.email}</td>
						<td>${personal.firstname}</td>
						<td>${personal.lastname}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
		</div>
	</div>
	</div>
	</div>
	</div>