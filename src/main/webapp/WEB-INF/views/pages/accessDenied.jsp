<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Acceso denegado</title>
	<link href='<c:url value="/static/bootstrap/bootstrap4/css/bootstrap.min.css"></c:url>' rel="stylesheet">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">	
</head>
<body>
<div class="row align-items-center">
    <div class="col">
	<div class="card card-inverse card-danger text-center">
  <div class="card-block">
    <blockquote class="card-blockquote">
     <h4> <p>Lo sentimos ${loggedinuser}, Pero tu no tienes permisos de accesar a esta pagina.</p></h4> <a class="material-icons" href='<c:url value="/" />'style="font-size:48px;">cancelar</a>
      <footer><a class="btn btn-secondary btn-lg" href='<c:url value="/logout" />'>Salir</a></span></footer>
    </blockquote>
  </div>
</div>
</div>
</div>
</body>
</html>