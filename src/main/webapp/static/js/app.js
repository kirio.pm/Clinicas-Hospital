//JAVASCRIPT GENERAL PARA EL SISTEMA
//
$(document).ready(function(){

	//Desabilita el comportamiento por defecto de las etiquetas <li> cuando estas tienen en su src = "#"
	//Para desabilitar este comportamiento agregar class = "disabled_href"
    $('.disabled_href').click(function(e){
        e.preventDefault();
    });
});
/*
 * Token and header for post ajax request
 */
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

function postRequest(url, data,successMessage){
	return $.post({
        url: url,
        data: data,
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000,
	})
	.done(function(){
        if (successMessage !== undefined) {
        	showSnackbar(successMessage,1500,true);
        }
	})
	.fail(function(xhr, status, errorThrown){
		$.snackbar({content: "Ha ocurrido un error " + xhr.status +":" + errorThrown + "  Estado: " + status});
	})
}

function getRequest(url, data, successMessage){
		return	$.get({
				url: url,
				data: data,
				contentType: "application/json",
				dataType: 'json',
				timeout: 60000,
			})
	.done(function() {
        if (successMessage !== undefined) {
        	showSnackbar(successMessage,1500,true);
        }
    })
	.fail(function(xhr, status, errorThrown){
		$.snackbar({content: "Ha ocurrido un error " + xhr.status +":" + errorThrown + "  Estado: " + status});
	});
};


function showSnackbar(message,time,html){
	$.snackbar({
		content: message,
		timeout: time,
		htmlAllowed: html
	});
}

function showToast(message,time,html){
	$.snackbar({
		content: message,
		timeout: time,
		style: "toast",
		htmlAllowed: html
	});
}

jQuery(".date").datetimepicker({
    format:'Y/m/d'
});
