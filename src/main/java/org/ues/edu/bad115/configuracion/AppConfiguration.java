package org.ues.edu.bad115.configuracion;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;
import org.ues.edu.bad115.converter.RoleToProfileConverter;


@Configuration
@ComponentScan(basePackages="org.ues.edu.bad115")
@EnableWebMvc
public class AppConfiguration extends WebMvcConfigurerAdapter{
	@Autowired
	RoleToProfileConverter roleToProfileConverter;
	
	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(new String[] { "/WEB-INF/views/**/tiles.xml","/WEB-INF/views/**/luis/tiles.xml","/WEB-INF/views/**/tony/tiles.xml","/WEB-INF/views/**/jose/tiles.xml","/WEB-INF/views/**/kirio/tiles.xml","/WEB-INF/views/**/jorge/tiles.xml"});
		tilesConfigurer.setCheckRefresh(true);
		tilesConfigurer.setDefinitionsFactoryClass(CustomLocaleDefinitionsFactory.class);
		return tilesConfigurer;
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		TilesViewResolver viewResolver = new TilesViewResolver();
		registry.viewResolver(viewResolver);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(roleToProfileConverter);
	}

	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer matcher) {
		matcher.setUseRegisteredSuffixPatternMatch(true);
	}
	
	@Bean(name = "multipartResolver")
	 public CommonsMultipartResolver commonsMultipartResolver(){
	        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
	        commonsMultipartResolver.setDefaultEncoding("utf-8");
	        commonsMultipartResolver.setMaxUploadSize(50000000);// Maximum Upload
	        commonsMultipartResolver.setMaxInMemorySize(9048576); //Maximum Size Of File In Memory (In Bytes) 
	        return commonsMultipartResolver;
	    }
	@Bean
	public JavaMailSender getMailSender(){
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		
		//Using gmail.
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername("pm10037@ues.edu.sv");
		mailSender.setPassword("");
		
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.starttls.enable", "true");
		javaMailProperties.put("mail.smtp.auth", "true");
		javaMailProperties.put("mail.transport.protocol", "smtp");
		javaMailProperties.put("mail.debug", "false");
		javaMailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		javaMailProperties.put("mail.smtp.socketFactory.port","465");
		javaMailProperties.put("mail.smtp.starttls.enable","true");
		
		
		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}
	
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(500000);
	    return multipartResolver;
	}
	
}
