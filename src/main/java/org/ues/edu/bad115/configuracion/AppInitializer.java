package org.ues.edu.bad115.configuracion;

import javax.servlet.ServletContext;

import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected void registerContextLoaderListener(ServletContext context) {
		context.addListener(RequestContextListener.class);
		super.registerContextLoaderListener(context);
	}

	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {};
	}
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {AppConfiguration.class};
	}
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
}

