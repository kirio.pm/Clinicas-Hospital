package org.ues.edu.bad115.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	private static final String[] PUBLIC_PAGES = {"/","/index","/help"};
	
	private static final String[] ADMINISTRADOR_PAGES = {"/usuarios/", "/usuarios/listaUsuarios","/usuarios/nuevoUsuario","/usuarios/editarUsuario-*","/usuarios/eliminarUsuario-*"
												,"/empleados/", "/empleados/listaEmpleados","/empleados/nuevoEmpleados","/empleados/editarEmp-*"
												, "/doctor/","/doctor/listaDoctor","/doctor/nuevoDoctor","/doctor/editarDoc-*","/log/"};
	private static final String[] RECEPCIONISTA_PAGES = {"/paciente/","/hclinico/"};
	private static final String[] ENFERMERA_PAGES = {"/consultaMedica/nuevaConsulta/*"};
	private static final String[] DOCTOR_PAGES = {"/consultaMedica/","/hospitalizacion/","/cirugia/","/cirugia/nuevaCirugia"};
	private static final String[] SECRETARIA_PAGES = {"/expediente/","/cita/"};
	
	@Autowired
	@Qualifier("customUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private PersistentTokenRepository tokenRepository;
	
	@Autowired
	private AuthenticationFailureHandler customAuthenticationFailureHandler;
	
	@Autowired
	private AuthenticationSuccessHandler customAuthenticationSuccessHandler;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers(PUBLIC_PAGES).access("hasRole('ADMIN') or hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('SECRETARY') or hasRole('NURSE')")
				.antMatchers(RECEPCIONISTA_PAGES).access("hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('NURSE')")
				.antMatchers(DOCTOR_PAGES).access("hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('NURSE')")
				.antMatchers(ADMINISTRADOR_PAGES).access("hasRole('ADMIN')")
				.antMatchers(SECRETARIA_PAGES).access("hasRole('ADMIN') or hasRole('RECEPCTION') or hasRole('DOCTOR') or hasRole('SECRETARY') or hasRole('NURSE')")
				.antMatchers(ENFERMERA_PAGES).access("hasRole('NURSE') or hasRole('RECEPCTION') or hasRole('ADMIN')")
				.and().formLogin().loginPage("/login").failureHandler(customAuthenticationFailureHandler).successHandler(customAuthenticationSuccessHandler)
				.loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password").and()
				.rememberMe().rememberMeParameter("remember-me").tokenRepository(tokenRepository)
				.tokenValiditySeconds(43200).and().exceptionHandling().accessDeniedPage("/Access_Denied").and().csrf();
	}
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}
	@Bean
	public PersistentTokenBasedRememberMeServices getPersistentTokenBasedRememberMeServices() {
		PersistentTokenBasedRememberMeServices tokenBasedservice = new PersistentTokenBasedRememberMeServices(
				"remember-me", userDetailsService, tokenRepository);
		return tokenBasedservice;
	}
	@Bean
	public AuthenticationTrustResolver getAuthenticationTrustResolver() {
		return new AuthenticationTrustResolverImpl();
	}
	


}