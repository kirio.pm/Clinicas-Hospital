package org.ues.edu.bad115.security;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.modelos.User;
import org.ues.edu.bad115.servicios.LoginAttemptService;
import org.ues.edu.bad115.servicios.UserService;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{
	
	static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	private LoginAttemptService logingAttemptService;
	
	@Autowired
    private HttpServletRequest request;
	
	@Autowired
	private JavaMailSender getMailSender;

	@Transactional
	public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException{
		String ip = getClientIP();	
		User user = userService.findByUsername(username);
		
		if(username.equalsIgnoreCase("cleanup")) {
			logger.info("CleanUp Cache.....................................");
			logingAttemptService.clearCache();
		}
		
		if(user==null){
			throw new UsernameNotFoundException("Username not found");
		}
		
		if(logingAttemptService.isBlocked(ip) && (user!=null)) {
			getMailSender.send(new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, true, "UTF-8");
					mimeMsgHelperObj.setTo(user.getEmpleado().getEmilEmp());				
					mimeMsgHelperObj.setText("\n"+"SE LE HA BLOQUEADO LA CUENTA POR HABER FALLADO LOS TRES INTENTOS, SE LE HABILITARA EN 3 MINUTOS");
					mimeMsgHelperObj.setSubject("BLOQUEO DE CUENTA");
					mimeMsgHelperObj.addCc("luis.jca96@gmail.com");
					
				}
			});
			throw new RuntimeException("blocked");
		}
		
		return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),true,true,true,true,getGrantedAuthorities(user));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for(Profile profile:user.getProfiles()){
			logger.info("User profile: {}",profile);
			authorities.add(new SimpleGrantedAuthority("ROLE_"+profile.getType()));
		}
		logger.info("authorities; {}",authorities);
		updateUserLastLogging(user.getId());
		return authorities;
	}

	private void updateUserLastLogging(int id){
		userService.updateUserLastLogging(id);
	}
	
	private String getClientIP() {
	    String xfHeader = request.getHeader("X-Forwarded-For");
	    
	    if (xfHeader == null){
	        return request.getRemoteAddr();
	    }	    
	    return xfHeader.split(",")[0];
	}

}

