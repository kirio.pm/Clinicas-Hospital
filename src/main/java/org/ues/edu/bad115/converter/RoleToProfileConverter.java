package org.ues.edu.bad115.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.servicios.ProfileService;

@Component
public class RoleToProfileConverter implements Converter<Object, Profile> {
	
	static final Logger logger = LoggerFactory.getLogger(RoleToProfileConverter.class);
	
	@Autowired
	ProfileService profileService;

	@Override
	public Profile convert(Object element) {
		Integer id = Integer.parseInt((String)element);
		Profile profile = profileService.findById(id);
		logger.info("Profile: {}",profile);
		return profile;
	}
}

