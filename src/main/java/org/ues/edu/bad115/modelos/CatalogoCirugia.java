/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="CATALOGOCIRUGIA")

public class CatalogoCirugia  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cat_ciru", nullable = false)
    private int idCatCiru;
    
    @Size(max = 1024)
    @Column(name = "nombre_ciru", length = 1024)
    private String nombreCiru;
    
    @Size(max = 1024)
    @Column(name = "procediemiento_ciru", length = 1024)
    private String procediemientoCiru;
    
    @Size(max = 1024)
    @Column(name = "tipo_ciru", length = 1024)
    private String tipoCiru;
    
    @Column(name = "costo_cirugia", precision = 8, scale = 2)
    private int costoCirugia;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatCiru", orphanRemoval = true)
    private List<Cirugia> cirugiaList = new ArrayList<>();

    

    public int getIdCatCiru() {
        return idCatCiru;
    }

    public void setIdCatCiru(int idCatCiru) {
        this.idCatCiru = idCatCiru;
    }

    public String getNombreCiru() {
        return nombreCiru;
    }

    public void setNombreCiru(String nombreCiru) {
        this.nombreCiru = nombreCiru;
    }

    public String getProcediemientoCiru() {
        return procediemientoCiru;
    }

    public void setProcediemientoCiru(String procediemientoCiru) {
        this.procediemientoCiru = procediemientoCiru;
    }

    public String getTipoCiru() {
        return tipoCiru;
    }

    public void setTipoCiru(String tipoCiru) {
        this.tipoCiru = tipoCiru;
    }

    public int getCostoCirugia() {
        return costoCirugia;
    }

    public void setCostoCirugia(int costoCirugia) {
        this.costoCirugia = costoCirugia;
    }

    
    public List<Cirugia> getCirugiaList() {
        return cirugiaList;
    }

    public void setCirugiaList(List<Cirugia> cirugiaList) {
        this.cirugiaList = cirugiaList;
    }

    
    
}
