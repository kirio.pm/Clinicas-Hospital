/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="HOSPITALIZACION")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_HOSPITALIZACION_PROCE",
	        procedureName="ADD_HOSPITALIZACION_PROCE",
	        resultClasses = { Hospitalizacion.class },
	        parameters={
	        	@StoredProcedureParameter(name="FEINI", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="FEFIN", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDDOC", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDPAC", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDSAL", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_HOSPITALIZACION_PROCE",
	        procedureName="UPD_HOSPITALIZACION_PROCE",
	        resultClasses = { Hospitalizacion.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDHOS", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="FEINI", type=LocalDate.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="FEFIN", type=LocalDate.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDDOC", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDPAC", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDSAL", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Hospitalizacion  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_hospitalizacion", nullable = false)
    private int idHospitalizacion;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_inicio_hosp")
    private LocalDate fechaInicioHosp;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_fin_hosp")
    private LocalDate fechaFinHosp;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idHospitalizacion",orphanRemoval=true)
    private List<BitacoraProcedimientos> bitacoraProcedimientosList= new ArrayList<>();
    
    //@OneToMany(cascade=CascadeType.ALL,mappedBy = "idHopitalizacion",orphanRemoval=true)
    //private List<Paciente> pacienteList = new ArrayList<>();
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_doctor", referencedColumnName = "id_doctor", nullable = false)
    private Doctor idDoctor;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente", nullable=false)
    private Paciente idPaciente;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_sala", referencedColumnName = "id_sala", nullable = false)
    private Sala idSala;
    
    public int getIdHospitalizacion() {
		return idHospitalizacion;
	}

	public void setIdHospitalizacion(int idHospitalizacion) {
		this.idHospitalizacion = idHospitalizacion;
	}

	public LocalDate getFechaInicioHosp() {
		return fechaInicioHosp;
	}

	public void setFechaInicioHosp(LocalDate fechaInicioHosp) {
		this.fechaInicioHosp = fechaInicioHosp;
	}

	public LocalDate getFechaFinHosp() {
		return fechaFinHosp;
	}

	public void setFechaFinHosp(LocalDate fechaFinHosp) {
		this.fechaFinHosp = fechaFinHosp;
	}

	public List<BitacoraProcedimientos> getBitacoraProcedimientosList() {
        return bitacoraProcedimientosList;
    }

    public void setBitacoraProcedimientosList(List<BitacoraProcedimientos> bitacoraProcedimientosList) {
        this.bitacoraProcedimientosList = bitacoraProcedimientosList;
    }

    public Doctor getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Doctor idDoctor) {
        this.idDoctor = idDoctor;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Sala getIdSala() {
        return idSala;
    }

    public void setIdSala(Sala idSala) {
        this.idSala = idSala;
    }

    
    
}
