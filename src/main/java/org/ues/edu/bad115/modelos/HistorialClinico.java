/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.NamedStoredProcedureQuery;



/**
 *
 * @author ADMIN
 */
@Entity	
@Table(name="HISTORIALCLINICO")
@NamedStoredProcedureQuery(
        name="ADD_HISTO_DOS_PROCE",
        procedureName="ADD_HISTO_DOS_PROCE",
        resultClasses = { HistorialClinico.class },
        parameters={
            @StoredProcedureParameter(name="ENFERMEDADES_PADECIDAS", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="TRATAMIENTO_PREVENTIVO", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_PACIENTE", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="apellidoPadre", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="direccion", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="fechaNacimiento", type=LocalDate.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="historialDeEnfermedades", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="nombrePadre", type=String.class, mode=ParameterMode.IN)
        }
)
public class HistorialClinico  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_historial_clinico", nullable = false)
    private int idHistorialClinico;
    
    @Size(max = 1024)
    @Column(name = "enfermedades_padecidas", length = 1024)
    private String enfermedadesPadecidas;
    
    @Size(max = 1024)
    @Column(name = "tratamiento_preventivo", length = 1024)
    private String tratamientoPreventivo;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_datos_familiares", referencedColumnName = "id_datos_familiares")
    private DatosFamiliares idDatosFamiliares;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente", nullable = false)
    private Paciente idPaciente;

    

    public int getIdHistorialClinico() {
        return idHistorialClinico;
    }

    public void setIdHistorialClinico(int idHistorialClinico) {
        this.idHistorialClinico = idHistorialClinico;
    }

    public String getEnfermedadesPadecidas() {
        return enfermedadesPadecidas;
    }

    public void setEnfermedadesPadecidas(String enfermedadesPadecidas) {
        this.enfermedadesPadecidas = enfermedadesPadecidas;
    }

    public String getTratamientoPreventivo() {
        return tratamientoPreventivo;
    }

    public void setTratamientoPreventivo(String tratamientoPreventivo) {
        this.tratamientoPreventivo = tratamientoPreventivo;
    }

    public DatosFamiliares getIdDatosFamiliares() {
        return idDatosFamiliares;
    }

    public void setIdDatosFamiliares(DatosFamiliares idDatosFamiliares) {
        this.idDatosFamiliares = idDatosFamiliares;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    
    
}
