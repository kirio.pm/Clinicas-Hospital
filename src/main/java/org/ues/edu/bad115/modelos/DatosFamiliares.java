/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="DATOSFAMILIARES")
@NamedStoredProcedureQuery(
        name="ADD_DATA_PROCE",
        procedureName="ADD_DATA_PROCE",
        resultClasses = { Paciente.class },
        parameters={
        	@StoredProcedureParameter(name="apellidoPadre", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="direccion", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="fechaNacimiento", type=LocalDate.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="historialDeEnfermedades", type=String.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="nombrePadre", type=String.class, mode=ParameterMode.IN)
        }
)
public class DatosFamiliares  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_datos_familiares", nullable = false)
    private int idDatosFamiliares;
    
    @Size(max = 1024)
    @Column(name = "nombre_padre", length = 1024)
    private String nombrePadre;
    
    @Size(max = 1024)
    @Column(name = "apellido_padre", length = 1024)
    private String apellidoPadre;
    
    @Size(max = 1024)
    @Column(length = 1024)
    private String direccion;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
   
    @Size(max = 1024)
    @Column(name = "historial_de_enfermedades", length = 1024)
    private String historialDeEnfermedades;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatosFamiliares",orphanRemoval=true)
    private List<HistorialClinico> historialClinicoList = new ArrayList<>();

    

    public int getIdDatosFamiliares() {
        return idDatosFamiliares;
    }

    public void setIdDatosFamiliares(int idDatosFamiliares) {
        this.idDatosFamiliares = idDatosFamiliares;
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public void setNombrePadre(String nombrePadre) {
        this.nombrePadre = nombrePadre;
    }

    public String getApellidoPadre() {
        return apellidoPadre;
    }

    public void setApellidoPadre(String apellidoPadre) {
        this.apellidoPadre = apellidoPadre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    

    public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getHistorialDeEnfermedades() {
        return historialDeEnfermedades;
    }

    public void setHistorialDeEnfermedades(String historialDeEnfermedades) {
        this.historialDeEnfermedades = historialDeEnfermedades;
    }

    
    public List<HistorialClinico> getHistorialClinicoList() {
        return historialClinicoList;
    }

    public void setHistorialClinicoList(List<HistorialClinico> historialClinicoList) {
        this.historialClinicoList = historialClinicoList;
    }

    
    
}
