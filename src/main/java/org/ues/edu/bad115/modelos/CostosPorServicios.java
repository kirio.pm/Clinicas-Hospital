/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="COSTOSPORSERVICIOS")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_COS_SER_PROCE",
	        procedureName="ADD_COS_SER_PROCE",
	        resultClasses = { CostosPorServicios.class },
	        parameters={
	            @StoredProcedureParameter(name="FECO", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="UM", type=String.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_COS_SER_PROCE",
	        procedureName="UPD_COS_SER_PROCE",
	        resultClasses = { CostosPorServicios.class },
	        parameters={
	        	@StoredProcedureParameter(name="ID_COS", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="FECO", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="UM", type=String.class, mode=ParameterMode.IN)
	        }
	)
})
public class CostosPorServicios  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_costo", nullable = false)
    private int idCosto;
    
    @Size(max = 1024)
    @Column(length = 1024)
    private String unidadMonetaria;
    
    @Column(name="total_costo", precision = 8, scale = 2)
    private double total;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_costo")
    private LocalDate fechaCosto;
    

    public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public LocalDate getFechaCosto() {
		return fechaCosto;
	}

	public void setFechaCosto(LocalDate fechaCosto) {
		this.fechaCosto = fechaCosto;
	}

	public int getIdCosto() {
        return idCosto;
    }

    public void setIdCosto(int idCosto) {
        this.idCosto = idCosto;
    }

    public String getUnidadMonetaria() {
        return unidadMonetaria;
    }

    public void setUnidadMonetaria(String unidadMonetaria) {
        this.unidadMonetaria = unidadMonetaria;
    }

    
}
