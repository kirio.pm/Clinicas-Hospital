/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CLINICA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_CLINICA_PROCE",
	        procedureName="ADD_CLINICA_PROCE",
	        resultClasses = { Clinica.class },
	        parameters={
	        		@StoredProcedureParameter(name="DIRECION_CLI", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="NOMBRE_CLI", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="TELELFONO_CLI", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="ID_REGION", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_CLINICA_PROCE",
	        procedureName="UPD_CLINICA_PROCE",
	        resultClasses = { Clinica.class },
	        parameters={
	        		@StoredProcedureParameter(name="ID_CLINICA", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="DIRECION_CLI", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="NOMBRE_CLI", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="TELELFONO_CLI", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="ID_REGION", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Clinica  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_clinica", nullable = false)
    private int idClinica;
    
    @Size(max = 1024)
    @Column(name = "nombre_cli", length = 1024)
    private String nombreCli;
    
    @Size(max = 1024)
    @Column(name = "direccion_cli", length = 1024)
    private String direccionCli;
    
    @Size(max = 1024)
    @Column(name = "telelfono_cli", length = 1024)
    private String telelfonoCli;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idClinica", orphanRemoval= true)
    private List<CatalogoCitas> catalogoCitasList = new ArrayList<>();

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_region", referencedColumnName = "id_region")
    private Region idRegion;

    public Region getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Region idRegion) {
		this.idRegion = idRegion;
	}

	public int getIdClinica() {
        return idClinica;
    }

    public void setIdClinica(int idClinica) {
        this.idClinica = idClinica;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getDireccionCli() {
        return direccionCli;
    }

    public void setDireccionCli(String direccionCli) {
        this.direccionCli = direccionCli;
    }

    public String getTelelfonoCli() {
        return telelfonoCli;
    }

    public void setTelelfonoCli(String telelfonoCli) {
        this.telelfonoCli = telelfonoCli;
    }

   
    public List<CatalogoCitas> getCatalogoCitasList() {
        return catalogoCitasList;
    }

    public void setCatalogoCitasList(List<CatalogoCitas> catalogoCitasList) {
        this.catalogoCitasList = catalogoCitasList;
    }

    
    
}
