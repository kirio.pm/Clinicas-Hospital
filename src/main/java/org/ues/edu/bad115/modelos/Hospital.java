/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="HOSPITAL")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_HOS_PROCE",
	        procedureName="ADD_HOS_PROCE",
	        resultClasses = { Hospital.class },
	        parameters={
	        		@StoredProcedureParameter(name="DIRHOS", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="NOMHOS", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="TELEHOS", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="ID_REG", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_HOS_PROCE",
	        procedureName="UPD_HOS_PROCE",
	        resultClasses = { Hospital.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDHOS", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="DIRHOS", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="NOMHOS", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="TELEHOS", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDRE", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Hospital  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_hospital", nullable = false)
    private int idHospital;
    
    @Size(max = 1024)
    @Column(name = "nombre_hos", length = 1024)
    private String nombreHos;
    
    @Size(max = 1024)
    @Column(name = "direccion_hos", length = 1024)
    private String direccionHos;
    
    @Size(max = 1024)
    @Column(name = "telefono_hos", length = 1024)
    private String telefonoHos;
    
    @OneToMany(cascade= CascadeType.ALL,mappedBy = "idHospital",orphanRemoval=true)
    private List<Sala> salaList = new ArrayList<>();

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_region", referencedColumnName = "id_region")
    private Region idRegion;

    public Region getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Region idRegion) {
		this.idRegion = idRegion;
	}

	public int getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(int idHospital) {
        this.idHospital = idHospital;
    }

    public String getNombreHos() {
        return nombreHos;
    }

    public void setNombreHos(String nombreHos) {
        this.nombreHos = nombreHos;
    }

    public String getDireccionHos() {
        return direccionHos;
    }

    public void setDireccionHos(String direccionHos) {
        this.direccionHos = direccionHos;
    }

    public String getTelefonoHos() {
        return telefonoHos;
    }

    public void setTelefonoHos(String telefonoHos) {
        this.telefonoHos = telefonoHos;
    }

    
    public List<Sala> getSalaList() {
        return salaList;
    }

    public void setSalaList(List<Sala> salaList) {
        this.salaList = salaList;
    }

    
    
}
