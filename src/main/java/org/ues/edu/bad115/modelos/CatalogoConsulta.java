/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;	

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="CATALOGOCONSULTA")

public class CatalogoConsulta  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cat_con", nullable = false)
    private int idCatCon;
    
    @Size(max = 1024)
    @Column(name = "tipo_con", length = 1024)
    private String tipoCon;
    
    @Size(max = 7)
    @Column(name = "cod_con", length = 7)
    private String codCon;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatCon", orphanRemoval = true)
    private List<ConsultaMedica> consultaMedicaList = new ArrayList<>();


    public int getIdCatCon() {
        return idCatCon;
    }

    public void setIdCatCon(int idCatCon) {
        this.idCatCon = idCatCon;
    }

    public String getTipoCon() {
        return tipoCon;
    }

    public void setTipoCon(String tipoCon) {
        this.tipoCon = tipoCon;
    }

    public String getCodCon() {
        return codCon;
    }

    public void setCodCon(String codCon) {
        this.codCon = codCon;
    }

    
    public List<ConsultaMedica> getConsultaMedicaList() {
        return consultaMedicaList;
    }

    public void setConsultaMedicaList(List<ConsultaMedica> consultaMedicaList) {
        this.consultaMedicaList = consultaMedicaList;
    }

    
    
}
