/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="CATALOGODIAGNOSTICO")

public class CatalogoDiagnostico  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cata_diag", nullable = false)
    private int idCataDiag;
    
    @Size(max = 3)
    @Column(name = "cod_enf", length = 3)
    private String codEnf;
    
    @Size(max = 1024)
    @Column(name = "nombre_enf", length = 1024)
    private String nombreEnf;
    
    
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDiagnostico", orphanRemoval = true)
    private List<Diagnostico> diagnosticoList = new ArrayList<>();

   

    public int getIdCataDiag() {
        return idCataDiag;
    }

    public void setIdCataDiag(int idCataDiag) {
        this.idCataDiag = idCataDiag;
    }

    public String getCodEnf() {
        return codEnf;
    }

    public void setCodEnf(String codEnf) {
        this.codEnf = codEnf;
    }

    public String getNombreEnf() {
        return nombreEnf;
    }

    public void setNombreEnf(String nombreEnf) {
        this.nombreEnf = nombreEnf;
    }

	public List<Diagnostico> getDiagnosticoList() {
		return diagnosticoList;
	}

	public void setDiagnosticoList(List<Diagnostico> diagnosticoList) {
		this.diagnosticoList = diagnosticoList;
	}

    
    
}
