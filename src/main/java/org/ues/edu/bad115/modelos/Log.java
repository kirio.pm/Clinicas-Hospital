/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;



import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "Log")
public class Log  {

	
	@Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Log", nullable = false)
    private int idLog;
    
    @Size(max = 1024)
    @Column(name = "accion", length = 1024)
    private String accion;
    
    @Size(max = 1024)
    @Column(name = "usuario", length = 1024)
    private String usuario;
    
    @Size(max = 1024)
    @Column(name = "tablas_afectadas", length = 1024)
    private String tablasAfectadas;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_log")
    private LocalDate fechaLog;
    	
	
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	int getIdLog() {
		return idLog;
	}

	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getTablasAfectadas() {
		return tablasAfectadas;
	}

	public void setTablasAfectadas(String tablasAfectadas) {
		this.tablasAfectadas = tablasAfectadas;
	}

	public LocalDate getFechaLog() {
		return fechaLog;
	}

	public void setFechaLog(LocalDate fechaLog) {
		this.fechaLog = fechaLog;
	}

	
    
    
    
   

   

        
}
