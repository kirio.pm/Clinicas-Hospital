/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="EXAMEN")

public class Examen  {
   
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXAMEN_SEQ")
    @SequenceGenerator(sequenceName = "examen_seq", allocationSize = 1, name = "EXAMEN_SEQ")
    @Column(name = "id_examen", nullable = false)
    private int idExamen;
   
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_rececion_exa")
    private LocalDate fechaRececionExa;
    
    @Size(max = 1024)
    @Column(name = "nombre_original", length = 1024)
    private String nombreOriginal;
    
    @Size(max = 1024)
    @Column(name = "formato", length = 1024)
    private String formato;
    
    @Size(max = 1024)
    @Column(name = "ruta_archivo", length = 1024)
    private String rutaArchivo;
    
    @Column(name = "tamanio")
    private Long tamanio;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_cat_examen", referencedColumnName = "id_cat_examen", nullable = false)
    private CatalogoDeExamenes idCatExamen;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_consulta", referencedColumnName = "id_consulta", nullable = false)
    private ConsultaMedica idConsultaMedica;
    
    public int getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(int idExamen) {
        this.idExamen = idExamen;
    }

    public LocalDate getFechaRececionExa() {
        return fechaRececionExa;
    }

    public void setFechaRececionExa(LocalDate fechaRececionExa) {
        this.fechaRececionExa = fechaRececionExa;
    }

    public CatalogoDeExamenes getIdCatExamen() {
        return idCatExamen;
    }

    public void setIdCatExamen(CatalogoDeExamenes idCatExamen) {
        this.idCatExamen = idCatExamen;
    }

	public String getNombreOriginal() {
		return nombreOriginal;
	}

	public void setNombreOriginal(String nombreOriginal) {
		this.nombreOriginal = nombreOriginal;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public Long getTamanio() {
		return tamanio;
	}

	public void setTamanio(Long tamanio) {
		this.tamanio = tamanio;
	}

	public ConsultaMedica getIdConsultaMedica() {
		return idConsultaMedica;
	}

	public void setIdConsultaMedica(ConsultaMedica idConsultaMedica) {
		this.idConsultaMedica = idConsultaMedica;
	}
    
        
}
