/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;



import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="CITAMEDICA")
@NamedStoredProcedureQueries({
@NamedStoredProcedureQuery(
        name="ADD_CIT_MED_PROCE",
        procedureName="ADD_CIT_MED_PROCE",
        resultClasses = { CitaMedica.class },
        parameters={
            @StoredProcedureParameter(name="FECHA_CITA", type=LocalDate.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="HORA_CITA", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="MEDIO_REGISTRO", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_CATALOGO_CITA", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_DOCTOR", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_PACIENTE", type=Integer.class, mode=ParameterMode.IN)
        }
),
@NamedStoredProcedureQuery(
        name="ADD_CIT_MED_UP_PROCE",
        procedureName="ADD_CIT_MED_UP_PROCE",
        resultClasses = { CitaMedica.class },
        parameters={
        	@StoredProcedureParameter(name="ID_CITA", type=Integer.class, mode=ParameterMode.IN),	
            @StoredProcedureParameter(name="FECHA_CITA", type=LocalDate.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="HORA_CITA", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="MEDIO_REGISTRO", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_CATALOGO_CITA", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_DOCTOR", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="ID_PACIENTE", type=Integer.class, mode=ParameterMode.IN)
        }
)
})
public class CitaMedica {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cita", nullable = false)
    private int idCita;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_cita")
    private LocalDate fechaCita;
    
    @Size(max = 1024)
    @Column(name = "hora_cita", length = 1024)
    private String horaCita;
    
    @Size(max = 1024)
    @Column(name = "medio_registro", length = 1024)
    private String medioRegistro;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_doctor", referencedColumnName = "id_doctor", nullable = false)
    private Doctor idDoctor ;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_catalogo_cita", referencedColumnName = "id_catalogo_cita", nullable = false)
    private CatalogoCitas idCatalogoCita;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente", nullable = false)
    private Paciente idPaciente;
    

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

  public LocalDate getFechaCita() {
		return fechaCita;
	}

	public void setFechaCita(LocalDate fechaCita) {
		this.fechaCita = fechaCita;
	}

	public String getHoraCita() {
		return horaCita;
	}

	public void setHoraCita(String horaCita) {
		this.horaCita = horaCita;
	}

	public Doctor getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(Doctor idDoctor) {
		this.idDoctor = idDoctor;
	}

	public CatalogoCitas getIdCatalogoCita() {
        return idCatalogoCita;
    }

    public void setIdCatalogoCita(CatalogoCitas idCatalogoCita) {
        this.idCatalogoCita = idCatalogoCita;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

	public String getMedioRegistro() {
		return medioRegistro;
	}

	public void setMedioRegistro(String medioRegistro) {
		this.medioRegistro = medioRegistro;
	}

	
    
    
}
