/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CATALOGOTERAPIA")

public class CatalogoTerapia  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cat_tera", nullable = false)
    private int idCatTera;
    
    @Size(max = 1024)
    @Column(name = "nombre_tera", length = 1024)
    private String nombreTera;
    
    @Column(name = "costo_tera", precision = 8, scale = 2)
    private double costoTera;
    
    @Size(max = 6)
    @Column(name = "codigo_tera", length = 6)
    private String codigoTera;
    
    @Size(max = 1024)
    @Column(name = "tipo_tera", length = 1024)
    private String tipoTera;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatTera", orphanRemoval = true)
    private List<Terapia> terapiaList = new ArrayList<>();

    

    public int getIdCatTera() {
        return idCatTera;
    }

    public void setIdCatTera(int idCatTera) {
        this.idCatTera = idCatTera;
    }

    public String getNombreTera() {
        return nombreTera;
    }

    public void setNombreTera(String nombreTera) {
        this.nombreTera = nombreTera;
    }

    public double getCostoTera() {
        return costoTera;
    }

    public void setCostoTera(double costoTera) {
        this.costoTera = costoTera;
    }

    public String getCodigoTera() {
        return codigoTera;
    }

    public void setCodigoTera(String codigoTera) {
        this.codigoTera = codigoTera;
    }

    public String getTipoTera() {
        return tipoTera;
    }

    public void setTipoTera(String tipoTera) {
        this.tipoTera = tipoTera;
    }

    
    public List<Terapia> getTerapiaList() {
        return terapiaList;
    }

    public void setTerapiaList(List<Terapia> terapiaList) {
        this.terapiaList = terapiaList;
    }

    
    
}
