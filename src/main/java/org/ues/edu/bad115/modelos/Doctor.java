/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="DOCTOR")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_DOC_PROCE",
	        procedureName="ADD_DOC_PROCE",
	        resultClasses = { Doctor.class },
	        parameters={
	            @StoredProcedureParameter(name="ESPECIALIDAD_DOC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="ID_EMPLEADO", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_DOC_PROCE",
	        procedureName="UDP_DOC_PROCE",
	        resultClasses = { Doctor.class },
	        parameters={
	        	@StoredProcedureParameter(name="ID_DOCTOR", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="ESPECIALIDAD_DOC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="ID_EMPLEADO", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Doctor  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_doctor", nullable = false)
    private int idDoctor;
    
    @Size(max = 1024)
    @Column(name = "especialidad_doc", length = 1024)
    private String especialidadDoc;
        
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDoctor", orphanRemoval=true)
    private List<Hospitalizacion> hospitalizacionList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDoctor", orphanRemoval=true)
    private List<CitaMedica> doctorList = new ArrayList<>();
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", nullable=false)
    private Empleado idEmpleado;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDoctor", orphanRemoval=true)
    private List<ConsultaMedica> consultaMedicaList = new ArrayList<>();

    

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getEspecialidadDoc() {
        return especialidadDoc;
    }

    public void setEspecialidadDoc(String especialidadDoc) {
        this.especialidadDoc = especialidadDoc;
    }

    public List<Hospitalizacion> getHospitalizacionList() {
        return hospitalizacionList;
    }

    public void setHospitalizacionList(List<Hospitalizacion> hospitalizacionList) {
        this.hospitalizacionList = hospitalizacionList;
    }

    

    public List<CitaMedica> getDoctorList() {
		return doctorList;
	}

	public void setDoctorList(List<CitaMedica> doctorList) {
		this.doctorList = doctorList;
	}

	public Empleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    
    public List<ConsultaMedica> getConsultaMedicaList() {
        return consultaMedicaList;
    }

    public void setConsultaMedicaList(List<ConsultaMedica> consultaMedicaList) {
        this.consultaMedicaList = consultaMedicaList;
    }

        
}
