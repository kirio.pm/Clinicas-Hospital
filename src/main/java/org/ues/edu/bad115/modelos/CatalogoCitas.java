/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CATALOGOCITAS")

public class CatalogoCitas  {
	
	
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_catalogo_cita", nullable = false)
    private int idCatalogoCita;
    
    @Size(max = 1024)
    @Column(name = "tipo_cita", length = 1024)
    private String tipoCita;
    
    @Size(max = 1024)
    @Column(name = "nombre_cita", length = 1024)
    private String nombreCita;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_clinica", referencedColumnName = "id_clinica", nullable = false)
    private Clinica idClinica;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatalogoCita")
    private List<CitaMedica> citaMedicaList = new ArrayList<>();

    

    public int getIdCatalogoCita() {
        return idCatalogoCita;
    }

    public void setIdCatalogoCita(int idCatalogoCita) {
        this.idCatalogoCita = idCatalogoCita;
    }

    public String getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(String tipoCita) {
        this.tipoCita = tipoCita;
    }

    public String getNombreCita() {
        return nombreCita;
    }

    public void setNombreCita(String nombreCita) {
        this.nombreCita = nombreCita;
    }

    public Clinica getIdClinica() {
        return idClinica;
    }

    public void setIdClinica(Clinica idClinica) {
        this.idClinica = idClinica;
    }

    
    public List<CitaMedica> getCitaMedicaList() {
        return citaMedicaList;
    }

    public void setCitaMedicaList(List<CitaMedica> citaMedicaList) {
        this.citaMedicaList = citaMedicaList;
    }

   
    
}
