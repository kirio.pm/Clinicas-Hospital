/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;


/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="EXPEDIENTECLINICO")
@NamedStoredProcedureQuery(
        name="addEmployeeProcedure",
        procedureName="INSERTAREXPEDIENTE",
        resultClasses = { ExpedienteClinico.class },
        parameters={
            @StoredProcedureParameter(name="ID_PACIENTE", type=Integer.class, mode=ParameterMode.IN)
        }
)


public class ExpedienteClinico {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_expediente", nullable = false)
    private int idExpediente;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_creacion")
    private Date fechaCreacion;
    
    @Size(max = 7)
    @Column(name = "cod_expediente", length = 7)
    private String codExpediente;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente", nullable = false)
    private Paciente idPaciente;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idExpediente", orphanRemoval=true)
    private List<ConsultaMedica> consultaMedicaList = new ArrayList<>();
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idExpediente", orphanRemoval = true)
    private List<ArchivosDeRegistro> archivoList = new ArrayList<>();

    

    public List<ArchivosDeRegistro> getArchivoList() {
		return archivoList;
	}

	public void setArchivoList(List<ArchivosDeRegistro> archivoList) {
		this.archivoList = archivoList;
	}

	public int getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(int idExpediente) {
        this.idExpediente = idExpediente;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCodExpediente() {
        return codExpediente;
    }

    public void setCodExpediente(String codExpediente) {
        this.codExpediente = codExpediente;
    }

    public List<ConsultaMedica> getConsultaMedicaList() {
        return consultaMedicaList;
    }

    public void setConsultaMedicaList(List<ConsultaMedica> consultaMedicaList) {
        this.consultaMedicaList = consultaMedicaList;
    }

	public Paciente getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Paciente idPaciente) {
		this.idPaciente = idPaciente;
	}

      
    
}
