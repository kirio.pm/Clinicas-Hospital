package org.ues.edu.bad115.modelos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="DIAGNOSTICO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_DIAG_PROCE",
	        procedureName="ADD_DIAG_PROCE",
	        resultClasses = { Diagnostico.class },
	        parameters={
	            @StoredProcedureParameter(name="IDCATDI", type=Integer.class, mode=ParameterMode.IN),	            
	            @StoredProcedureParameter(name="IDCO", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_DIAG_PROCE",
	        procedureName="UDP_DIAG_PROCE",
	        resultClasses = { Diagnostico.class },
	        parameters={
	        	@StoredProcedureParameter(name="IDDIAG", type=Integer.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="FEC", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCADI", type=Integer.class, mode=ParameterMode.IN),	            
	            @StoredProcedureParameter(name="IDCO", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Diagnostico {
	
	@Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_diagnostico", nullable = false)
    private int idDiagnostico;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_diag")
    private LocalDate fechaDiag;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cata_diag", referencedColumnName = "id_cata_diag", nullable=false)
    private CatalogoDiagnostico idCataDiag;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_consulta", referencedColumnName = "id_consulta", nullable = false)
    private ConsultaMedica idConsulta;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idDiagnostico", orphanRemoval = true)
    private List<TratamientoMedico> tratamientoMedicoList = new ArrayList<>();

	public int getIdDiagnostico() {
		return idDiagnostico;
	}

	public void setIdDiagnostico(int idDiagnostico) {
		this.idDiagnostico = idDiagnostico;
	}
	
	public LocalDate getFechaDiag() {
		return fechaDiag;
	}

	public void setFechaDiag(LocalDate fechaDiag) {
		this.fechaDiag = fechaDiag;
	}

	public CatalogoDiagnostico getIdCataDiag() {
		return idCataDiag;
	}

	public void setIdCataDiag(CatalogoDiagnostico idCataDiag) {
		this.idCataDiag = idCataDiag;
	}

	public ConsultaMedica getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(ConsultaMedica idConsulta) {
		this.idConsulta = idConsulta;
	}

	public List<TratamientoMedico> getTratamientoMedicoList() {
		return tratamientoMedicoList;
	}

	public void setTratamientoMedicoList(List<TratamientoMedico> tratamientoMedicoList) {
		this.tratamientoMedicoList = tratamientoMedicoList;
	}
	
	
}
