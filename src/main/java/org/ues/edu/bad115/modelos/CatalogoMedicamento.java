/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CATALOGOMEDICAMENTO")

public class CatalogoMedicamento  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cat_medi", nullable = false)
    private int idCatMedi;
    
    @Size(max = 1024)
    @Column(name = "nombre_medi", length = 1024)
    private String nombreMedi;
    
    @Size(max = 1024)
    @Column(name = "tipo_medi", length = 1024)
    private String tipoMedi;
    
    @Column(name = "costo_med", precision = 8, scale = 2)
    private double costoMed;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatMedi", orphanRemoval = true)
    private List<Medicamento> medicamentoList = new ArrayList<>();

    

    public int getIdCatMedi() {
        return idCatMedi;
    }

    public void setIdCatMedi(int idCatMedi) {
        this.idCatMedi = idCatMedi;
    }

    public String getNombreMedi() {
        return nombreMedi;
    }

    public void setNombreMedi(String nombreMedi) {
        this.nombreMedi = nombreMedi;
    }

    public String getTipoMedi() {
        return tipoMedi;
    }

    public void setTipoMedi(String tipoMedi) {
        this.tipoMedi = tipoMedi;
    }

    public double getCostoMed() {
        return costoMed;
    }

    public void setCostoMed(double costoMed) {
        this.costoMed = costoMed;
    }

   
    public List<Medicamento> getMedicamentoList() {
        return medicamentoList;
    }

    public void setMedicamentoList(List<Medicamento> medicamentoList) {
        this.medicamentoList = medicamentoList;
    }

    
    
}
