/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="EMPLEADO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_EMP_PROCE",
	        procedureName="ADD_EMP_PROCE",
	        resultClasses = { Empleado.class },
	        parameters={
	            @StoredProcedureParameter(name="APELLIDO_EMPLEADO", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="DUI_EMP", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="EMAIL_EMP", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="NOMBRE_EMPLEADO", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TELEFONO_EMP", type=String.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_EMP_PROCE",
	        procedureName="UPD_EMP_PROCE",
	        resultClasses = { Empleado.class },
	        parameters={
	        	@StoredProcedureParameter(name="ID_EMPLEADO", type=Integer.class, mode=ParameterMode.IN),	
	            @StoredProcedureParameter(name="APELLIDO_EMPLEADO", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="DUI_EMP", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="EMAIL_EMP", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="NOMBRE_EMPLEADO", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TELEFONO_EMP", type=String.class, mode=ParameterMode.IN)
	        }
	)
})
public class Empleado {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_empleado", nullable = false)
    private int idEmpleado;
    
    @Size(max = 1024)
    @Column(name = "nombre_empleado", length = 1024)
    private String nombreEmpleado;
    
    @Size(max = 1024)
    @Column(name = "apellido_empleado", length = 1024)
    private String apellidoEmpleado;
    
    @Size(max = 10)
    @Column(name = "dui_emp", length = 10)
    private String duiEmp;
    
    @Size(max = 10)
    @Column(name = "telefono_emp", length = 10)
    private String telefonoEmp;
    
    @Size(max = 1024)
    @Column(name = "emil_emp", length = 1024)
    private String emilEmp;
        
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEmpleado", orphanRemoval=true)
    private List<Doctor> doctorList = new ArrayList<>();
    
    @Valid
	@OneToOne(mappedBy = "empleado", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private User user;

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getApellidoEmpleado() {
        return apellidoEmpleado;
    }

    public void setApellidoEmpleado(String apellidoEmpleado) {
        this.apellidoEmpleado = apellidoEmpleado;
    }

    public String getDuiEmp() {
        return duiEmp;
    }

    public void setDuiEmp(String duiEmp) {
        this.duiEmp = duiEmp;
    }

    public String getTelefonoEmp() {
        return telefonoEmp;
    }

    public void setTelefonoEmp(String telefonoEmp) {
        this.telefonoEmp = telefonoEmp;
    }

    public String getEmilEmp() {
        return emilEmp;
    }

    public void setEmilEmp(String emilEmp) {
        this.emilEmp = emilEmp;
    }

  
    public List<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public void addUser(User user) {
		this.user = user;
		user.setEmpleado(this);
	}

	public void removeUser() {
		if (user != null) {
			user.setEmpleado(null);
		}
		this.user = null;
	}

    
    
}
