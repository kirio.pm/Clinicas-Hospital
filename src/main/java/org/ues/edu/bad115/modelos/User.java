package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USER_APP")
public class User {
	@Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
    @SequenceGenerator(sequenceName = "user_seq", allocationSize = 1, name = "USER_SEQ")
    @Column(name = "id_user", nullable = false)
    private int id;
	
    @Size(max = 1024)
    @Column(name = "username", length = 1024)
    private String username;
    
    @Size(max = 1024)
    @Column(name = "password", length = 1024)
    private String password;
    
    @org.hibernate.annotations.Type(type="true_false")
	@NotNull
	private boolean enabled;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "joining_date")
    private Date joining_date;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
    private Date last_login;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_lock_date")
    private Date lastLockDate;
    
    @OneToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "FK_empleado")
    private Empleado empleado;
    
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinTable(name = "USER_PROFILE", joinColumns = {
			@JoinColumn(name = "id_user") }, inverseJoinColumns = {
					@JoinColumn(name = "id_profile") })
	private List<Profile> profiles = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}

	public Date getLast_login() {
		return last_login;
	}

	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getLastLockDate() {
		return lastLockDate;
	}

	public void setLastLockDate(Date lastLockDate) {
		this.lastLockDate = lastLockDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
    

}
