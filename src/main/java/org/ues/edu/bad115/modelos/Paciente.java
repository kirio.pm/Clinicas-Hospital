/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="PACIENTE")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="add_paciente_proce",
	        procedureName="ADD_PACIENTE_PROCE",
	        resultClasses = { Paciente.class },
	        parameters={
	        	@StoredProcedureParameter(name="apellido_de_casado_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="apellido_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="direccion_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="email_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="estado_civil_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="fecha_nacimiento_pac", type=LocalDate.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="genero_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="nombre_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="numero_identificacion", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="responsable_por_emergencia_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="telefono_pac", type=String.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="ADD_PACIENTE_PROCE_UPDATE",
	        procedureName="ADD_PACIENTE_PROCE_UPDATE",
	        resultClasses = { Paciente.class },
	        parameters={
	        	@StoredProcedureParameter(name="id_paciente", type=Integer.class, mode=ParameterMode.IN),	
	        	@StoredProcedureParameter(name="apellido_de_casado_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="apellido_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="direccion_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="email_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="estado_civil_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="fecha_nacimiento_pac", type=LocalDate.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="genero_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="nombre_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="numero_identificacion", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="responsable_por_emergencia_pac", type=String.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="telefono_pac", type=String.class, mode=ParameterMode.IN)
	        }
	)
})
public class Paciente {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_paciente", nullable = false)
    private int idPaciente;
    
    @Size(max = 1024)
    @Column(name = "nombre_pac", length = 1024)
    private String nombrePac;
    
    @Size(max = 1024)
    @Column(name = "apellido_pac", length = 1024)
    private String apellidoPac;
    
    @Size(max = 1024)
    @Column(name = "genero_pac", length = 4)
    private String generoPac;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_nacimiento_pac")
    private LocalDate fechaNacimientoPac;
    
    @Size(max = 1024)
    @Column(name = "direccion_pac", length = 1024)
    private String direccionPac;
    
    @Size(max = 1024)
    @Column(name = "telefono_pac", length = 1024)
    private String telefonoPac;
    
    @Size(max = 1024)
    @Column(name = "responsable_por_emergencia_pac", length = 1024)
    private String responsablePorEmergenciaPac;
    
    @Size(max = 1024)
    @Column(name = "email_pac", length = 1024)
    private String emailPac;
    
    @Size(max = 1024)
    @Column(name = "apellido_de_casado_pac", length = 1024, nullable = true)
    private String apellidoDeCasadoPac;
    
    @Size(max = 1024)
    @Column(name = "estado_civil_pac", length = 1024)
    private String estadoCivilPac;
    
    @Size(max = 1024)
    @Column(name = "numero_identificacion", length = 1024)
    private String numeroIdentificacion;
    
   
    
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente",orphanRemoval=true)
    private List<HistorialClinico> historialClinicoList = new ArrayList<>();
    
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente", orphanRemoval=true)
    private List<ExpedienteClinico> expedienteList = new ArrayList<>();
       
    @OneToMany(cascade=CascadeType.ALL,mappedBy = "idPaciente",orphanRemoval=true)
    private List<Hospitalizacion> hospitalizacionList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente", orphanRemoval=true)
    private List<CitaMedica> citaMedicaList= new ArrayList<>();

   

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombrePac() {
        return nombrePac;
    }

    public void setNombrePac(String nombrePac) {
        this.nombrePac = nombrePac;
    }

    public String getApellidoPac() {
        return apellidoPac;
    }

    public void setApellidoPac(String apellidoPac) {
        this.apellidoPac = apellidoPac;
    }

    public String getGeneroPac() {
        return generoPac;
    }

    public void setGeneroPac(String generoPac) {
        this.generoPac = generoPac;
    }

    

    public LocalDate getFechaNacimientoPac() {
		return fechaNacimientoPac;
	}

	public void setFechaNacimientoPac(LocalDate fechaNacimientoPac) {
		this.fechaNacimientoPac = fechaNacimientoPac;
	}

	public String getDireccionPac() {
        return direccionPac;
    }

    public void setDireccionPac(String direccionPac) {
        this.direccionPac = direccionPac;
    }

    public String getTelefonoPac() {
        return telefonoPac;
    }

    public void setTelefonoPac(String telefonoPac) {
        this.telefonoPac = telefonoPac;
    }

    public String getResponsablePorEmergenciaPac() {
        return responsablePorEmergenciaPac;
    }

    public void setResponsablePorEmergenciaPac(String responsablePorEmergenciaPac) {
        this.responsablePorEmergenciaPac = responsablePorEmergenciaPac;
    }

    public String getEmailPac() {
        return emailPac;
    }

    public void setEmailPac(String emailPac) {
        this.emailPac = emailPac;
    }

    public String getApellidoDeCasadoPac() {
        return apellidoDeCasadoPac;
    }

    public void setApellidoDeCasadoPac(String apellidoDeCasadoPac) {
        this.apellidoDeCasadoPac = apellidoDeCasadoPac;
    }

    public String getEstadoCivilPac() {
        return estadoCivilPac;
    }

    public void setEstadoCivilPac(String estadoCivilPac) {
        this.estadoCivilPac = estadoCivilPac;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

   
    public List<HistorialClinico> getHistorialClinicoList() {
        return historialClinicoList;
    }

    public void setHistorialClinicoList(List<HistorialClinico> historialClinicoList) {
        this.historialClinicoList = historialClinicoList;
    }

    public List<Hospitalizacion> getHospitalizacionList() {
        return hospitalizacionList;
    }

    public void setHospitalizacionList(List<Hospitalizacion> hospitalizacionList) {
        this.hospitalizacionList = hospitalizacionList;
    }
   
    public List<CitaMedica> getCitaMedicaList() {
        return citaMedicaList;
    }

    public void setCitaMedicaList(List<CitaMedica> citaMedicaList) {
        this.citaMedicaList = citaMedicaList;
    }

	
	public List<ExpedienteClinico> getExpedienteList() {
		return expedienteList;
	}

	public void setExpedienteList(List<ExpedienteClinico> expedienteList) {
		this.expedienteList = expedienteList;
	}

		
    
}
