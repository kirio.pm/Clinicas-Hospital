/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;



import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="SINTOMATOLOGIA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_SIN_PROCE",
	        procedureName="ADD_SIN_PROCE",
	        resultClasses = { Sintomatologia.class },
	        parameters={
	            @StoredProcedureParameter(name="SIT", type=String.class, mode=ParameterMode.IN),	            
	            @StoredProcedureParameter(name="IDCON", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_SIN_PROCE",
	        procedureName="UDP_SIN_PROCE",
	        resultClasses = { Sintomatologia.class },
	        parameters={
	        	@StoredProcedureParameter(name="IDS", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="SIN", type=String.class, mode=ParameterMode.IN),	            
	            @StoredProcedureParameter(name="IDC", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Sintomatologia  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sintomatologia", nullable = false)
    private int idSintomatologia;
    
    @Size(max = 1024)
    @Column(length = 1024)
    private String sintomas;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_consulta", referencedColumnName = "id_consulta", nullable = false)
    private ConsultaMedica idConsulta;

    
    public int getIdSintomatologia() {
		return idSintomatologia;
	}

	public void setIdSintomatologia(int idSintomatologia) {
		this.idSintomatologia = idSintomatologia;
	}

	public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

	public ConsultaMedica getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(ConsultaMedica idConsulta) {
		this.idConsulta = idConsulta;
	}

    
    

        
}
