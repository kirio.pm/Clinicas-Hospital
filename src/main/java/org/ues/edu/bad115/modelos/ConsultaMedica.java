/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CONSULTAMEDICA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
        name="ADD_CON_MED_PROCE",
        procedureName="ADD_CON_MED_PROCE",
        resultClasses = { ConsultaMedica.class },
        parameters={
            @StoredProcedureParameter(name="ID_CAT", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="IDDOCTOR", type=Integer.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="DESCRIPCION", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="IDEXPEDIENTE", type=Integer.class, mode=ParameterMode.IN)
        }
	),
	@NamedStoredProcedureQuery(
        name="UDP_CON_MED_PROCE",
        procedureName="UDP_CON_MED_PROCE",
        resultClasses = { ConsultaMedica.class },
        parameters={
        	@StoredProcedureParameter(name="IDCON", type=Integer.class, mode=ParameterMode.IN),
        	@StoredProcedureParameter(name="DES", type=String.class, mode=ParameterMode.IN),
            @StoredProcedureParameter(name="IDDO", type=Integer.class, mode=ParameterMode.IN)
            
        }
	)
})
public class ConsultaMedica {


    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_consulta", nullable = false)
    private int idConsulta;
    
    @Column(name = "correlativo_con", nullable = false)
    private int correlativoCon;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_con")
    private Date fechaCon;
    
    @Size(max = 1024)
    @Column(name = "descripcion_con", length = 1024)
    private String descripcionCon;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConsulta", orphanRemoval=true)
    private List<SignosVitales> signosVitalesList = new ArrayList<>();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cat_con", referencedColumnName = "id_cat_con")
    private CatalogoConsulta idCatCon;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_doctor", referencedColumnName = "id_doctor", nullable = false)
    private Doctor idDoctor;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente", nullable = false)
    private ExpedienteClinico idExpediente;
    
    
    @OneToMany(cascade=CascadeType.ALL,mappedBy = "idConsulta",orphanRemoval=true)
    private List<Sintomatologia> sintomatologiaList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConsulta", orphanRemoval=true)
    private List<Diagnostico> diagnosticoList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConsultaMedica", orphanRemoval=true)
    private List<Examen> examenList = new ArrayList<>();
    
    public int getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(int idConsulta) {
        this.idConsulta = idConsulta;
    }

    public Date getFechaCon() {
        return fechaCon;
    }

    public void setFechaCon(Date fechaCon) {
        this.fechaCon = fechaCon;
    }

    
    
    public List<SignosVitales> getSignosVitalesList() {
        return signosVitalesList;
    }

    public void setSignosVitalesList(List<SignosVitales> signosVitalesList) {
        this.signosVitalesList = signosVitalesList;
    }

    public CatalogoConsulta getIdCatCon() {
        return idCatCon;
    }

    public void setIdCatCon(CatalogoConsulta idCatCon) {
        this.idCatCon = idCatCon;
    }

    public Doctor getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Doctor idDoctor) {
        this.idDoctor = idDoctor;
    }

    public ExpedienteClinico getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(ExpedienteClinico idExpediente) {
        this.idExpediente = idExpediente;
    }

	public List<Sintomatologia> getSintomatologiaList() {
		return sintomatologiaList;
	}

	public void setSintomatologiaList(List<Sintomatologia> sintomatologiaList) {
		this.sintomatologiaList = sintomatologiaList;
	}

	public int getCorrelativoCon() {
		return correlativoCon;
	}

	public void setCorrelativoCon(int correlativoCon) {
		this.correlativoCon = correlativoCon;
	}

	public List<Diagnostico> getDiagnosticoList() {
		return diagnosticoList;
	}

	public void setDiagnosticoList(List<Diagnostico> diagnosticoList) {
		this.diagnosticoList = diagnosticoList;
	}

	public String getDescripcionCon() {
		return descripcionCon;
	}

	public void setDescripcionCon(String descripcionCon) {
		this.descripcionCon = descripcionCon;
	}

	public List<Examen> getExamenList() {
		return examenList;
	}

	public void setExamenList(List<Examen> examenList) {
		this.examenList = examenList;
	}  
    
}
