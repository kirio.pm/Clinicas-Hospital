/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CIRUGIA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_CIRU_PROCE",
	        procedureName="ADD_CIRU_PROCE",
	        resultClasses = { Cirugia.class },
	        parameters={
	            @StoredProcedureParameter(name="FECI", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="HC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDBI", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCC", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_CIRU_PROCE",
	        procedureName="UDP_CIRU_PROCE",
	        resultClasses = { Cirugia.class },
	        parameters={
	        	@StoredProcedureParameter(name="IDCI", type=Integer.class, mode=ParameterMode.IN),
	        	@StoredProcedureParameter(name="FECI", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="HC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDBI", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCC", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Cirugia  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cirugia", nullable = false)
    private int idCirugia;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_ciru")
    private LocalDate fechaCiru;
    
    @Size(max = 100)
    @Column(name = "hora_ciru", length = 100)
    private String horaCiru;
    
    @Size(max = 1024)
    @Column(name = "proceso_ciru", length = 1024)
    private String procesoCiru;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCirugia", orphanRemoval = true)
    private List<SeguimientoCirugia> seguimientoCirugiaList = new ArrayList<>();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_bitacora", referencedColumnName = "id_bitacora", nullable = false)
    private BitacoraProcedimientos idBitacora;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cat_ciru", referencedColumnName = "id_cat_ciru", nullable = false)
    private CatalogoCirugia idCatCiru;
    
    public int getIdCirugia() {
		return idCirugia;
	}

	public void setIdCirugia(int idCirugia) {
		this.idCirugia = idCirugia;
	}

	public String getHoraCiru() {
		return horaCiru;
	}

	public void setHoraCiru(String horaCiru) {
		this.horaCiru = horaCiru;
	}
    
    public List<SeguimientoCirugia> getSeguimientoCirugiaList() {
        return seguimientoCirugiaList;
    }

    public void setSeguimientoCirugiaList(List<SeguimientoCirugia> seguimientoCirugiaList) {
        this.seguimientoCirugiaList = seguimientoCirugiaList;
    }

    public BitacoraProcedimientos getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(BitacoraProcedimientos idBitacora) {
        this.idBitacora = idBitacora;
    }

    public CatalogoCirugia getIdCatCiru() {
        return idCatCiru;
    }

    public void setIdCatCiru(CatalogoCirugia idCatCiru) {
        this.idCatCiru = idCatCiru;
    }

	public LocalDate getFechaCiru() {
		return fechaCiru;
	}

	public void setFechaCiru(LocalDate fechaCiru) {
		this.fechaCiru = fechaCiru;
	}

	public String getProcesoCiru() {
		return procesoCiru;
	}

	public void setProcesoCiru(String procesoCiru) {
		this.procesoCiru = procesoCiru;
	}

       
}
