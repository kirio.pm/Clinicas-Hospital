/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "CATALOGODEEXAMENES")

public class CatalogoDeExamenes  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cat_examen", nullable = false)
    private int idCatExamen;
    
    @Size(max = 1024)
    @Column(name = "tipo_examen", length = 1024)
    private String tipoExamen;
    
    @Size(max = 1024)
    @Column(name = "nombre_examen", length = 1024)
    private String nombreExamen;
    
    @Column(name = "costo_examen", precision = 8, scale = 2)
    private double costoExamen;
    
    @Size(max = 7)
    @Column(name = "cod_exa", length = 7)
    private String codExa;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCatExamen", orphanRemoval = true)
    private List<Examen> examenList = new ArrayList<>();


    public int getIdCatExamen() {
        return idCatExamen;
    }

    public void setIdCatExamen(int idCatExamen) {
        this.idCatExamen = idCatExamen;
    }

    public String getTipoExamen() {
        return tipoExamen;
    }

    public void setTipoExamen(String tipoExamen) {
        this.tipoExamen = tipoExamen;
    }

    public String getNombreExamen() {
        return nombreExamen;
    }

    public void setNombreExamen(String nombreExamen) {
        this.nombreExamen = nombreExamen;
    }

    public double getCostoExamen() {
        return costoExamen;
    }

    public void setCostoExamen(double costoExamen) {
        this.costoExamen = costoExamen;
    }

    public String getCodExa() {
        return codExa;
    }

    public void setCodExa(String codExa) {
        this.codExa = codExa;
    }

    
    public List<Examen> getExamenList() {
        return examenList;
    }

    public void setExamenList(List<Examen> examenList) {
        this.examenList = examenList;
    }

        
}
