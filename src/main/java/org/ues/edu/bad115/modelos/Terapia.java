/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="TERAPIA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_TERA_PROCE",
	        procedureName="ADD_TERA_PROCE",
	        resultClasses = { Terapia.class },
	        parameters={
	        	@StoredProcedureParameter(name="FETR", type=LocalDate.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCT", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDTR", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_TERA_PROCE",
	        procedureName="UPD_TERA_PROCE",
	        resultClasses = { Terapia.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDTERA", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="FETR", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDCT", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDTR", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Terapia  {

    public void setIdCatTera(CatalogoTerapia idCatTera) {
		this.idCatTera = idCatTera;
	}

	@Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_terapia", nullable = false)
    private int idTerapia;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_de_tera")
    private LocalDate fechaDeTera;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_cat_tera", referencedColumnName = "id_cat_tera", nullable = false)
    private CatalogoTerapia idCatTera;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_tratamiento", referencedColumnName = "id_tratamiento", nullable = false)
    private TratamientoMedico idTratamiento;

   
    public int getIdTerapia() {
        return idTerapia;
    }

    public void setIdTerapia(int idTerapia) {
        this.idTerapia = idTerapia;
    }
    
    public LocalDate getFechaDeTera() {
		return fechaDeTera;
	}

	public void setFechaDeTera(LocalDate fechaDeTera) {
		this.fechaDeTera = fechaDeTera;
	}

    public CatalogoTerapia getIdCatTera() {
        return idCatTera;
    }

    public TratamientoMedico getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(TratamientoMedico idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

      
}
