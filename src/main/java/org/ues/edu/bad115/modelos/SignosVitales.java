/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="SIGNOSVITALES")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_SIG_VIT_PROCE",
	        procedureName="ADD_SIG_VIT_PROCE",
	        resultClasses = { SignosVitales.class },
	        parameters={
	            @StoredProcedureParameter(name="ALTURA_SIV", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="FRECUENCIA_CARDIACA_SIV", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PESO_SIV", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PRESION_ARTERIAL_SIV", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TEMPERATURA_SIV", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="ID_CONSULTA", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_SIG_VIT_PROCE",
	        procedureName="UDP_SIG_VIT_PROCE",
	        resultClasses = { SignosVitales.class },
	        parameters={
	        	@StoredProcedureParameter(name="IDSIG", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="ALT", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="FCS", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PESS", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PAS", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TES", type=Double.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCO", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class SignosVitales  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_signos_vitales", nullable = false)
    private int idSignosVitales;
    
    @Column(name = "temperatura_siv", precision = 8, scale = 2)
    private double temperaturaSiv;
    
    @Size(max = 1024)
    @Column(name = "presion_arterial_siv", length = 1024)
    private String presionArterialSiv;
    
    @Column(name = "frecuencia_cardiaca_siv")
    private int frecuenciaCardiacaSiv;
    
    @Column(name = "peso_siv", precision = 8, scale = 2)
    private double pesoSiv;
    
    @Column(name = "altura_siv", precision = 8, scale = 2)
    private double alturaSiv;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_consulta", referencedColumnName = "id_consulta", nullable = false)
    private ConsultaMedica idConsulta;

    

    public int getIdSignosVitales() {
        return idSignosVitales;
    }

    public void setIdSignosVitales(int idSignosVitales) {
        this.idSignosVitales = idSignosVitales;
    }

    public double getTemperaturaSiv() {
        return temperaturaSiv;
    }

    public void setTemperaturaSiv(double temperaturaSiv) {
        this.temperaturaSiv = temperaturaSiv;
    }

    public String getPresionArterialSiv() {
        return presionArterialSiv;
    }

    public void setPresionArterialSiv(String presionArterialSiv) {
        this.presionArterialSiv = presionArterialSiv;
    }

    public int getFrecuenciaCardiacaSiv() {
        return frecuenciaCardiacaSiv;
    }

    public void setFrecuenciaCardiacaSiv(int frecuenciaCardiacaSiv) {
        this.frecuenciaCardiacaSiv = frecuenciaCardiacaSiv;
    }

    public double getPesoSiv() {
        return pesoSiv;
    }

    public void setPesoSiv(double pesoSiv) {
        this.pesoSiv = pesoSiv;
    }

    public double getAlturaSiv() {
        return alturaSiv;
    }

    public void setAlturaSiv(double alturaSiv) {
        this.alturaSiv = alturaSiv;
    }

    public ConsultaMedica getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(ConsultaMedica idConsulta) {
        this.idConsulta = idConsulta;
    }

    
    
}
