package org.ues.edu.bad115.modelos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="REGION")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_REG_PROCE",
	        procedureName="ADD_REG_PROCE",
	        resultClasses = { Region.class },
	        parameters={
	        	@StoredProcedureParameter(name="CONT", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="PAIS", type=String.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_REG_PROCE",
	        procedureName="UPD_REG_PROCE",
	        resultClasses = { Region.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDRE", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="CONT", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="PAI", type=String.class, mode=ParameterMode.IN)
	        }
	)
})
public class Region {
	
	
	@Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_region", nullable = false)
    private int idRegion;
	
	@Size(max = 1024)
	@Column(name = "continiente", length = 1024)
	private String continente;

	@Size(max = 1024)
	@Column(name = "pais", length = 1024)
	private String pais;

	@OneToMany(cascade=CascadeType.ALL,mappedBy = "idRegion",orphanRemoval=true)
    private List<Clinica> clinicaList = new ArrayList<>();
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy = "idRegion",orphanRemoval=true)
    private List<Hospital> hospitalList = new ArrayList<>();

	public int getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(int idRegion) {
		this.idRegion = idRegion;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public List<Clinica> getClinicaList() {
		return clinicaList;
	}

	public void setClinicaList(List<Clinica> clinicaList) {
		this.clinicaList = clinicaList;
	}

	public List<Hospital> getHospitalList() {
		return hospitalList;
	}

	public void setHospitalList(List<Hospital> hospitalList) {
		this.hospitalList = hospitalList;
	}
	
	
}
