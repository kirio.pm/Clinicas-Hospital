/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="MEDICAMENTO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_MEDCA_PROCE",
	        procedureName="ADD_MEDCA_PROCE",
	        resultClasses = { Medicamento.class },
	        parameters={
	        	@StoredProcedureParameter(name="DOC", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDCM", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IDTR", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_MEDCA_PROCE",
	        procedureName="UPD_MEDCA_PROCE",
	        resultClasses = { Medicamento.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDME", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="DOC", type=String.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDCM", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="IDTR", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Medicamento  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_medicamento", nullable = false)
    private int idMedicamento;
    
    @Size(max = 1024)
    @Column(name = "docis_med", length = 1024)
    private String docisMed;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_cat_medi", referencedColumnName = "id_cat_medi", nullable = false)
    private CatalogoMedicamento idCatMedi;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_tratamiento", referencedColumnName = "id_tratamiento")
    private TratamientoMedico idTratamiento;

    

    public int getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(int idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getDocisMed() {
        return docisMed;
    }

    public CatalogoMedicamento getIdCatMedi() {
        return idCatMedi;
    }

    public void setIdCatMedi(CatalogoMedicamento idCatMedi) {
        this.idCatMedi = idCatMedi;
    }

    

    public TratamientoMedico getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(TratamientoMedico idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

	public void setDocisMed(String docisMed) {
		this.docisMed = docisMed;
	}
    
        
}
