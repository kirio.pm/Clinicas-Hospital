package org.ues.edu.bad115.modelos;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	USER("USUARIO"),
	ADMIN("ADMIN"),
	RECEPCTION("RECEPCIONISTA"),
	NURSE("ENFERMERA"),
	DOCTOR("DOCTOR"),
	SECRETARY("SECRETARIA");
	
	String userProfileType;
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	public String getUserProfileType(){
		return userProfileType;
	}
	

}
