package org.ues.edu.bad115.modelos;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "PERSISTEN_LOGINS")
public class PersistentLogin {
	@Id
    @Basic(optional = false)
    @Column(name = "id_user", nullable = false)
	private String series;

	@Column(name = "username", unique = true, nullable = false, length = 25)
	private String username;

	@Column(name = "token", unique = true, nullable = false, length = 100)
	private String token;

	@Column(name = "last_used")
	private Date last_used;

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLast_used() {
		return last_used;
	}

	public void setLast_used(Date last_used) {
		this.last_used = last_used;
	}

	

}
