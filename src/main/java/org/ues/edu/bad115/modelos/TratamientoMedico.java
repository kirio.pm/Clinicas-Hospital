/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="TRATAMIENTOMEDICO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_TRA_MED_PROCE",
	        procedureName="ADD_TRA_MED_PROCE",
	        resultClasses = { TratamientoMedico.class },
	        parameters={
	        	@StoredProcedureParameter(name="FRTR", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TITR", type=String.class, mode=ParameterMode.IN),	            
	            @StoredProcedureParameter(name="IDDI", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_TRA_MED_PROCE",
	        procedureName="UPD_TRA_MED_PROCE",
	        resultClasses = { TratamientoMedico.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDTR", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="FRTR", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="TITR", type=String.class, mode=ParameterMode.IN),	            
		            @StoredProcedureParameter(name="IDDI", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class TratamientoMedico  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tratamiento", nullable = false)
    private int idTratamiento;
    
    @Size(max = 1024)
    @Column(name = "tipo_tra", length = 1024)
    private String tipoTra;
    
    @Column(name = "frecuencia_tra")
    private int frecuenciaTra;
    
    @OneToMany(cascade=CascadeType.ALL,mappedBy = "idTratamiento",orphanRemoval=true)
    private List<Medicamento> medicamentoList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTratamiento",orphanRemoval=true)
    private List<Terapia> terapiaList = new ArrayList<>();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_diagnostico", referencedColumnName = "id_diagnostico", nullable = false)
    private Diagnostico idDiagnostico;


    public int getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(int idTratamiento) {
        this.idTratamiento = idTratamiento;
    }


    public String getTipoTra() {
        return tipoTra;
    }

    public void setTipoTra(String tipoTra) {
        this.tipoTra = tipoTra;
    }

    public int getFrecuenciaTra() {
        return frecuenciaTra;
    }

    public void setFrecuenciaTra(int frecuenciaTra) {
        this.frecuenciaTra = frecuenciaTra;
    }

    
    public List<Medicamento> getMedicamentoList() {
        return medicamentoList;
    }

    public void setMedicamentoList(List<Medicamento> medicamentoList) {
        this.medicamentoList = medicamentoList;
    }

    
    public List<Terapia> getTerapiaList() {
        return terapiaList;
    }

    public void setTerapiaList(List<Terapia> terapiaList) {
        this.terapiaList = terapiaList;
    }

	public Diagnostico getIdDiagnostico() {
		return idDiagnostico;
	}

	public void setIdDiagnostico(Diagnostico idDiagnostico) {
		this.idDiagnostico = idDiagnostico;
	}
    
    
       
}
