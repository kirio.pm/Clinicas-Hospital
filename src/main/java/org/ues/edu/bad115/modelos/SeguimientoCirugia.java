/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="SEGUIMIENTOCIRUGIA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_SEG_CIR_PROCE",
	        procedureName="ADD_SEG_CIR_PROCE",
	        resultClasses = { SeguimientoCirugia.class },
	        parameters={
	            @StoredProcedureParameter(name="TS", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IC", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UDP_SEG_CIR_PROCE",
	        procedureName="UDP_SEG_CIR_PROCE",
	        resultClasses = { SeguimientoCirugia.class },
	        parameters={
	        	@StoredProcedureParameter(name="ISC", type=Integer.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="TS", type=String.class, mode=ParameterMode.IN),
	            @StoredProcedureParameter(name="IC", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class SeguimientoCirugia  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_seguimiento", nullable = false)
    private int idSeguimiento;
    
    @Size(max = 1024)
    @Column(name = "tipo_seguimiento", length = 1024)
    private String tipoSeguimiento;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_cirugia", referencedColumnName = "id_cirugia", nullable = false)
    private Cirugia idCirugia;

    

    public int getIdSeguimiento() {
        return idSeguimiento;
    }

    public void setIdSeguimiento(int idSeguimiento) {
        this.idSeguimiento = idSeguimiento;
    }

    public String getTipoSeguimiento() {
        return tipoSeguimiento;
    }

    public void setTipoSeguimiento(String tipoSeguimiento) {
        this.tipoSeguimiento = tipoSeguimiento;
    }

    public Cirugia getIdCirugia() {
        return idCirugia;
    }

    public void setIdCirugia(Cirugia idCirugia) {
        this.idCirugia = idCirugia;
    }

    
    
}
