/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name="ARCHIVOSDEREGISTRO")
public class ArchivosDeRegistro  {

    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_archivo", nullable = false)
    private int idarchivo;
    
    @Size(max = 1024)
    @Column(name = "nombre_de_archivo", length = 1024)
    private String nombreDeArchivo;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente", nullable = false)
    private ExpedienteClinico idExpediente ;

    public ExpedienteClinico getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(ExpedienteClinico idExpediente) {
		this.idExpediente = idExpediente;
	}

	public ArchivosDeRegistro() {
    }

    public ArchivosDeRegistro(int idarchivo) {
        this.idarchivo = idarchivo;
    }

    public int getIdarchivo() {
        return idarchivo;
    }

    public void setIdarchivo(int idarchivo) {
        this.idarchivo = idarchivo;
    }

    public String getNombreDeArchivo() {
        return nombreDeArchivo;
    }

    public void setNombreDeArchivo(String nombreDeArchivo) {
        this.nombreDeArchivo = nombreDeArchivo;
    }
      
    
}
