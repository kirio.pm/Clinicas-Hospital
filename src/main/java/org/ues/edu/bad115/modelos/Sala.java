/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="SALA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(
	        name="ADD_SAL_PROCE",
	        procedureName="ADD_SAL_PROCE",
	        resultClasses = { Sala.class },
	        parameters={
	        		@StoredProcedureParameter(name="CS", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="HS", type=Integer.class, mode=ParameterMode.IN),		            
		            @StoredProcedureParameter(name="IDH", type=Integer.class, mode=ParameterMode.IN)
	        }
	),
	@NamedStoredProcedureQuery(
	        name="UPD_SAL_PROCE",
	        procedureName="UPD_SAL_PROCE",
	        resultClasses = { Hospital.class },
	        parameters={
	        		@StoredProcedureParameter(name="IDS", type=Integer.class, mode=ParameterMode.IN),
	        		@StoredProcedureParameter(name="CS", type=Integer.class, mode=ParameterMode.IN),
		            @StoredProcedureParameter(name="HS", type=Integer.class, mode=ParameterMode.IN),           
		            @StoredProcedureParameter(name="IDH", type=Integer.class, mode=ParameterMode.IN)
	        }
	)
})
public class Sala  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sala", nullable = false)
    private int idSala;
    
    @Column(name = "habitacion_sal")
    private int habitacionSal;
    
    @Column(name = "cama_sal")
    private int camaSal;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_hospital", referencedColumnName = "id_hospital")
    private Hospital idHospital;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSala",orphanRemoval=true)
    private List<Hospitalizacion> hospitalizacionList = new ArrayList<>();

    

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public int getHabitacionSal() {
        return habitacionSal;
    }

    public void setHabitacionSal(int habitacionSal) {
        this.habitacionSal = habitacionSal;
    }

    public int getCamaSal() {
        return camaSal;
    }

    public void setCamaSal(int camaSal) {
        this.camaSal = camaSal;
    }

    public Hospital getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(Hospital idHospital) {
        this.idHospital = idHospital;
    }

  
    public List<Hospitalizacion> getHospitalizacionList() {
        return hospitalizacionList;
    }

    public void setHospitalizacionList(List<Hospitalizacion> hospitalizacionList) {
        this.hospitalizacionList = hospitalizacionList;
    }

    
    
}
