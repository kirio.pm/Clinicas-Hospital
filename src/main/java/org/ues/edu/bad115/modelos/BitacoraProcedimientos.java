/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ues.edu.bad115.modelos;



import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "BitacoraProcedimientos")
public class BitacoraProcedimientos  {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_bitacora", nullable = false)
    private int idBitacora;
    
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_bitacora")
    private LocalDate fechaBitacora;
    
    @Column(name="total" ,precision = 8, scale = 2)
    private double total;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_hospitalizacion", referencedColumnName = "id_hospitalizacion", nullable = false)
    private Hospitalizacion idHospitalizacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBitacora", orphanRemoval = true)
    private List<Cirugia> cirugiaList = new ArrayList<>();

    public BitacoraProcedimientos() {
    }

    public BitacoraProcedimientos(int idBitacora) {
        this.idBitacora = idBitacora;
    }

    public int getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(int idBitacora) {
        this.idBitacora = idBitacora;
    }
    
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Hospitalizacion getIdHospitalizacion() {
        return idHospitalizacion;
    }

    public void setIdHospitalizacion(Hospitalizacion idHospitalizacion) {
        this.idHospitalizacion = idHospitalizacion;
    }

    public LocalDate getFechaBitacora() {
		return fechaBitacora;
	}

	public void setFechaBitacora(LocalDate fechaBitacora) {
		this.fechaBitacora = fechaBitacora;
	}
    
    public List<Cirugia> getCirugiaList() {
        return cirugiaList;
    }

    public void setCirugiaList(List<Cirugia> cirugiaList) {
        this.cirugiaList = cirugiaList;
    }

        
}
