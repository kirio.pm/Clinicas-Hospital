package org.ues.edu.bad115.dto;

import java.util.Date;

public class detalleExpediente {

	private int idExpediente;
	private String codExpediente;
	private Date fechaCreacion;
	private String nombrePac;
	private String apellidoPac;
	private String generoPac;
	private String estadoCivilPac;
	private String direccionPac;
	private String telefonoPac;
	private String responsablePorEmergenciaPac;
	private String emailPac;
	
	
	
	public detalleExpediente(int idExpediente, String codExpediente, Date fechaCreacion, String nombrePac,
			String apellidoPac, String generoPac, String estadoCivilPac, String direccionPac, String telefonoPac,
			String responsablePorEmergenciaPac, String emailPac) {
		super();
		this.idExpediente = idExpediente;
		this.codExpediente = codExpediente;
		this.fechaCreacion = fechaCreacion;
		this.nombrePac = nombrePac;
		this.apellidoPac = apellidoPac;
		this.generoPac = generoPac;
		this.estadoCivilPac = estadoCivilPac;
		this.direccionPac = direccionPac;
		this.telefonoPac = telefonoPac;
		this.responsablePorEmergenciaPac = responsablePorEmergenciaPac;
		this.emailPac = emailPac;
	}
	
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getCodExpediente() {
		return codExpediente;
	}
	public void setCodExpediente(String codExpediente) {
		this.codExpediente = codExpediente;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getNombrePac() {
		return nombrePac;
	}
	public void setNombrePac(String nombrePac) {
		this.nombrePac = nombrePac;
	}
	public String getApellidoPac() {
		return apellidoPac;
	}
	public void setApellidoPac(String apellidoPac) {
		this.apellidoPac = apellidoPac;
	}
	public String getGeneroPac() {
		return generoPac;
	}
	public void setGeneroPac(String generoPac) {
		this.generoPac = generoPac;
	}
	public String getEstadoCivilPac() {
		return estadoCivilPac;
	}
	public void setEstadoCivilPac(String estadoCivilPac) {
		this.estadoCivilPac = estadoCivilPac;
	}
	public String getDireccionPac() {
		return direccionPac;
	}
	public void setDireccionPac(String direccionPac) {
		this.direccionPac = direccionPac;
	}
	public String getTelefonoPac() {
		return telefonoPac;
	}
	public void setTelefonoPac(String telefonoPac) {
		this.telefonoPac = telefonoPac;
	}
	public String getResponsablePorEmergenciaPac() {
		return responsablePorEmergenciaPac;
	}
	public void setResponsablePorEmergenciaPac(String responsablePorEmergenciaPac) {
		this.responsablePorEmergenciaPac = responsablePorEmergenciaPac;
	}
	public String getEmailPac() {
		return emailPac;
	}
	public void setEmailPac(String emailPac) {
		this.emailPac = emailPac;
	}
	
	
}
