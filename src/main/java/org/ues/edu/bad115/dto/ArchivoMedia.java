package org.ues.edu.bad115.dto;

public class ArchivoMedia {
	
	private String nombreOriginal;
	private String formato;
	private String rutaArchivo;
	private String tamanio;
	
	public ArchivoMedia() {
		super();
	}
	
	public ArchivoMedia(String nombreOriginal, String formato, String rutaArchivo, String tamanio) {
		super();
		this.nombreOriginal = nombreOriginal;
		this.formato = formato;
		this.rutaArchivo = rutaArchivo;
		this.tamanio = tamanio;
	}

	public String getNombreOriginal() {
		return nombreOriginal;
	}
	public void setNombreOriginal(String nombreOriginal) {
		this.nombreOriginal = nombreOriginal;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public String getRutaArchivo() {
		return rutaArchivo;
	}
	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}
	public String getTamanio() {
		return tamanio;
	}
	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}
	
	

}
