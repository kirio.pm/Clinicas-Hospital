package org.ues.edu.bad115.servicios;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ues.edu.bad115.dao.ExpedienteDao;
import org.ues.edu.bad115.modelos.ExpedienteClinico;

@Service
@Transactional
public class ExpedienteClinicoServiceIml  implements ExpedienteClinicoService{
	
	@Autowired
	private ExpedienteDao dao;

	@Override
	public boolean GuardarExpediente(int ID_PACIENTE) {
		// TODO Auto-generated method stub
		dao.GuardarExpediente(ID_PACIENTE);
		return true;
	}

	
	@Override
	public List<ExpedienteClinico> ListarExpedientes() {
		
		return dao.ListarExpedientes();
		}


	@Override
	public ExpedienteClinico BuscarExpediente(int idExpediente) {
		// TODO Auto-generated method stub
		return dao.BuscarExpediente(idExpediente);
	}
	
	

}
