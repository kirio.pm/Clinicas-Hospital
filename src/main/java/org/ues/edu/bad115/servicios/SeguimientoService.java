package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.SeguimientoCirugia;

public interface SeguimientoService {
	public boolean addSeguimiento( String tipoSeguimiento, int idCirugia);
	public List<SeguimientoCirugia> listaSeguimiento();
	public boolean udpSeguimiento(int idSeguimiento, String tipoSeguimiento, int idCirugia);
	public SeguimientoCirugia buscarSeguimiento(int idSeguimiento);
}
