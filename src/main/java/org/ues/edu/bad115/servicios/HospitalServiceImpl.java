package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.HospitalDao;
import org.ues.edu.bad115.modelos.Hospital;

@Service
@Transactional
public class HospitalServiceImpl implements HospitalService {
	@Autowired
	private HospitalDao dao;

	@Override
	public boolean addHospital(String direccionHos, String nombreHos, String telefonoHos, int idRegion) {
		dao.addHospital(direccionHos, nombreHos, telefonoHos, idRegion);
		return true;
	}

	@Override
	public List<Hospital> listaHospital() {
		
		return dao.listaHospital();
	}

	@Override
	public boolean udpHospital(int idHospital, String direccionHos, String nombreHos, String telefonoHos,
			int idRegion) {
		dao.udpHospital(idHospital, direccionHos, nombreHos, telefonoHos, idRegion);
		return true;
	}

	@Override
	public Hospital buscarHospital(int idHospital) {
		
		return dao.buscarHospital(idHospital);
	}
}
