package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoMedicamento;

public interface CatalogoMedicamentoService {
	public List<CatalogoMedicamento> listaCatalogoMedicamento();
}
