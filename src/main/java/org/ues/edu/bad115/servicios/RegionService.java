package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.Region;

public interface RegionService {
	public boolean addRegion(String continente, String pais);
	public List<Region> listaRegion();
	public boolean udpRegion(int idRegion, String continente, String pais);
	public Region buscarRegion(int idRegion);
}
