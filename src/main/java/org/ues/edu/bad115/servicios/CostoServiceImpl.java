package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CostoDao;
import org.ues.edu.bad115.modelos.CostosPorServicios;

@Service
@Transactional
public class CostoServiceImpl implements CostoService{
	
	@Autowired
	private CostoDao dao;

	@Override
	public boolean addCosto(LocalDate fechaCosto, String unidadMonetaria) {
		dao.addCosto(fechaCosto, unidadMonetaria);
		return true;
	}

	@Override
	public List<CostosPorServicios> listaCosto() {
		
		return dao.listaCosto();
	}

	@Override
	public boolean udpCosto(int idCosto, LocalDate fechaCosto, String unidadMonetaria) {
		dao.udpCosto(idCosto, fechaCosto, unidadMonetaria);
		return true;
	}

	@Override
	public CostosPorServicios buscarCosto(int idCosto) {
		
		return dao.buscarCosto(idCosto);
	}
	
	
}
