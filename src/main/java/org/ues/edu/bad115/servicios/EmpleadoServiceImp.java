package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.EmpleadoDao;
import org.ues.edu.bad115.modelos.Empleado;

@Service
@Transactional
public class EmpleadoServiceImp implements EmpleadoService{
	
	
	@Autowired
	private EmpleadoDao dao;
	
	@Override
	public boolean addEmpleado(String apellidoEmpleado, String duiEmp, String emilEmp, String nombreEmpleado,
			String telefonoEmp) {
		dao.addEmpleado(apellidoEmpleado, duiEmp, emilEmp, nombreEmpleado, telefonoEmp);
		return true;
	}

	@Override
	public List<Empleado> listaEmpleados() {
		// TODO Auto-generated method stub
		return dao.listaEmpleados();
	}

	@Override
	public boolean updEmpleado(int idEmpleado, String apellidoEmpleado, String duiEmp, String emilEmp,
			String nombreEmpleado, String telefonoEmp) {
		dao.updEmpleado(idEmpleado, apellidoEmpleado, duiEmp, emilEmp, nombreEmpleado, telefonoEmp);
		return true;
	}

	@Override
	public Empleado buscarEmpleado(int idEmpleado) {
		
		return dao.buscarEmpleado(idEmpleado);
	}

}
