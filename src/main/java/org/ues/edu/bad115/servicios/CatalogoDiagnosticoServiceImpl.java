package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoDiagnosticoDao;
import org.ues.edu.bad115.modelos.CatalogoDiagnostico;

@Service
@Transactional
public class CatalogoDiagnosticoServiceImpl implements CatalogoDiagnosticoService{

	@Autowired
	public CatalogoDiagnosticoDao dao;

	@Override
	public List<CatalogoDiagnostico> listaCataDiag() {
		
		return dao.listaCataDiag();
	}
}
