package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.dto.ArchivoMedia;
import org.ues.edu.bad115.modelos.Examen;

public interface ExamenService {
	
	public List<Examen> buscarTodosLosExamenes(int id_consulta);
	
	public Examen buscarExamenPorId(int id_examen);
	
	public void actualizarExamen(Examen examen);
	
	public ArchivoMedia getExamenMediaInformacion();
	
	public void guardarArchivoMedia(int id_examen,ArchivoMedia media);
	
	public void eliminarArchivoMedia(int id_examen);

}
