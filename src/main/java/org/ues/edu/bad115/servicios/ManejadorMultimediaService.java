package org.ues.edu.bad115.servicios;

public interface ManejadorMultimediaService {
	public boolean existeDirectorioFuente();
	public String obtenerRutaAbsoluta();
	public String obtenerDelimitadorOs();
	
}
