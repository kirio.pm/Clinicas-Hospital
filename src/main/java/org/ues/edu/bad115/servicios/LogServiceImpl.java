package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.LogDao;
import org.ues.edu.bad115.modelos.Log;

@Service
@Transactional
public class LogServiceImpl implements LogServices {
	
	@Autowired
	private LogDao dao;
	
	
	@Override
	public List<Log> listaCitas() {
		// TODO Auto-generated method stub
		return dao.listaLog();
	}

}
