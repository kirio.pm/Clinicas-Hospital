package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ues.edu.bad115.dao.HistorialClinicoDao;
import org.ues.edu.bad115.modelos.HistorialClinico;

@Service
@Transactional
public class HistorialClinicoServiceImpl implements HistorialClinicoService {

	@Autowired
	private HistorialClinicoDao HCDao;
	
	@Override
	public boolean guardarHistorialClinico(String enfermedadesPadecidas, 
			String tratamientoPreventivo,
			int idDatosFamiliares,
			int idPaciente) {
		HCDao.guardarHistorialClinico(enfermedadesPadecidas, tratamientoPreventivo, idDatosFamiliares, idPaciente);
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<HistorialClinico> listaHistorialClinico() {
		// TODO Auto-generated method stub
		return HCDao.listaHistorialClinico();
	}

	@Override
	public boolean guardarHistorialDOSClinico(String enfermedadesPadecidas, String tratamientoPreventivo,
			int idPaciente, String apellidoPadre, String direccion, LocalDate fechaNacimiento,
			String historialDeEnfermedades, String nombrePadre) {
		HCDao.guardarHistorialDOSClinico(enfermedadesPadecidas, tratamientoPreventivo, idPaciente, apellidoPadre, direccion, fechaNacimiento, historialDeEnfermedades, nombrePadre);
		return false;
	}

}
