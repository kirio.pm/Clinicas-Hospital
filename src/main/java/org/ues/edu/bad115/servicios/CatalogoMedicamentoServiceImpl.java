package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoMedicamentoDao;
import org.ues.edu.bad115.modelos.CatalogoMedicamento;

@Service
@Transactional
public class CatalogoMedicamentoServiceImpl implements CatalogoMedicamentoService {
	
	@Autowired
	public CatalogoMedicamentoDao dao;

	@Override
	public List<CatalogoMedicamento> listaCatalogoMedicamento() {
		return dao.listaCatalogoMedicamento();
	}

	
}
