package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.ues.edu.bad115.modelos.CitaMedica;

public interface CitaService {
	public boolean addCita(LocalDate fechaCita,String horaCita,String medioRegistro,int idDoctor,int idCatalogoCita, int idPaciente );
	public boolean EditarCita(int idCita,LocalDate fechaCita,String horaCita,String medioRegistro,int idDoctor,int idCatalogoCita, int idPaciente );
	public List<CitaMedica> listaCitas();
	public CitaMedica buscarCita(int idCita);
}
