package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.ues.edu.bad115.modelos.DatosFamiliares;

public interface RegistroDatosFamiliaresService {
	public boolean guardarDatosFamiliares(String apellidoPadre,
			String direccion,
			LocalDate fechaNacimiento,
			String historialDeEnfermedades,
			String nombrePadre);
	public void actualizarDatosFamiliares(DatosFamiliares datosFamiliares);
	public DatosFamiliares buscarDatosFamiliares(int idDatosFamiliares);
	public List<DatosFamiliares> listaDatosFamiliares();
}
