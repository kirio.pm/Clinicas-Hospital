package org.ues.edu.bad115.servicios;

public interface EscrituraArchivoService {
	boolean existeArchivo(String nombre);

    String escribir(String contenido);

    String escribir(String nombre, String contenido);

    String escribirNombreBandera(String nombre, String contenido);

    void eliminar(String nombre);

}
