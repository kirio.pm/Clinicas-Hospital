package org.ues.edu.bad115.servicios;

import java.io.File;


import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Component
public class ManejadorMultimediaServiceImpl implements ManejadorMultimediaService {
	
	private final String OPERATIVE_SYSTEM_WINDOWS = "Windows 10";
    private final String OPERATIVE_SYSTEM_LINUX = "Linux";
    private String SOURCE_DIRECTORY;
    private boolean is_source_directory_exist = false;
    private File directory;
    private static final Logger logger = LoggerFactory.getLogger(ManejadorMultimediaServiceImpl.class);
    
    @PostConstruct
    public void init(){
        setSourceDirectoryForOS();
        checkDirectoryIfExist();
        createDirectoryIfNotExist();
    }

    
    private void setSourceDirectoryForOS(){
        logger.info("Checking operating system...");
        String os = System.getProperty("os.name","");
        logger.info(os);
        if(os.equalsIgnoreCase(OPERATIVE_SYSTEM_LINUX)){
            SOURCE_DIRECTORY = System.getProperty("user.home") + "/clinica_server/sources";
        }else if(os.equalsIgnoreCase(OPERATIVE_SYSTEM_WINDOWS)){
            SOURCE_DIRECTORY = "C:\\clinica_server\\sources";
        } else {
            try {
				throw new OperatingSystemNotSupportedException("Operating system [ " + os + "] not supported");
			} catch (OperatingSystemNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    private void checkDirectoryIfExist(){
        logger.info("Checking if directory exist...");
        is_source_directory_exist = new File(SOURCE_DIRECTORY).exists();
    }

    private void createDirectoryIfNotExist(){
        directory = new File(SOURCE_DIRECTORY);
        logger.info("Directory exist :{}",is_source_directory_exist);
        if(!is_source_directory_exist){
            logger.info("Creating directory...");
            is_source_directory_exist = directory.mkdirs();
            logger.info("Directories created successfully: {}", is_source_directory_exist);
        }
    }


	@Override
	public boolean existeDirectorioFuente() {
		// TODO Auto-generated method stub
		return is_source_directory_exist;
	}

	@Override
	public String obtenerRutaAbsoluta() {
		// TODO Auto-generated method stub
		return directory.getAbsolutePath();
	}

	@Override
	public String obtenerDelimitadorOs() {
		// TODO Auto-generated method stub
		if(System.getProperty("os.name","").equalsIgnoreCase(OPERATIVE_SYSTEM_LINUX)){
            return "/";
        }else if (System.getProperty("os.name","").equalsIgnoreCase(OPERATIVE_SYSTEM_WINDOWS)){
            return "\\";
        }else {
            try {
				throw new OperatingSystemNotSupportedException("Operating system not supported");
			} catch (OperatingSystemNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return null;
	}

}
