package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.Sintomatologia;

public interface SintomatologiaService {

	public boolean addSintomatologia( String sintomas, int idConsulta);
	public List<Sintomatologia> listaSintoma();
	public boolean udpSintomatologia(int idSintomatologia, String sintomas, int idConsulta);
	public Sintomatologia buscarSintoma(int idSintomatologia);
}
