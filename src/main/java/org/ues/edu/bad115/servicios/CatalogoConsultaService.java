package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoConsulta;

public interface CatalogoConsultaService {

	public List<CatalogoConsulta> listaCatalogo();
}
