package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.MedicamentoDao;
import org.ues.edu.bad115.modelos.Medicamento;

@Service
@Transactional
public class MedicamentoServiceImpl implements MedicamentoService {
	
	@Autowired
	private MedicamentoDao dao;

	@Override
	public boolean addMedicamento(String docisMed, int idCatMedi, int idTratamiento) {
		dao.addMedicamento(docisMed, idCatMedi, idTratamiento);
		return true;
	}

	@Override
	public List<Medicamento> listaMedicamento() {
		// TODO Auto-generated method stub
		return dao.listaMedicamento();
	}

	@Override
	public boolean udpMedicamento(int idMedicamento, String docisMed, int idCatMedi,
			int idTratamiento) {
		// TODO Auto-generated method stub
		dao.udpMedicamento(idMedicamento, docisMed, idCatMedi, idTratamiento);
		return true;
	}

	@Override
	public Medicamento buscarMedicamento(int idMedicamento) {
		// TODO Auto-generated method stub
		return dao.buscarMedicamento(idMedicamento);
	}
}
