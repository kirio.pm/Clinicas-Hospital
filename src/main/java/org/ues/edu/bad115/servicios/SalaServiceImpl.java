package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.SalaDao;
import org.ues.edu.bad115.modelos.Sala;

@Service
@Transactional
public class SalaServiceImpl implements SalaService{
	
	@Autowired
	private SalaDao dao;

	@Override
	public boolean addSala(int camaSal, int habitacionSal, int idHospital) {
		dao.addSala(camaSal, habitacionSal, idHospital);
		return true;
	}

	@Override
	public List<Sala> listaSala() {
		
		return dao.listaSala();
	}

	@Override
	public boolean udpSala(int idSala, int camaSal, int habitacionSal, int idHospital) {
		dao.udpSala(idSala, camaSal, habitacionSal, idHospital);
		return true;
	}

	@Override
	public Sala buscarSala(int idSala) {
		
		return dao.buscarSala(idSala);
	}
}
