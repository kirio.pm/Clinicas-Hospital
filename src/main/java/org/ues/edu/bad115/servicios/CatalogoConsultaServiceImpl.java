package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoConsultaDao;
import org.ues.edu.bad115.modelos.CatalogoConsulta;

@Service
@Transactional
public class CatalogoConsultaServiceImpl implements CatalogoConsultaService{

	@Autowired
	private CatalogoConsultaDao dao;
	
	@Override
	public List<CatalogoConsulta> listaCatalogo() {
		// TODO Auto-generated method stub
		return dao.listaCatalogo();
	}
	

}
