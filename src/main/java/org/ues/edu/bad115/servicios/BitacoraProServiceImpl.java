package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.BitacoraProDao;
import org.ues.edu.bad115.modelos.BitacoraProcedimientos;

@Service
@Transactional
public class BitacoraProServiceImpl implements BitacoraProService{
	
	@Autowired
	private BitacoraProDao dao;

	@Override
	public List<BitacoraProcedimientos> listaBitacora() {
		// TODO Auto-generated method stub
		return dao.listaBitacoraPro();
	}
}
