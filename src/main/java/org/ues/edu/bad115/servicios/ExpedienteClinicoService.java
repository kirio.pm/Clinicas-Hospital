package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.ExpedienteClinico;

public interface ExpedienteClinicoService {
	public boolean GuardarExpediente(int ID_PACIENTE);
	public List<ExpedienteClinico> ListarExpedientes();
	public ExpedienteClinico BuscarExpediente(int idExpediente);
}
