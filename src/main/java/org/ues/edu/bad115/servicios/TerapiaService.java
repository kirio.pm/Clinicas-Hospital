package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.ues.edu.bad115.modelos.Terapia;

public interface TerapiaService {
	public boolean addTerapia(LocalDate fechaDeTera,int idCatTera,int idTratamiento );
	public List<Terapia> listaTerapia();
	public boolean udpTerapia(int idTerapia, LocalDate fechaDeTera, int idCatTera, int idTratamiento);
	public Terapia buscarTerapia(int idTerapia);
}
