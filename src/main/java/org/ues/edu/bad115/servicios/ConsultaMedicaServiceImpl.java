package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.ConsultaMedicaDao;
import org.ues.edu.bad115.modelos.ConsultaMedica;


@Service
@Transactional
public class ConsultaMedicaServiceImpl implements ConsultaMedicaService {
	
	@Autowired
	private ConsultaMedicaDao dao;
	
	@Override
	public boolean addConsultaMedica(int idCatCon, int idDoctor, String descripcion , int idExpediente) {
		dao.addConsultaMedica(idCatCon, idDoctor, descripcion , idExpediente);
		return true;
	}

	@Override
	public List<ConsultaMedica> listaConsulta() {
		return dao.listaConsulta();
	}

	@Override
	public boolean udpConsultaMedica(int idConsulta, String descripcion, int idDoctor) {
		dao.udpConsultaMedica(idConsulta, descripcion, idDoctor);
		return true;
	}

	@Override
	public ConsultaMedica buscarConsulta(int idConsulta) {
		
		return dao.buscarConsulta(idConsulta);
	}

	@Override
	public void actualizarConsulta(ConsultaMedica consulta) {
		dao.actualizarConsulta(consulta);
	}

}
