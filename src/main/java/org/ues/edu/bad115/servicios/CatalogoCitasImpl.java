package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoCitaDao;
import org.ues.edu.bad115.modelos.CatalogoCitas;

@Service
@Transactional
public class CatalogoCitasImpl implements  CatalogoCitasService{
	
	@Autowired
	private CatalogoCitaDao dao;

	@Override
	public List<CatalogoCitas> listaCatalogoCitas() {
		return	dao.listaCatalogoCita();
	}

	
	


}
