package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.DiagnosticoDao;
import org.ues.edu.bad115.modelos.Diagnostico;

@Service
@Transactional
public class DiagnosticoServiceImpl implements DiagnosticoService {
	
	@Autowired
	private DiagnosticoDao dao;

	@Override
	public boolean addDiagnostico(int idCataDiag, int idConsulta) {
		dao.addDiagnostico(idCataDiag, idConsulta);
		return true;
	}

	@Override
	public List<Diagnostico> listaDiagnostico() {
		
		return dao.listaDiagnostico();
	}

	@Override
	public boolean udpDiagnostico(int idDiagnostico, LocalDate fechaDiag, int idCataDiag, int idConsulta) {
		dao.udpDiagnostico(idDiagnostico, fechaDiag, idCataDiag, idConsulta);
		return true;
	}

	@Override
	public Diagnostico buscarDiagnostico(int idDiagnostico) {
		
		return dao.buscarDiagnostico(idDiagnostico);
	}

}
