package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.TerapiaDao;
import org.ues.edu.bad115.modelos.Terapia;

@Service
@Transactional
public class TerapiaServiceImpl implements TerapiaService {
	
	@Autowired
	private TerapiaDao dao;
	
	@Override
	public boolean addTerapia(LocalDate fechaDeTera, int idCatTera,  int idTratamiento) {
		// TODO Auto-generated method stub
		dao.addTerapia(fechaDeTera, idCatTera,idTratamiento);
		return true;
	}

	@Override
	public List<Terapia> listaTerapia() {
		// TODO Auto-generated method stub
		return dao.listaTerapia();
	}

	@Override
	public boolean udpTerapia(int idTerapia, LocalDate fechaDeTera,  int idCatTera, int idTratamiento) {
		// TODO Auto-generated method stub
		dao.udpTerapia(idTerapia, fechaDeTera, idCatTera, idTratamiento);
		return true;
	}

	@Override
	public Terapia buscarTerapia(int idTerapia) {
		// TODO Auto-generated method stub
		return dao.buscarTerapia(idTerapia);
	}

}
