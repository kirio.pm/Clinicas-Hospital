package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.ues.edu.bad115.modelos.Diagnostico;

public interface DiagnosticoService {
	public boolean addDiagnostico( int idCataDiag, int idConsulta);
	public List<Diagnostico> listaDiagnostico();
	public boolean udpDiagnostico(int idDiagnostico, LocalDate fechaDiag, int idCataDiag, int idConsulta);
	public Diagnostico buscarDiagnostico(int idDiagnostico);
}
