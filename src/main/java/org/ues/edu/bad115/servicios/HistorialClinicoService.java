package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.ues.edu.bad115.modelos.HistorialClinico;

public interface HistorialClinicoService {
	public boolean guardarHistorialClinico(String enfermedadesPadecidas,
			String tratamientoPreventivo, 
			int idDatosFamiliares,
			int idPaciente);
	public List<HistorialClinico> listaHistorialClinico();
	public boolean guardarHistorialDOSClinico(String enfermedadesPadecidas,
			String tratamientoPreventivo,
			int idPaciente,
			String apellidoPadre,
			String direccion,
			LocalDate fechaNacimiento,
			String historialDeEnfermedades,
			String nombrePadre);
}
