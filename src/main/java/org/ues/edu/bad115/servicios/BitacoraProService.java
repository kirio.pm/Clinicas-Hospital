package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.BitacoraProcedimientos;

public interface BitacoraProService {
	public List<BitacoraProcedimientos> listaBitacora();
}
