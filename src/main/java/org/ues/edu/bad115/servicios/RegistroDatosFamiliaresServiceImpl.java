package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ues.edu.bad115.dao.RegistroDatosFamiliaresDao;
import org.ues.edu.bad115.modelos.DatosFamiliares;

@Service
@Transactional
public class RegistroDatosFamiliaresServiceImpl implements RegistroDatosFamiliaresService {
	@Autowired
	private RegistroDatosFamiliaresDao dao; 

	@Override
	public boolean guardarDatosFamiliares(String apellidoPadre,
			String direccion,
			LocalDate fechaNacimiento,
			String historialDeEnfermedades,
			String nombrePadre) {
		// TODO Auto-generated method stub
		dao.guardarDatosFamiliares( apellidoPadre,direccion,fechaNacimiento,
				 historialDeEnfermedades,nombrePadre);
		return true;
		
	}

	@Override
	public void actualizarDatosFamiliares(DatosFamiliares datosFamiliares) {
		// TODO Auto-generated method stub
		dao.actualizarDatosFamiliares(datosFamiliares);
		
	}

	@Override
	public DatosFamiliares buscarDatosFamiliares(int idDatosFamiliares) {
		// TODO Auto-generated method stub
		return dao.BuscarDatosFamiliares(idDatosFamiliares);
	}

	@Override
	public List<DatosFamiliares> listaDatosFamiliares() {
		// TODO Auto-generated method stub
		return dao.listaDatosFamiliares();
	}
  

}
