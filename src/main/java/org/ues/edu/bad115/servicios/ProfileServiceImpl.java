package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ues.edu.bad115.dao.ProfileDao;
import org.ues.edu.bad115.modelos.Profile;
@Service("profileService")
@Transactional(readOnly=true)
public class ProfileServiceImpl implements ProfileService{

	@Autowired
	private ProfileDao dao;
	
	public Profile findById(int id) {
		return dao.findById(id);
	}

	public Profile findByType(String type) {
		return dao.findByType(type);
	}

	public List<Profile> findAll() {
		return dao.findAll();
	}

	@Override
	public Profile findInitializeProfileById(int id) {
		return dao.findInitializeProfileById(id);
	}

}
