package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoTerapiaDao;
import org.ues.edu.bad115.modelos.CatalogoTerapia;

@Service
@Transactional
public class CatalogoTerapiaServiceImpl implements CatalogoTerapiaService{

	@Autowired
	public CatalogoTerapiaDao dao;

	@Override
	public List<CatalogoTerapia> listaTerapia() {
		// TODO Auto-generated method stub
		return dao.listaTerapia();
	}
}
