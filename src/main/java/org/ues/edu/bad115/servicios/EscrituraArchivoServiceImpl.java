package org.ues.edu.bad115.servicios;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EscrituraArchivoServiceImpl implements EscrituraArchivoService{
	
	private static final Logger logger = LoggerFactory.getLogger(LectorArchivosMultimediaServiceImpl.class);
	
	@Autowired
	private ManejadorMultimediaService manejador;
	
	@Override
	public boolean existeArchivo(String nombre) {
		// TODO Auto-generated method stub
		return new File(manejador.obtenerRutaAbsoluta()+manejador.obtenerDelimitadorOs()+nombre).exists();
	}

	@Override
	public String escribir(String contenido) {
		File new_file = createNewFileIfNotExist(manejador.obtenerRutaAbsoluta() + buildFileNameWithDateRegister("output"));
        writeContentIntoFile(contenido,new_file);
        return new_file.getName();

	}

	@Override
	public String escribir(String nombre, String contenido) {
		File new_file = createNewFileIfNotExist(manejador.obtenerRutaAbsoluta() + buildFileNameWithDateRegister(nombre));
        writeContentIntoFile(contenido,new_file);
        return new_file.getName();

	}

	@Override
	public String escribirNombreBandera(String nombre, String contenido) {
		File new_file = createNewFileIfNotExist(manejador.obtenerRutaAbsoluta() + buildFileName(nombre));
        writeContentIntoFile(contenido, new_file);
        return new_file.getName();

	}

	@Override
	public void eliminar(String nombre) {
		 File file = new File(manejador.obtenerRutaAbsoluta() + "/" + nombre);
	        boolean is_file_deleted = false;
	        if(file.exists()) is_file_deleted = file.delete();
	        logger.info("File with name {} was deleted: {}",nombre,is_file_deleted);

		
	}
	


    private File createNewFileIfNotExist(String file_name){
        File new_file = new File(file_name);
        logger.info("File with name: {} exist: {}",new_file.getName(),new_file.exists());
        try {
            if(!new_file.exists()){
                boolean isFileCreatedSuccessfully = new_file.createNewFile();
                logger.info("File with name {} created successfully: {}",new_file.getName(), isFileCreatedSuccessfully);
                logger.info("File info: name: {}, size (bytes): {}, path: {},",new_file.getName(),new_file.length(),new_file.getAbsolutePath());
            }
        } catch (IOException e) {
            logger.error("Error creating new File");
            e.printStackTrace();
        }
        return new_file;
    }

    private void writeContentIntoFile(String content, File file){
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            logger.error("File with name {} is not found.", file.getName());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Error found trying write file or close output stream.");
            e.printStackTrace();
        }
    }

    private String buildFileName(String file_name){
        String build_name = manejador.obtenerDelimitadorOs();
        build_name += getSimpleFileName(file_name);
        build_name += ".clinica";
        return build_name;
    }

    private String buildFileNameWithDateRegister(String file_name){
        String build_name = manejador.obtenerDelimitadorOs();
        build_name += getSimpleFileName(file_name);
        build_name += getDateRegister();
        build_name += ".aps";
        return build_name;
    }

    private String getSimpleFileName(String file_name){
        if(file_name.contains(".")) return file_name.split("\\.")[0];
        else return file_name;
    }

    private String getDateRegister(){
        LocalDateTime now = LocalDateTime.now();
        return "_" +
                now.getYear() +
                now.getMonthValue() +
                now.getDayOfMonth() +
                now.getHour() +
                now.getMinute() +
                now.getSecond();
    }


}
