package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CirugiaDao;
import org.ues.edu.bad115.modelos.Cirugia;

@Service
@Transactional
public class CirugiaServiceImpl implements CirugiaService{
	
	@Autowired
	private CirugiaDao dao;

	@Override
	public boolean addCirugia(LocalDate fechaCiru, String horaCiru, String procesoCiru,int idBitacora, int idCatCiru) {
		// TODO Auto-generated method stub
		dao.addCirugia(fechaCiru, horaCiru, procesoCiru,idBitacora, idCatCiru);
		return true;
	}

	@Override
	public List<Cirugia> listaCirugia() {
		// TODO Auto-generated method stub
		return dao.listaCirugia();
	}

	@Override
	public boolean udpCirugia(int idCirugia, LocalDate fechaCiru, String horaCiru, String procesoCiru, int idBitacora, int idCatCiru) {
		// TODO Auto-generated method stub
		dao.udpCirugia(idCirugia, fechaCiru, horaCiru, procesoCiru,idBitacora, idCatCiru);
		return true;
	}

	@Override
	public Cirugia buscarCirugia(int idCirugia) {
		// TODO Auto-generated method stub
		return dao.buscarCirugia(idCirugia);
	}
}
