package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ues.edu.bad115.dao.RegistroPacienteDao;
import org.ues.edu.bad115.modelos.Paciente;

@Service
@Transactional
public class RegistroPacienteServiceImpl implements RegistroPacienteService{
	@Autowired
	private RegistroPacienteDao dao;

	@Override
	public List<Paciente> listaPaciente() {		
		return dao.listaPaciente();

	}
	
	@Override
	public Paciente buscarPaciente(int idPaciente) {
		return dao.buscarPaciente(idPaciente);
	}

	@Override
	public boolean guardarPaciente(
			String apellidoCasadoPac,
			String apellidoPac, 
			String direccionPac, 
			String emailPac,
			String estadoCivilPac,
			LocalDate fechaNacimientoPac,
			String generoPac,
			String nombrePac, 
			String numeroIdentificacion,
			String responsablePorEmergenciaPac,
			String telefonoPac
			) {
		dao.guardarPaciente(apellidoCasadoPac,apellidoPac,direccionPac,emailPac,estadoCivilPac,fechaNacimientoPac,generoPac,nombrePac,  
				numeroIdentificacion,responsablePorEmergenciaPac,telefonoPac);
		return true;
	}

	@Override
	public boolean actualizarPaciente(
			int idPaciente,
			String apellidoCasadoPac,
			String apellidoPac, 
			String direccionPac, 
			String emailPac,
			String estadoCivilPac,
			LocalDate fechaNacimientoPac,
			String generoPac,
			String nombrePac, 
			String numeroIdentificacion,
			String responsablePorEmergenciaPac,
			String telefonoPac
			) {
		dao.actualizarPaciente(idPaciente,apellidoCasadoPac, apellidoPac, direccionPac,
				emailPac, estadoCivilPac, fechaNacimientoPac, generoPac, 
				nombrePac, numeroIdentificacion, responsablePorEmergenciaPac, telefonoPac);
			return true;
	}



}
