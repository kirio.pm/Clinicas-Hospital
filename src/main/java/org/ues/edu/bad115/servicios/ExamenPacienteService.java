
package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoDeExamenes;
import org.ues.edu.bad115.modelos.Examen;

public interface ExamenPacienteService {
	
	public List<CatalogoDeExamenes> listaCatalogoExamen();
	
	public long numeroExamenesPaciente(int id_paciente);
	
	public void guardarExamen(Examen examen);

}
