package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoCirugiaDao;
import org.ues.edu.bad115.modelos.CatalogoCirugia;

@Service
@Transactional
public class CatalogoCirugiaServiceImpl implements CatalogoCirugiaService{
	
	@Autowired
	private CatalogoCirugiaDao dao;

	@Override
	public List<CatalogoCirugia> listaCirugia() {
		// TODO Auto-generated method stub
		return dao.listaCirugia();
	}
}
