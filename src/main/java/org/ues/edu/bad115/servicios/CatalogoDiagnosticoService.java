package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoDiagnostico;

public interface CatalogoDiagnosticoService {
	public List<CatalogoDiagnostico> listaCataDiag();
}
