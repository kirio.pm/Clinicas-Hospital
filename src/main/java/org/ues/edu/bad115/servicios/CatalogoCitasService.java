package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoCitas;

public interface CatalogoCitasService {

	public List<CatalogoCitas> listaCatalogoCitas();
}
