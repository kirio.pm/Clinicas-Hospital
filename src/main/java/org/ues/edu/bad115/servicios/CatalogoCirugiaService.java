package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoCirugia;

public interface CatalogoCirugiaService {
	public List<CatalogoCirugia> listaCirugia();
}
