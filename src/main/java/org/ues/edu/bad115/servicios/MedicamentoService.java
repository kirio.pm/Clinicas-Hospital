package org.ues.edu.bad115.servicios;

import java.util.List;
import org.ues.edu.bad115.modelos.Medicamento;

public interface MedicamentoService {
	public boolean addMedicamento(String docisMed, int idCatMedi, int idTratamiento );
	public List<Medicamento> listaMedicamento();
	public boolean udpMedicamento(int idMedicamento, String docisMed, int idCatMedi, int idTratamiento);
	public Medicamento buscarMedicamento(int idMedicamento);
}
