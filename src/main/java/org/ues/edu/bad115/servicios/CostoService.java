package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;
import org.ues.edu.bad115.modelos.CostosPorServicios;

public interface CostoService {
	public boolean addCosto(LocalDate fechaCosto, String unidadMonetaria);
	public List<CostosPorServicios> listaCosto();
	public boolean udpCosto(int idCosto, LocalDate fechaCosto, String unidadMonetaria);
	public CostosPorServicios buscarCosto(int idCosto);
}
