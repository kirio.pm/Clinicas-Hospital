package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.modelos.User;

public interface UserService {
	public User findById(int id);

	public User findByUsername(String username);

	public List<User> findAll();

	public void saveUser(User user);

	public void updateUser(User user);

	public void deleteByUsername(String username);

	public boolean isUsernameUnique(int id, String username);
	
	public Empleado getPersonalByUsername(String username);
	
	public List<Profile> getProfileByUsername(String username);
	
	public User getInitializeUserByUsername(String username);

	public void updateUserLastLogging(int id);
	
	public User getInitializeUserById(int id);

}
