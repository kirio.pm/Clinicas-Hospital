package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CatalogoExamenDao;
import org.ues.edu.bad115.dao.ExamenDao;
import org.ues.edu.bad115.modelos.CatalogoDeExamenes;
import org.ues.edu.bad115.modelos.Examen;

@Service
@Transactional
public class ExamenPacienteServiceImpl implements ExamenPacienteService{

	@Autowired
	private CatalogoExamenDao catalogoDao;
	
	@Autowired
	private ExamenDao examenDao;
	
	@Override
	public List<CatalogoDeExamenes> listaCatalogoExamen() {
		return catalogoDao.listaCatalogoExamen();
	}

	@Override
	public void guardarExamen(Examen examen) {
		examenDao.guardarExamen(examen);
	}

	@Override
	public long numeroExamenesPaciente(int id_paciente) {
		// TODO Auto-generated method stub
		return 0;
	}

	

}
