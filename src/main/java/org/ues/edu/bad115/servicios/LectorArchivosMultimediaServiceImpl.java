package org.ues.edu.bad115.servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LectorArchivosMultimediaServiceImpl implements LectorArchivosMultimediaService{
	
	private static final Logger logger = LoggerFactory.getLogger(LectorArchivosMultimediaServiceImpl.class);
	
	@Autowired
	private ManejadorMultimediaService manejador;
	
	@Override
	public boolean existeArchivo(String nombre) {
		// TODO Auto-generated method stub
		return new File(manejador.obtenerRutaAbsoluta()+manejador.obtenerDelimitadorOs()+nombre).exists();
	}

	@Override
	public String leerContenido(String nombre) {
		String file_content = "";
        if (existeArchivo(nombre)){
            File file = new File(manejador.obtenerRutaAbsoluta()+manejador.obtenerDelimitadorOs()+nombre);
            logger.info("File info: name: {}, size (bytes): {}, path: {}, last modification: {}",file.getName(),file.length(),file.getAbsolutePath(),new SimpleDateFormat("MMMM dd, yyyy hh:mm a").format(file.lastModified()));
            file_content = readFile(file);
        }
        return file_content;

	}
	private String readFile(File file){
        String result = "";
        try {
            FileInputStream inputStream = new FileInputStream(file);
            result = streamToString(inputStream);
            inputStream.close();
        } catch (IOException e) {
            logger.error("File not found or FileInputStream not closed");
            e.printStackTrace();
        }
        return result;
    }

    private String streamToString(InputStream stream){
        Scanner scanner = new Scanner(stream).useDelimiter("\\A");
        try{
            return scanner.next();
        }finally {
            scanner.close();
        }
    }

}
