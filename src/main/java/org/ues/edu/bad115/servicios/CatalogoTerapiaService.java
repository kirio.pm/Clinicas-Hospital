package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoTerapia;

public interface CatalogoTerapiaService {
	public List<CatalogoTerapia> listaTerapia();
}
