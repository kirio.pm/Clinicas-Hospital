package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.HospitalizacionDao;
import org.ues.edu.bad115.modelos.Hospitalizacion;

@Service
@Transactional
public class HospitalizacionServiceImpl implements HospitalizacionService {
	
	@Autowired
	private HospitalizacionDao dao;

	@Override
	public boolean addHospitalizacion(LocalDate fechaInicioHosp, LocalDate fechaFinHosp, int idDoctor, int idPaciente,
			int idSala) {
		dao.addHospitalizacion(fechaInicioHosp, fechaFinHosp, idDoctor, idPaciente, idSala);
		return true;
	}

	@Override
	public List<Hospitalizacion> listaHospitalizacion() {
		
		return dao.listaHospitalizacion();
	}

	@Override
	public boolean udpHospitalizacion(int idHospitalizacion, LocalDate fechaInicioHosp, LocalDate fechaFinHosp,
			int idDoctor, int idPaciente, int idSala) {
		dao.udpHospitalizacion(idHospitalizacion, fechaInicioHosp, fechaFinHosp, idDoctor, idPaciente, idSala);
		return true;
	}

	@Override
	public Hospitalizacion buscarHospitalizacion(int idHospitalizacion) {
		
		return dao.buscarHospitalizacion(idHospitalizacion);
	}
}
