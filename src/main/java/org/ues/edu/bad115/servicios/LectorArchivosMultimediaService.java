package org.ues.edu.bad115.servicios;

public interface LectorArchivosMultimediaService {
	boolean existeArchivo(String nombre);

    String leerContenido(String nombre);

}
