package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.RegionDao;
import org.ues.edu.bad115.modelos.Region;

@Service
@Transactional
public class RegionServiceImpl implements RegionService{
	
	@Autowired
	private RegionDao dao;
	
	@Override
	public boolean addRegion(String continente, String pais) {
		dao.addRegion(continente, pais);
		return true;
	}

	@Override
	public List<Region> listaRegion() {
		
		return dao.listaRegion();
	}

	@Override
	public boolean udpRegion(int idRegion, String continente, String pais) {
		dao.udpRegion(idRegion, continente, pais);
		return true;
	}

	@Override
	public Region buscarRegion(int idRegion) {
		
		return dao.buscarRegion(idRegion);
	}
	


}
