package org.ues.edu.bad115.servicios;


import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.ues.edu.bad115.dao.UserDao;
import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.modelos.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public boolean isUsernameUnique(int id, String username) {
		User user = findByUsername(username);
		return (user == null || ((id != 0) && (user.getId() == id)));
	}

	@Override
	public User findById(int id) {
		return dao.findById(id);
	}

	@Override
	public User findByUsername(String username) {
		return dao.findByUsername(username);
	}

	@Override
	public List<User> findAll() {
		return dao.findAll();
	}

	@Override
	public void saveUser(User user) {
		setUserLastLoginAndJoiningDateNow(user);
		encryptUserPassword(user);
		dao.saveUser(user);
	}

	@Override
	public void updateUser(User user) {
		updateUserPasswordIfNotEquals(user);
		dao.updateUser(user);
	}

	@Override
	public void deleteByUsername(String username) {
		User user = dao.findByUsername(username);
		
		dao.deleteUser(user);
	}
	
	public void setUserLastLoginAndJoiningDateNow(User user) {
		user.setJoining_date(Date.from(Instant.now()));
		user.setLast_login(Date.from(Instant.now()));
	}
	
	public void encryptUserPassword(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
	}
	
	public void updateUserPasswordIfNotEquals(User user) {
		String currentUserPassword = dao.findById(user.getId()).getPassword();
		if(!user.getPassword().equals(currentUserPassword))
			user.setPassword(passwordEncoder.encode(user.getPassword()));
	}

	@Override
	public Empleado getPersonalByUsername(String username) {
		return dao.getPersonalFromUser(username);
	}

	@Override
	public List<Profile> getProfileByUsername(String username) {
		return dao.findAllProfilesFromUser(username); 	
	}

	@Override
	public User getInitializeUserByUsername(String username) {
		return dao.getInitializeUserByUsername(username);
	}

	@Override
	public void updateUserLastLogging(int id) {
		dao.updateUserLastLogging(id);
	}

	@Override
	public User getInitializeUserById(int id) {
		return dao.getInitializeUserById(id);
	}

}

