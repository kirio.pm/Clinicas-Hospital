package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.ClinicaDao;
import org.ues.edu.bad115.modelos.Clinica;

@Service
@Transactional
public class ClinicaServiceImpl implements ClinicaService {
	
	@Autowired
	private ClinicaDao dao;

	@Override
	public boolean addClinica(String direccionCli, String nombreCli, String telefonoCli, int idRegion) {
		dao.addClinica(direccionCli, nombreCli, telefonoCli, idRegion);
		return true;
	}

	@Override
	public List<Clinica> listaClinica() {

		return dao.listaClinica();
	}

	@Override
	public boolean udpClinica(int idClinica, String direccionCli, String nombreCli, String telefonoCli, int idRegion) {
		dao.udpClinica(idClinica, direccionCli, nombreCli, telefonoCli, idRegion);
		return true;
	}

	@Override
	public Clinica buscarClinica(int idClinica) {
		// TODO Auto-generated method stub
		return dao.buscarClinica(idClinica);
	}
}
