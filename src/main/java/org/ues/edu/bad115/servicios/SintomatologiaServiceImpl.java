package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.SintomatologiaDao;
import org.ues.edu.bad115.modelos.Sintomatologia;

@Service
@Transactional
public class SintomatologiaServiceImpl implements SintomatologiaService {
	
	@Autowired
	private SintomatologiaDao dao;

	@Override
	public boolean addSintomatologia(String sintomas, int idConsulta) {
		dao.addSintomatologia(sintomas, idConsulta);
		return true;
	}

	@Override
	public List<Sintomatologia> listaSintoma() {
		return dao.listaSintoma();
	}

	@Override
	public boolean udpSintomatologia(int idSintomatologia, String sintomas, int idConsulta) {
		dao.udpSintomatologia(idSintomatologia, sintomas, idConsulta);
		return true;
	}

	@Override
	public Sintomatologia buscarSintoma(int idSintomatologia) {
		
		return dao.buscarSintoma(idSintomatologia);
	}
	
	
}
