package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.TratamientoMedicoDao;
import org.ues.edu.bad115.modelos.TratamientoMedico;

@Service
@Transactional
public class TratamientoMedicoServiceImpl implements TratamientoMedicoService {
	
	@Autowired
	public TratamientoMedicoDao dao;

	@Override
	public boolean addTratamientoMedico(int frecuenciaTra, String tipoTra, int idDiagnostico) {
		dao.addTratamientoMedico(frecuenciaTra, tipoTra, idDiagnostico);
		return true;
	}

	@Override
	public List<TratamientoMedico> listaTratamientoMedico() {
		
		return dao.listaTratamientoMedico();
	}

	@Override
	public boolean udpTratamientoMedico(int idTratamiento, int frecuenciaTra, String tipoTra, int idDiagnostico) {
		dao.udpTratamientoMedico(idTratamiento, frecuenciaTra, tipoTra, idDiagnostico);
		return true;
	}

	@Override
	public TratamientoMedico buscarTratamientoMedico(int idTratamiento) {
		
		return dao.buscarTratamientoMedico(idTratamiento);
	}
}
