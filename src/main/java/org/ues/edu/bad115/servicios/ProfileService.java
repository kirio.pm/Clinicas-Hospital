package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.Profile;

public interface ProfileService {
	public Profile findById(int id);

	public Profile findByType(String type);

	public List<Profile> findAll();
	
	public Profile findInitializeProfileById(int id);

}
