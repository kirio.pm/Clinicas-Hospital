package org.ues.edu.bad115.servicios;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.CitaDao;
import org.ues.edu.bad115.dao.ConsultaMedicaDao;
import org.ues.edu.bad115.modelos.CitaMedica;


@Service
@Transactional
public class CitaServiceImpl implements CitaService {

	@Autowired
	private CitaDao dao;
	
	@Override
	public boolean addCita(LocalDate fechaCita, String horaCita, String medioRegistro, int idDoctor, int idCatalogoCita,
			int idPaciente) {
		dao.addCita(fechaCita, horaCita, medioRegistro, idDoctor, idCatalogoCita, idPaciente);
		return true;
	}

	@Override
	public List<CitaMedica> listaCitas() {
		// TODO Auto-generated method stub
		return dao.listaCitas();
	}

	@Override
	public boolean EditarCita(int idCita, LocalDate fechaCita, String horaCita, String medioRegistro, int idDoctor,
			int idCatalogoCita, int idPaciente) {
		dao.EditarCita(idCita, fechaCita, horaCita, medioRegistro, idDoctor, idCatalogoCita, idPaciente);
		return true;
	}

	@Override
	public CitaMedica buscarCita(int idCita) {
		// TODO Auto-generated method stub
		return dao.buscarCita(idCita);
	}

	

	
}
