package org.ues.edu.bad115.servicios;

import java.util.List;

import org.ues.edu.bad115.modelos.Clinica;

public interface ClinicaService {
	
	public boolean addClinica(String direccionCli, String nombreCli, String telefonoCli, int idRegion);
	public List<Clinica> listaClinica();
	public boolean udpClinica(int idClinica, String direccionCli, String nombreCli, String telefonoCli, int idRegion);
	public Clinica buscarClinica(int idClinica);
}
