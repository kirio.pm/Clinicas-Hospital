package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.DoctorDao;
import org.ues.edu.bad115.modelos.Doctor;

@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	private DoctorDao dao;
	
	@Override
	public boolean addDoctor(String especialidadDoc, int idEmpleado) {
		dao.addDoctor(especialidadDoc, idEmpleado);
		return true;
	}

	@Override
	public List<Doctor> listaDoctor() {
		return dao.listaDoctor();
	}

	@Override
	public boolean udpDoctor(int idDoctor, String especialidadDoc, int idEmpleado) {
		dao.udpDoctor(idDoctor, especialidadDoc, idEmpleado);
		return true;
	}

	@Override
	public Doctor buscarDoctor(int idDoctor) {
		
		return dao.buscarDoctor(idDoctor);
	}

}
