package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.SeguimientoDao;
import org.ues.edu.bad115.modelos.SeguimientoCirugia;

@Service
@Transactional
public class SeguimientoServiceImpl implements SeguimientoService{
	
	@Autowired
	private SeguimientoDao dao;

	@Override
	public boolean addSeguimiento(String tipoSeguimiento, int idCirugia) {
		// TODO Auto-generated method stub
		dao.addSeguimiento(tipoSeguimiento, idCirugia);
		return true;
	}

	@Override
	public List<SeguimientoCirugia> listaSeguimiento() {
		// TODO Auto-generated method stub
		return dao.listaSeguimiento();
	}

	@Override
	public boolean udpSeguimiento(int idSeguimiento, String tipoSeguimiento, int idCirugia) {
		// TODO Auto-generated method stub
		dao.udpSeguimiento(idSeguimiento, tipoSeguimiento, idCirugia);
		return true;
	}

	@Override
	public SeguimientoCirugia buscarSeguimiento(int idSeguimiento) {
		// TODO Auto-generated method stub
		return dao.buscarSeguimiento(idSeguimiento);
	}
}
