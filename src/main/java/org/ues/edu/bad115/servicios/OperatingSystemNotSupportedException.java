package org.ues.edu.bad115.servicios;

public class OperatingSystemNotSupportedException extends Exception {
	public OperatingSystemNotSupportedException() {
    }

    public OperatingSystemNotSupportedException(String message) {
        super(message);
    }


}
