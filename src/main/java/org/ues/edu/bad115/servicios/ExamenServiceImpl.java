package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.ExamenDao;
import org.ues.edu.bad115.dto.ArchivoMedia;
import org.ues.edu.bad115.modelos.Examen;

@Service
@Transactional
public class ExamenServiceImpl implements ExamenService{
	
	@Autowired
	private ExamenDao dao;

	@Override
	public ArchivoMedia getExamenMediaInformacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarArchivoMedia(int id_examen, ArchivoMedia media) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarArchivoMedia(int id_examen) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Examen> buscarTodosLosExamenes(int id_consulta) {
		return dao.buscarTodosLosExamenesDeConsulta(id_consulta);
	}

	@Override
	public Examen buscarExamenPorId(int id_examen) {
		return dao.buscarExamen(id_examen);
	}

	@Override
	public void actualizarExamen(Examen examen) {
		dao.editarExamen(examen);
	}

}
