package org.ues.edu.bad115.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ues.edu.bad115.dao.SignosVitalesDao;
import org.ues.edu.bad115.modelos.SignosVitales;

@Service
@Transactional
public class SignosVitalesServiceImpl implements SignosVitalesService {
	
	@Autowired
	private SignosVitalesDao dao;
	
	@Override
	public boolean addSignosVitales(double alturaSiv, int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv, double temperaturaSiv, int idConsulta) {
		dao.addSignosVitales(alturaSiv, frecuenciaCardiacaSiv, pesoSiv, presionArterialSiv, temperaturaSiv, idConsulta);
		
		return true;
	}

	@Override
	public List<SignosVitales> listaSigno() {
		// TODO Auto-generated method stub
		return dao.listaSigno();
	}

	@Override
	public boolean udpSignoVitales(int idSignosVitales, double alturaSiv, int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv, double temperaturaSiv, int idConsulta) {
		dao.udpSignoVitales(idSignosVitales, alturaSiv, frecuenciaCardiacaSiv, pesoSiv, presionArterialSiv, temperaturaSiv, idConsulta);
		return true;
	}

	@Override
	public SignosVitales buscarSigno(int idSignosVitales) {
		
		return dao.buscarSigno(idSignosVitales);
	}

}
