package org.ues.edu.bad115.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/","/index"})
public class HomeController {
	
	@GetMapping
	public String showHomePage(ModelMap model) {
		return "index";
	}

}
