package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Sintomatologia;
import org.ues.edu.bad115.servicios.ConsultaMedicaService;
import org.ues.edu.bad115.servicios.SintomatologiaService;

@Controller
@RequestMapping("/Sintomatologia")
public class SintomatologiaController {
	
	@Autowired
	private SintomatologiaService sin;
	
	@Autowired
	private ConsultaMedicaService con;
	
	@RequestMapping(value={"/","/listaSintomas"}, method=RequestMethod.GET)
	public String mostrarSintoma(ModelMap model) {
		model.addAttribute("lista", sin.listaSintoma());
		return "listaSintomas";
	}
	
	@RequestMapping(value={"/nuevoSintoma/{px}"}, method=RequestMethod.GET)
	public String addNewSintoma(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("sintoma", new Sintomatologia());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevoSintoma";
	}
	
	@RequestMapping(value={"/nuevoSintoma/{px}"}, method=RequestMethod.POST)
	public String guardarSintoma(@Valid @ModelAttribute("sintoma") Sintomatologia sintoma, BindingResult result, ModelMap model,@PathVariable("px") int px) {
		if(result.hasErrors()) {
			model.addAttribute("sintoma", new Sintomatologia());
			return "nuevoSintoma";
		}if(sin.addSintomatologia(sintoma.getSintomas(),px) == true) {
			return "redirect:/consultaMedica/";
		}
		return "nuevoSintoma";
	}
	
	@RequestMapping(value={"/editarSin-{idSintomatologia}"}, method=RequestMethod.GET)
	public String recuperarSintoma(ModelMap model, @PathVariable("idSintomatologia") int idSintomatologia) {
	Sintomatologia sin1= sin.buscarSintoma(idSintomatologia);
	model.addAttribute("sinUnico", sin1);
	model.addAttribute("listaCon", con.listaConsulta());
		return "editarSin";
	}
	
	@RequestMapping(value={"/editarSin-{idSintomatologia}"}, method=RequestMethod.POST)
	public String editarSintoma(@Valid @ModelAttribute("sinUnico")  Sintomatologia sinUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("sinUnico", new Sintomatologia());
			return "editarSin";
		}if(sin.udpSintomatologia(sinUnico.getIdSintomatologia(), sinUnico.getSintomas(), sinUnico.getIdConsulta().getIdConsulta())== true) {
			return "successSintoma";
		}
		return "editarSin";
	}
}
