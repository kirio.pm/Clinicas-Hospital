package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.TratamientoMedico;
import org.ues.edu.bad115.servicios.DiagnosticoService;
import org.ues.edu.bad115.servicios.TratamientoMedicoService;

@Controller
@RequestMapping("/tratamiento")
public class TratamientoMedicoController {

	@Autowired
	public TratamientoMedicoService traMe;
	
	@Autowired
	public DiagnosticoService diag;
	
	@RequestMapping(value={"/","/listaTratamiento"}, method=RequestMethod.GET)
	public String mostrarTratamiento(ModelMap model) {
		model.addAttribute("lista", traMe.listaTratamientoMedico());
		return "listaTratamiento";
	}
	
	@RequestMapping(value={"/nuevoTratamiento/{px}"}, method=RequestMethod.GET)
	public String addNewTratamiento(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("tratamiento", new TratamientoMedico());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevoTratamiento";
	}
	
	@RequestMapping(value={"/nuevoTratamiento/{px}"}, method=RequestMethod.POST)
	public String guardarTratamiento(@Valid @ModelAttribute("tratamiento") TratamientoMedico tra, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors()) {
			model.addAttribute("tratamiento", new TratamientoMedico());
			return "nuevoTratamiento";
		}if(traMe.addTratamientoMedico(tra.getFrecuenciaTra(), tra.getTipoTra(),px) == true) {
			return "redirect:/tratamiento/";
		}
		return "nuevoTratamiento";
	}
	
	@RequestMapping(value={"/editarTra-{idTratamiento}"}, method=RequestMethod.GET)
	public String recuperarTratamiento(ModelMap model, @PathVariable("idTratamiento") int idTratamiento) {
	TratamientoMedico tra1= traMe.buscarTratamientoMedico(idTratamiento);
	model.addAttribute("traUnico", tra1);
	model.addAttribute("listaDiag", diag.listaDiagnostico());
		return "editarTra";
	}
	
	@RequestMapping(value={"/editarTra-{idTratamiento}"}, method=RequestMethod.POST)
	public String editarTratamiento(@Valid @ModelAttribute("traUnico")  TratamientoMedico traUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("traUnico", new TratamientoMedico());
			return "editarTra";
		}if(traMe.udpTratamientoMedico(traUnico.getIdTratamiento(), traUnico.getFrecuenciaTra(), traUnico.getTipoTra(), traUnico.getIdDiagnostico().getIdDiagnostico())== true) {
			return "successTratamiento";
		}
		return "editarTra";
	}
	
}
