package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Clinica;
import org.ues.edu.bad115.modelos.Hospital;
import org.ues.edu.bad115.servicios.HospitalService;
import org.ues.edu.bad115.servicios.RegionService;

@Controller
@RequestMapping("/hospital")
public class HopitalController {
	
	@Autowired
	private HospitalService hs;
	
	@Autowired
	private RegionService reg;
	
	@RequestMapping(value={"/","/listaHospital"}, method=RequestMethod.GET)
	public String mostrarHospital(ModelMap model) {
		model.addAttribute("lista", hs.listaHospital());
		//model.addAttribute("listaReg", reg.listaRegion());
		return "listaHospital";
	}
	
	@RequestMapping(value={"/nuevoHospital"}, method=RequestMethod.GET)
	public String addNewHospital(ModelMap model) {
		model.addAttribute("hospital", new Hospital());
		model.addAttribute("listaReg", reg.listaRegion());
		return "nuevoHospital";
	}
	
	@RequestMapping(value={"/nuevoHospital"}, method=RequestMethod.POST)
	public String guardarHospital(@Valid @ModelAttribute("hospital") Hospital hos, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("hospital", new Hospital());
			return "nuevoHospital";
		}if(hs.addHospital(hos.getDireccionHos(), hos.getNombreHos(), hos.getTelefonoHos(), 
				hos.getIdRegion().getIdRegion())== true) {
			return "redirect:/hospital/";
		}
		return "nuevoHospital";
	}
	
	@RequestMapping(value={"/editarHos-{idHospital}"}, method=RequestMethod.GET)
	public String recuperarHospital(ModelMap model, @PathVariable("idHospital") int idHospital) {
	Hospital hos1 = hs.buscarHospital(idHospital);		
	model.addAttribute("hosUnico", hos1);
	model.addAttribute("listaReg", reg.listaRegion());
		return "editarHos";
	}
	
	@RequestMapping(value={"/editarHos-{idHospital}"}, method=RequestMethod.POST)
	public String editarHospital(@Valid @ModelAttribute("hosUnico")  Hospital hosUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("hospital", new Hospital());
			model.addAttribute("listaReg", reg.listaRegion());
			return "editarHos";
		}if(hs.udpHospital(hosUnico.getIdHospital(), hosUnico.getDireccionHos(), hosUnico.getNombreHos(), 
				hosUnico.getTelefonoHos(), hosUnico.getIdRegion().getIdRegion())== true) {
			return "successHospital";
		}
		return "editarHos";
	}
}
