package org.ues.edu.bad115.controladores;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.ues.edu.bad115.modelos.User;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.servicios.EmpleadoService;
import org.ues.edu.bad115.servicios.ProfileService;
import org.ues.edu.bad115.servicios.UserService;

@Controller
@RequestMapping("/usuarios")
@SessionAttributes({ "roles" })
public class UserController {
	public static final String USER_LIST_VIEW = "userslist";

	public static final String USER_INFO_VIEW = "datailUser";

	public static final String USER_FROM_VIEW = "userRegistration";

	public static final String USER_REGISTRATION_SUCCESS_VIEW = "userRegistrationSuccess";
	
	public static final String USER_LIST_REDIRECTION = "redirect:/usuarios/";
	
	public static final String EDIT_USER_FROM_VIEW = "editUser";
	@Autowired
	UserService userService;
	
	@Autowired
	EmpleadoService emp;
	
	@Autowired
	EmpleadoService personalService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	ProfileService profileService;

	@GetMapping({ "/", "/listaUsuarios" })
	public String showUserList(ModelMap model, Principal principal) {
		model.addAttribute("loggedinuser",principal.getName());
		model.addAttribute("users", userService.findAll());
		return USER_LIST_VIEW;
	}

	@GetMapping("/nuevoUsuario")
	public String newUser(ModelMap model) {
		model.addAttribute("user", new User());
		model.addAttribute("empleados", emp.listaEmpleados());
		model.addAttribute("edit", false);
		return USER_FROM_VIEW;
	}

	@PostMapping("/nuevoUsuario")
	public String saveUser(@Valid User user, BindingResult result, ModelMap model) {
		userService.saveUser(user);
		model.addAttribute("success", messageSource.getMessage("Success.user.createmessage",
				new String[] { user.getUsername() }, Locale.getDefault()));
		return USER_REGISTRATION_SUCCESS_VIEW;
	}

	@GetMapping("editarUsuario-{id}")
	public String editUser(@PathVariable int id, ModelMap model) {
		model.addAttribute("user", userService.getInitializeUserById(id));
		return EDIT_USER_FROM_VIEW;
	}

	@PostMapping("editarUsuario-{id}")
	public String updateUser(@Valid @ModelAttribute("user") User user, ModelMap model) {
		User us = userService.getInitializeUserById(user.getId());
		user.setJoining_date(us.getJoining_date());
		user.setLast_login(user.getLast_login());
		userService.updateUser(user);
		model.addAttribute("success", messageSource.getMessage("Success.user.editmessage",
				new String[] { user.getUsername() }, Locale.getDefault()));
		return USER_REGISTRATION_SUCCESS_VIEW;
	}

	@GetMapping("eliminarUsuario-{username}")
	public String deleteUser(@PathVariable String username) {
		userService.deleteByUsername(username);
		return USER_LIST_REDIRECTION;
	}

	@ModelAttribute("roles")
	public List<Profile> initializeProfiles() {
		return profileService.findAll();
	}


}
