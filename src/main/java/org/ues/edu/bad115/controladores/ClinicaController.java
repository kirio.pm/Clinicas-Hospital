package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Clinica;
import org.ues.edu.bad115.servicios.ClinicaService;
import org.ues.edu.bad115.servicios.RegionService;

@Controller
@RequestMapping("/Clinica")
public class ClinicaController {
 
	@Autowired
	private ClinicaService cli;
	
	@Autowired
	private RegionService reg;
	
	@RequestMapping(value={"/","/listaClinica"}, method=RequestMethod.GET)
	public String mostrarClinica(ModelMap model) {
		model.addAttribute("lista", cli.listaClinica());
		//model.addAttribute("listaReg", reg.listaRegion());
		return "listaClinica";
	}
	
	@RequestMapping(value={"/nuevaClinica"}, method=RequestMethod.GET)
	public String addNewClinica(ModelMap model) {
		model.addAttribute("clinica", new Clinica());
		model.addAttribute("listaReg", reg.listaRegion());
		return "nuevaClinica";
	}
	
	@RequestMapping(value={"/nuevaClinica"}, method=RequestMethod.POST)
	public String guardarClinica(@Valid @ModelAttribute("clinica") Clinica clin, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("clinica", new Clinica());
			return "nuevaClinica";
		}if(cli.addClinica(clin.getDireccionCli(), clin.getNombreCli(),
				clin.getTelelfonoCli(), clin.getIdRegion().getIdRegion())== true) {
			return "successClinica";
		}
		return "nuevaClinica";
	}
	
	@RequestMapping(value={"/editarCli-{idClinica}"}, method=RequestMethod.GET)
	public String recuperarclinica(ModelMap model, @PathVariable("idClinica") int idClinica) {
	Clinica cli1= cli.buscarClinica(idClinica);		
	model.addAttribute("cliUnico", cli1);
	model.addAttribute("listaReg", reg.listaRegion());
		return "editarCli";
	}
	
	@RequestMapping(value={"/editarCli-{idClinica}"}, method=RequestMethod.POST)
	public String editarClinica(@Valid @ModelAttribute("cliUnico")  Clinica cliUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("clinica", new Clinica());
			model.addAttribute("listaReg", reg.listaRegion());
			return "editarCli";
		}if(cli.udpClinica(cliUnico.getIdClinica(), cliUnico.getDireccionCli(), cliUnico.getNombreCli(),
				cliUnico.getTelelfonoCli(), cliUnico.getIdRegion().getIdRegion())== true) {
			return "successClinica";
		}
		return "editarCli";
	}
}
