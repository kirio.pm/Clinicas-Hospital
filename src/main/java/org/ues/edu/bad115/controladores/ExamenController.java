package org.ues.edu.bad115.controladores;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.ues.edu.bad115.modelos.ConsultaMedica;
import org.ues.edu.bad115.modelos.Examen;
import org.ues.edu.bad115.servicios.ConsultaMedicaService;
import org.ues.edu.bad115.servicios.ExamenPacienteService;
import org.ues.edu.bad115.servicios.ExamenService;
import org.ues.edu.bad115.servicios.StorageService;

@Controller
@RequestMapping("/examen")
public class ExamenController {

	@Autowired
	private ExamenPacienteService examentPacienteService;

	@Autowired
	private ExamenService examenService;

	@Autowired
	private ConsultaMedicaService consultaService;

	@Autowired
	private StorageService storageService;
	
	@PostConstruct
	public void init() {
		storageService.init();
	}

	@GetMapping("/{id_consulta}")
	public String mostrarListadeExamenes(@PathVariable int id_consulta, ModelMap model) {
		model.addAttribute("id_consulta", id_consulta);
		model.addAttribute("examenes", examenService.buscarTodosLosExamenes(id_consulta));
		return "lista_examen";
	}

	@GetMapping("/{id_consulta}/nuevo-examen")
	public String crearNuevoExamen(@PathVariable int id_consulta, ModelMap model) {
		model.addAttribute("id_consulta", id_consulta);
		model.addAttribute("catalogo", examentPacienteService.listaCatalogoExamen());
		model.addAttribute("examen", new Examen());
		return "nuevo_examen";
	}

	@PostMapping("/{id_consulta}/nuevo-examen")
	public String guardarExamen(@ModelAttribute Examen examen, @PathVariable int id_consulta) {
		ConsultaMedica consulta = consultaService.buscarConsulta(id_consulta);
		consulta.getExamenList().add(examen);
		examen.setIdConsultaMedica(consulta);
		consultaService.actualizarConsulta(consulta);
		return "redirect:/examen/" + id_consulta;
	}

	@GetMapping("/{id_consulta}/examen/{id_examen}/add-media")
	public String agregarArchivoMultimedia(ModelMap model,@PathVariable("id_consulta") int id_consulta,@PathVariable("id_examen") int id_examen) {	
		model.addAttribute("id_examen", id_examen);
		model.addAttribute("id_consulta",id_consulta);
		return "add_media_file";
	}
	
	@PostMapping("/{id_consulta}/examen/{id_examen}/add-media")
	public String guardarArchivoMultimedia(@RequestParam("file") MultipartFile file, @PathVariable("id_consulta") int id_consulta,@PathVariable("id_examen") int id_examen) {
		Examen examen = examenService.buscarExamenPorId(id_examen);
		examen.setNombreOriginal(file.getOriginalFilename());
		examen.setFormato(file.getOriginalFilename().split("\\.")[1].toLowerCase());
		examen.setTamanio(file.getSize());
		examen.setRutaArchivo("/upload-files/");
		examenService.actualizarExamen(examen);
		storageService.store(file);
		return "redirect:/examen/" + id_consulta;
	}
	
	@GetMapping("/{id_examen}/media/{file_name}")
	public String mostrarArchivoMedia(@PathVariable int id_examen,@PathVariable String file_name, ModelMap model) {
		model.addAttribute("file_info",examenService.buscarExamenPorId(id_examen));
		return "show_media_file";
	}
	
	@GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

}
