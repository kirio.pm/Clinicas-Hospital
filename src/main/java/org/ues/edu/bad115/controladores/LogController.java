package org.ues.edu.bad115.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.servicios.LogServices;

@Controller
@RequestMapping("/log")
public class LogController {

	
	@Autowired
	private LogServices logServices;
	
	
	@RequestMapping(value={"/","/log"},method=RequestMethod.GET)
	public String mostrarExpediente(ModelMap model) {
	model.addAttribute("Lista", logServices.listaCitas());
	return "ListaLog";
    }
}
