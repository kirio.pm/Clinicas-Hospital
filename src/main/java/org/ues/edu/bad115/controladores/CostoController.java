package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.CostosPorServicios;
import org.ues.edu.bad115.servicios.CostoService;

@Controller
@RequestMapping("/costo")
public class CostoController {
	
	@Autowired
	private CostoService cos;
	
	@RequestMapping(value={"/","/listaCosto"}, method=RequestMethod.GET)
	public String mostrarCosto(ModelMap model) {
		model.addAttribute("lista", cos.listaCosto());
		return "listaCosto";
	}
	
	@RequestMapping(value={"/nuevoCosto"}, method=RequestMethod.GET)
	public String addNewCosto(ModelMap model) {
		model.addAttribute("costo", new CostosPorServicios());				
		return "nuevoCosto";
	}
	
	@RequestMapping(value={"/nuevoCosto"}, method=RequestMethod.POST)
	public String guardarCosto(@Valid @ModelAttribute("costo") CostosPorServicios costo, 
			BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("costo", new CostosPorServicios());
			return "nuevoCosto";
		}if(cos.addCosto(costo.getFechaCosto(), costo.getUnidadMonetaria())==true) {
			return "successCosto";
		}
		return "nuevoCosto";
	}
	
	@RequestMapping(value={"/editarCos-{idCosto}"}, method=RequestMethod.GET)
	public String recuperarCosto(ModelMap model, @PathVariable("idCosto") int idCosto) {
	CostosPorServicios cos1= cos.buscarCosto(idCosto);
	model.addAttribute("cosUnico", cos1);
		return "editarCos";
	}
	
	@RequestMapping(value={"/editarCos-{idCosto}"}, method=RequestMethod.POST)
	public String editarCosto(@Valid @ModelAttribute("cosUnico") CostosPorServicios cosUnico, 
			BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("cosUnico", new CostosPorServicios());
			return "editarCos";
		}if(cos.udpCosto(cosUnico.getIdCosto(), cosUnico.getFechaCosto(), cosUnico.getUnidadMonetaria())== true) {
			return "successCosto";
		}
		return "editarCos";
	}
}
