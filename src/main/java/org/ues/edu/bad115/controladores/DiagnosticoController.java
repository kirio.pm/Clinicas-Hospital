package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Diagnostico;
import org.ues.edu.bad115.servicios.CatalogoDiagnosticoService;
import org.ues.edu.bad115.servicios.ConsultaMedicaService;
import org.ues.edu.bad115.servicios.DiagnosticoService;

@Controller
@RequestMapping("/diagnostico")
public class DiagnosticoController {
	
	@Autowired
	public DiagnosticoService diag;
	@Autowired
	public CatalogoDiagnosticoService cds;
	@Autowired
	public ConsultaMedicaService cms;
	
	@RequestMapping(value={"/","/listaDiagnostico"}, method=RequestMethod.GET)
	public String mostrarDiagnostico(ModelMap model) {
		model.addAttribute("lista", diag.listaDiagnostico());
		return "listaDiagnostico";
	}
	
	@RequestMapping(value={"/nuevoDiagnostico/{px}"}, method=RequestMethod.GET)
	public String addNewDiagnostico(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("diagnostico", new Diagnostico());
		model.addAttribute("listaCatDia", cds.listaCataDiag());
		model.addAttribute("listaCon", cms.listaConsulta());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevoDiagnostico";
	}
	
	@RequestMapping(value={"/nuevoDiagnostico/{px}"}, method=RequestMethod.POST)
	public String guardarDiagnostico(@Valid @ModelAttribute("diagnostico") Diagnostico diagnostico, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors()) {
			model.addAttribute("diagnostico", new Diagnostico());
			return "nuevoDiagnostico";
		}if(diag.addDiagnostico(diagnostico.getIdCataDiag().getIdCataDiag(),px)==true) {
			return "redirect:/diagnostico/";
		}
		return "nuevoDiagnostico";
	}
	
	@RequestMapping(value={"/editarDiag-{idDiagnostico}"}, method=RequestMethod.GET)
	public String recuperarDiagnostico(ModelMap model, @PathVariable("idDiagnostico") int idDiagnostico) {
	Diagnostico diag1= diag.buscarDiagnostico(idDiagnostico);
	model.addAttribute("diagUnico", diag1);
	model.addAttribute("listaCatDia", cds.listaCataDiag());
	model.addAttribute("listaCon", cms.listaConsulta());
		return "editarDiag";
	}
	
	@RequestMapping(value={"/editarDiag-{idDiagnostico}"}, method=RequestMethod.POST)
	public String editarDiagnostico(@Valid @ModelAttribute("diagUnico") Diagnostico diagUnico, 
			BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("diagUnico", new Diagnostico());
			return "editarDiag";
		}if((diag.udpDiagnostico(diagUnico.getIdDiagnostico(), diagUnico.getFechaDiag(), 
				diagUnico.getIdCataDiag().getIdCataDiag(), diagUnico.getIdConsulta().getCorrelativoCon()))== true) {
			return "successDiagnostico";
		}
		return "editarDiag";
	}
}
