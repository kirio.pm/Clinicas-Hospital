package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Region;
import org.ues.edu.bad115.modelos.TratamientoMedico;
import org.ues.edu.bad115.servicios.RegionService;

@Controller
@RequestMapping("/region")
public class RegionController {
	@Autowired
	private RegionService reg;
	
	@RequestMapping(value={"/","/listaRegion"}, method=RequestMethod.GET)
	public String mostrarRegion(ModelMap model) {
		model.addAttribute("lista", reg.listaRegion());
		return "listaRegion";
	}
	
	@RequestMapping(value={"/nuevaRegion"}, method=RequestMethod.GET)
	public String addNewRegion(ModelMap model) {
		model.addAttribute("region", new Region());
		//model.addAttribute("listaDiag", diag.listaDiagnostico());
		return "nuevaRegion";
	}
	
	@RequestMapping(value={"/nuevaRegion"}, method=RequestMethod.POST)
	public String guardarRegion(@Valid @ModelAttribute("region") Region region, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("region", new Region());
			return "nuevaRegion";
		}if(reg.addRegion(region.getContinente(), region.getPais())== true) {
			return "successRegion";
		}
		return "nuevaRegion";
	}
	
	@RequestMapping(value={"/editarReg-{idRegion}"}, method=RequestMethod.GET)
	public String recuperarRegion(ModelMap model, @PathVariable("idRegion") int idRegion) {
	Region reg1 = reg.buscarRegion(idRegion);
	model.addAttribute("regUnico", reg1);
		return "editarReg";
	}
	
	@RequestMapping(value={"/editarReg-{idRegion}"}, method=RequestMethod.POST)
	public String editarRegion(@Valid @ModelAttribute("regUnico")  Region regUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("regUnico", new Region());
			return "editarReg";
		}if(reg.udpRegion(regUnico.getIdRegion(), regUnico.getContinente(), regUnico.getPais())== true) {
			return "successRegion";
		}
		return "editarReg";
	}
}

