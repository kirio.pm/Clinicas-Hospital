package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Cirugia;
import org.ues.edu.bad115.modelos.SeguimientoCirugia;
import org.ues.edu.bad115.servicios.CirugiaService;
import org.ues.edu.bad115.servicios.SeguimientoService;

@Controller
@RequestMapping("/seguimiento")
public class SeguimientoController {
	
	@Autowired
	private SeguimientoService sc;
	
	@Autowired
	private CirugiaService cs;
	
	@RequestMapping(value={"/","/listaSeguimiento"}, method=RequestMethod.GET)
	public String mostrarSeguimiento(ModelMap model) {
		model.addAttribute("lista", sc.listaSeguimiento());
		return "listaSeguimiento";
	}
	
	@RequestMapping(value={"/nuevoSeguimiento"}, method=RequestMethod.GET)
	public String addNewSeguimiento(ModelMap model) {
		model.addAttribute("seguimiento", new SeguimientoCirugia());
		model.addAttribute("listaCir", cs.listaCirugia());
		return "nuevoSeguimiento";
	}
	
	@RequestMapping(value={"/nuevoSeguimiento"}, method=RequestMethod.POST)
	public String guardarSeguimiento(@Valid @ModelAttribute("seguimiento") SeguimientoCirugia seg, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("seguimiento", new SeguimientoCirugia());
			return "nuevoSeguimiento";
		}if(sc.addSeguimiento(seg.getTipoSeguimiento(), seg.getIdCirugia().getIdCirugia())== true) {
			return "successSeguimiento";
		}
		return "nuevoSeguimiento";
	}
	
	@RequestMapping(value={"/editarSeg-{idSeguimiento}"}, method=RequestMethod.GET)
	public String recuperarCirugia(ModelMap model, @PathVariable("idSeguimiento") int idSeguimiento) {
		SeguimientoCirugia seg1 = sc.buscarSeguimiento(idSeguimiento);		
		model.addAttribute("segUnico", seg1);
		model.addAttribute("listaCir", cs.listaCirugia());
		return "editarSeg";
	}
	
	@RequestMapping(value={"/editarSeg-{idSeguimiento}"}, method=RequestMethod.POST)
	public String editarSeguimiento(@Valid @ModelAttribute("segUnico")  SeguimientoCirugia segUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("segUnico", new SeguimientoCirugia());
			model.addAttribute("listaCir", cs.listaCirugia());
			return "editarSeg";
		}if(sc.udpSeguimiento(segUnico.getIdSeguimiento(), segUnico.getTipoSeguimiento(), segUnico.getIdCirugia().getIdCirugia()) == true) {
			return "successSeguimiento";
		}
		return "editarSeg";
	}
}
