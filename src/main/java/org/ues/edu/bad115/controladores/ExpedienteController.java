package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.ExpedienteClinico;
import org.ues.edu.bad115.servicios.ExpedienteClinicoService;

@Controller
@RequestMapping("/expediente")

public class ExpedienteController {
	
	@Autowired
	private ExpedienteClinicoService expClinicoServices;
	
	@RequestMapping(value={"/","/ListaExp"},method=RequestMethod.GET)
	public String mostrarExpediente(ModelMap model) {
	System.out.println("****************************************" +  expClinicoServices.ListarExpedientes().size());
	model.addAttribute("Lista", expClinicoServices.ListarExpedientes());
	return "ListaExpediente";
    }
	
	
	@RequestMapping(value={"/detalles-{idExpediente}"}, method=RequestMethod.GET)
	public String addNewExpediente(ModelMap model,@PathVariable("idExpediente") int idExpediente) {
	ExpedienteClinico  exp1=expClinicoServices.BuscarExpediente(idExpediente);
	model.addAttribute("ExpedienteUnico", exp1);
		return "detalles";
	}
	
	@RequestMapping(value={"/newExpediente"},method=RequestMethod.POST)
	public String saveNewEmailAndSend(@Valid @ModelAttribute("expC") ExpedienteClinico expC, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("expC",new ExpedienteClinico());
            return "newExpediente";
        }
		if(expClinicoServices.GuardarExpediente(expC.getIdPaciente().getIdPaciente())==true) {
			
			return "ExpedienteSuccess";
		}
		return "newExpediente";
		
		
	}
	
	

}

