package org.ues.edu.bad115.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.SignosVitales;
import org.ues.edu.bad115.servicios.ConsultaMedicaService;
import org.ues.edu.bad115.servicios.SignosVitalesService;
import javax.validation.Valid;

@Controller
@RequestMapping("/signosVitales")
public class SignosVitaleContoller {

		@Autowired
		private SignosVitalesService signoV;
		@Autowired
		private ConsultaMedicaService con;
		
		@RequestMapping(value={"/","/listaSigno"}, method=RequestMethod.GET)
		public String mostrarDoctor(ModelMap model) {
			model.addAttribute("lista", signoV.listaSigno());
			return "listaSigno";
		}
		
		@RequestMapping(value={"/signosVitales/{px}"}, method=RequestMethod.GET)
		public String addNewSignosVitales(ModelMap model, @PathVariable("px") int id_paciente) {
			model.addAttribute("signos", new SignosVitales());
			model.addAttribute("id_paciente",id_paciente);
			return "nuevoSignosVitales";
		}
		
		@RequestMapping(value={"/signosVitales/{px}"}, method=RequestMethod.POST)
		public String guardarNuevoSigno(@Valid @ModelAttribute("signos") SignosVitales signo, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
			if(result.hasErrors()) {
				model.addAttribute("signos", new SignosVitales());
				return "nuevoSignosVitales";
			}if(signoV.addSignosVitales(signo.getAlturaSiv(), signo.getFrecuenciaCardiacaSiv(),signo.getPesoSiv(),
					signo.getPresionArterialSiv(),signo.getTemperaturaSiv(),px) == true) {
				return "redirect:/signosVitales/";
			}
			return "nuevoSignosVitales";
		}
		
		@RequestMapping(value={"/editarSig-{idSignosVitales}"}, method=RequestMethod.GET)
		public String recuperarSignosVitales(ModelMap model, @PathVariable("idSignosVitales") int idSignosVitales) {
		SignosVitales sig1= signoV.buscarSigno(idSignosVitales);
		model.addAttribute("sigUnico", sig1);
		model.addAttribute("listaCon", con.listaConsulta());
			return "editarSig";
		}
		
		@RequestMapping(value={"/editarSig-{idSignosVitales}"}, method=RequestMethod.POST)
		public String editarConsultaMedica(@Valid @ModelAttribute("sigUnico")  SignosVitales sigUnico, BindingResult result, ModelMap model) {
			if(result.hasErrors()) {
				model.addAttribute("sigUnico", new SignosVitales());
				return "editarSig";
			}if((signoV.udpSignoVitales(sigUnico.getIdSignosVitales(), sigUnico.getAlturaSiv(),sigUnico.getFrecuenciaCardiacaSiv(),sigUnico.getPesoSiv(), 
					sigUnico.getPresionArterialSiv(), sigUnico.getTemperaturaSiv(), sigUnico.getIdConsulta().getIdConsulta()))== true) {
				return "signosSuccess";
			}
			return "editarSig";
		}
		
}
