package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Medicamento;
import org.ues.edu.bad115.servicios.BitacoraProService;
import org.ues.edu.bad115.servicios.CatalogoMedicamentoService;
import org.ues.edu.bad115.servicios.CostoService;
import org.ues.edu.bad115.servicios.MedicamentoService;
import org.ues.edu.bad115.servicios.TratamientoMedicoService;

@Controller
@RequestMapping("/medicamento")
public class MedicamentoController {
	
	@Autowired
	private MedicamentoService med;
	
	@Autowired
	private CatalogoMedicamentoService came;
	
	@Autowired
	private CostoService cos;

	@Autowired
	private TratamientoMedicoService tra;
	
	@RequestMapping(value={"/","/listaMedicamento"}, method=RequestMethod.GET)
	public String mostrarMedicamento(ModelMap model) {
		model.addAttribute("lista", med.listaMedicamento());
		return "listaMedicamento";
	}
	
	@RequestMapping(value={"/nuevoMedicamento/{px}"}, method=RequestMethod.GET)
	public String addNewMedicamento(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("medicamento", new Medicamento());
		model.addAttribute("listaCMed", came.listaCatalogoMedicamento());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevoMedicamento";
	}
	
	@RequestMapping(value={"/nuevoMedicamento/{px}"}, method=RequestMethod.POST)
	public String guardarMedicamento(@Valid @ModelAttribute("medicamento") Medicamento medi, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors() ) {
			//if(result.hasFieldErrors("idCosto") && result.hasFieldErrors("docisMed") ) {
			model.addAttribute("medicamento", new Medicamento());
			return "nuevoMedicamento";
		  
		}
		System.out.println(medi.getDocisMed());
		if(med.addMedicamento(medi.getDocisMed(), medi.getIdCatMedi().getIdCatMedi(), px) == true) {
			return "successMedicamento";
		}
		return "nuevoMedicamento";
	}
	
	@RequestMapping(value={"/editarMed-{idMedicamento}"}, method=RequestMethod.GET)
	public String recuperarMedicamento(ModelMap model, @PathVariable("idMedicamento") int idMedicamento) {
		Medicamento med1 = med.buscarMedicamento(idMedicamento);
		model.addAttribute("medUnico", med1);
		model.addAttribute("listaCMed", came.listaCatalogoMedicamento());
		model.addAttribute("listaCos", cos.listaCosto());
		return "editarMed";
	}
	
	@RequestMapping(value={"/editarMed-{idMedicamento}"}, method=RequestMethod.POST)
	public String editarMedicamento(@Valid @ModelAttribute("medUnico")  Medicamento medUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("medUnico", new Medicamento());
			return "editarMed";
		}if(med.udpMedicamento(medUnico.getIdMedicamento(), medUnico.getDocisMed(), 
				medUnico.getIdCatMedi().getIdCatMedi(), medUnico.getIdTratamiento().getIdTratamiento())== true) {
			return "successMedicamento";
		}
		return "editarMed";
	}

}
