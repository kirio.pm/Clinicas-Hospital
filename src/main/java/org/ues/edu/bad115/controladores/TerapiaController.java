package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Medicamento;
import org.ues.edu.bad115.modelos.Terapia;
import org.ues.edu.bad115.servicios.BitacoraProService;
import org.ues.edu.bad115.servicios.CatalogoTerapiaService;
import org.ues.edu.bad115.servicios.CostoService;
import org.ues.edu.bad115.servicios.TerapiaService;
import org.ues.edu.bad115.servicios.TratamientoMedicoService;

@Controller
@RequestMapping("/terapia")
public class TerapiaController {
	
	@Autowired
	private TerapiaService ter;
	
	@Autowired
	private CatalogoTerapiaService ct;
	
	@Autowired
	private CostoService cos;
	

	@Autowired
	private TratamientoMedicoService tra;
	
	@RequestMapping(value={"/","/listaTerapia"}, method=RequestMethod.GET)
	public String mostrarMedicamento(ModelMap model) {
		model.addAttribute("lista", ter.listaTerapia());
		return "listaTerapia";
	}
	
	@RequestMapping(value={"/nuevaTerapia/{px}"}, method=RequestMethod.GET)
	public String addNewMedicamento(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("terapia", new Terapia());
		model.addAttribute("listaCTer", ct.listaTerapia());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevaTerapia";
	}
	
	@RequestMapping(value={"/nuevaTerapia/{px}"}, method=RequestMethod.POST)
	public String guardarMedicamento(@Valid @ModelAttribute("teraoia") Terapia tera, BindingResult result, ModelMap model,@PathVariable("px") int px) {
		if(result.hasErrors() ) {
			model.addAttribute("terapia", new Terapia());
			return "nuevaTerapia";
		}if(ter.addTerapia(tera.getFechaDeTera(),  tera.getIdCatTera().getIdCatTera(), 
			 px) == true) {
			return "redirect:/terapia/";
		}
		return "nuevaTerapia";
	}
	
	@RequestMapping(value={"/editarTer-{idTerapia}"}, method=RequestMethod.GET)
	public String recuperarMedicamento(ModelMap model, @PathVariable("idTerapia") int idTerapia) {
		Terapia ter1 = ter.buscarTerapia(idTerapia);
		model.addAttribute("terUnico", ter1);
		model.addAttribute("listaCTer", ct.listaTerapia());
		model.addAttribute("listaTra", tra.listaTratamientoMedico());
		return "editarTer";
	}
	
	@RequestMapping(value={"/editarTer-{idTerapia}"}, method=RequestMethod.POST)
	public String editarMedicamento(@Valid @ModelAttribute("terUnico")  Terapia terUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("terUnico", new Terapia());
			return "editarTer";
		}if(ter.udpTerapia(terUnico.getIdTerapia(), terUnico.getFechaDeTera(),  terUnico.getIdCatTera().getIdCatTera(), 
				 terUnico.getIdTratamiento().getIdTratamiento())== true) {
			return "successTerapia";
		}
		return "editarTer";
	}
}
