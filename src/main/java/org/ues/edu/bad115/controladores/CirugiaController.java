package org.ues.edu.bad115.controladores;

import javax.swing.text.html.CSS;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Cirugia;
import org.ues.edu.bad115.servicios.BitacoraProService;
import org.ues.edu.bad115.servicios.CatalogoCirugiaService;
import org.ues.edu.bad115.servicios.CirugiaService;
import org.ues.edu.bad115.servicios.CostoService;

@Controller
@RequestMapping("/cirugia")
public class CirugiaController {

	@Autowired
	private CirugiaService ci;
	
	@Autowired
	private BitacoraProService bp;
	
	@Autowired
	private CatalogoCirugiaService cci;
	
	@Autowired
	private CostoService cos;
	
	@RequestMapping(value={"/","/listaCirugia"}, method=RequestMethod.GET)
	public String mostrarCirugia(ModelMap model) {
		model.addAttribute("lista", ci.listaCirugia());
		return "listaCirugia";
	}
	
	@RequestMapping(value={"/nuevaCirugia"}, method=RequestMethod.GET)
	public String addNewCirugia(ModelMap model) {
		model.addAttribute("cirugia", new Cirugia());
		model.addAttribute("listaBit", bp.listaBitacora());
		model.addAttribute("listaCatCi", cci.listaCirugia());
		return "nuevaCirugia";
	}
	
	@RequestMapping(value={"/nuevaCirugia"}, method=RequestMethod.POST)
	public String guardarCirugia(@Valid @ModelAttribute("cirugia") Cirugia ciru, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("cirugia", new Cirugia());
			return "nuevaCirugia";
		}if(ci.addCirugia(ciru.getFechaCiru(),
				ciru.getHoraCiru(), ciru.getProcesoCiru(), ciru.getIdBitacora().getIdBitacora(), 
				ciru.getIdCatCiru().getIdCatCiru()) == true) {
			return "redirect:/cirugia/";

		}
		return "nuevaCirugia";
	}
	
	@RequestMapping(value={"/editarCir-{idCirugia}"}, method=RequestMethod.GET)
	public String recuperarCirugia(ModelMap model, @PathVariable("idCirugia") int idCirugia) {
		Cirugia cir1 = ci.buscarCirugia(idCirugia);		
		model.addAttribute("cirUnico", cir1);
		model.addAttribute("listaBit", bp.listaBitacora());
		model.addAttribute("listaCatCi", cci.listaCirugia());
		return "editarCir";
	}
	
	@RequestMapping(value={"/editarCir-{idCirugia}"}, method=RequestMethod.POST)
	public String editarCirugia(@Valid @ModelAttribute("cirUnico")  Cirugia cirUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("cirUnico", new Cirugia());
			model.addAttribute("listaBit", bp.listaBitacora());
			return "editarCir";
		}if(ci.udpCirugia(cirUnico.getIdCirugia(), cirUnico.getFechaCiru(), cirUnico.getHoraCiru(), cirUnico.getProcesoCiru(),
				cirUnico.getIdBitacora().getIdBitacora(), cirUnico.getIdCatCiru().getIdCatCiru())== true) {
			return "successCirugia";
		}
		return "editarCir";
	}
}
