package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Hospital;
import org.ues.edu.bad115.modelos.Hospitalizacion;
import org.ues.edu.bad115.servicios.DoctorService;
import org.ues.edu.bad115.servicios.HospitalizacionService;
import org.ues.edu.bad115.servicios.RegistroPacienteService;
import org.ues.edu.bad115.servicios.SalaService;

@Controller
@RequestMapping("/hospitalizacion")
public class HospitalizacionController {
	
	@Autowired
	private HospitalizacionService hospitalizacion;
	
	@Autowired
	private DoctorService doc;
	
	@Autowired
	private SalaService sal;
	
	@Autowired
	private RegistroPacienteService pac;
	
	@RequestMapping(value={"/","/listaHospitalizacion"}, method=RequestMethod.GET)
	public String mostrarHospitalizacion(ModelMap model) {
		model.addAttribute("lista", hospitalizacion.listaHospitalizacion());
		//model.addAttribute("listaReg", reg.listaRegion());
		return "listaHospitalizacion";
	}
	
	@RequestMapping(value={"/nuevaHospitalizacion/{px}"}, method=RequestMethod.GET)
	public String addNewHospitalizacion(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("hospitalizacion", new Hospitalizacion());
		model.addAttribute("listaDoc", doc.listaDoctor());
		model.addAttribute("listaSal", sal.listaSala());
		model.addAttribute("listaPac", pac.listaPaciente());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevaHospitalizacion";
	}
	
	@RequestMapping(value={"/nuevaHospitalizacion/{px}"}, method=RequestMethod.POST)
	public String guardarHospitalizacion(@Valid @ModelAttribute("hospitalizacion") Hospitalizacion hosp, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors()) {
			model.addAttribute("hospitalizacion", new Hospitalizacion());
			return "nuevaHospitalizacion";
		}if(hospitalizacion.addHospitalizacion(hosp.getFechaInicioHosp(), 
				hosp.getFechaFinHosp(),
				hosp.getIdDoctor().getIdDoctor(), 
				px, 
				hosp.getIdSala().getIdSala()) == true) {
			return "redirect:/hospitalizacion/";
		}
		return "nuevaHospitalizacion";
	}
	
	@RequestMapping(value={"/editarHcion-{idHospitalizacion}"}, method=RequestMethod.GET)
	public String recuperarHospitalizacion(ModelMap model, @PathVariable("idHospitalizacion") int idHospitalizacion) {
	Hospitalizacion hos1 = hospitalizacion.buscarHospitalizacion(idHospitalizacion);		
	model.addAttribute("hosUnico", hos1);
	model.addAttribute("listaDoc", doc.listaDoctor());
	model.addAttribute("listaSal", sal.listaSala());
	model.addAttribute("listaPac", pac.listaPaciente());
		return "editarHcion";
	}
	
	@RequestMapping(value={"/editarHcion-{idHospitalizacion}"}, method=RequestMethod.POST)
	public String editarHospitalizacion(@Valid @ModelAttribute("hosUnico")  Hospitalizacion hosUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("hosUnico", new Hospital());
			model.addAttribute("listaDoc", doc.listaDoctor());
			model.addAttribute("listaSal", sal.listaSala());
			model.addAttribute("listaPac", pac.listaPaciente());
			return "editarHcion";
		}if(hospitalizacion.udpHospitalizacion(hosUnico.getIdHospitalizacion(),hosUnico.getFechaInicioHosp(), hosUnico.getFechaFinHosp(), hosUnico.getIdDoctor().getIdDoctor(), 
				hosUnico.getIdPaciente().getIdPaciente(), hosUnico.getIdSala().getIdSala())== true) {
			return "successHospitalizacion";
		}
		return "editarHcion";
	}
}
