package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Sala;
import org.ues.edu.bad115.servicios.HospitalService;
import org.ues.edu.bad115.servicios.SalaService;

@Controller
@RequestMapping("/sala")
public class SalaController {
	@Autowired
	private SalaService ss;
	
	@Autowired
	private HospitalService ho;

	@RequestMapping(value={"/","/listaSala"}, method=RequestMethod.GET)
	public String mostrarSala(ModelMap model) {
		model.addAttribute("lista", ss.listaSala());
		return "listaSala";
	}
	
	@RequestMapping(value={"/nuevaSala"}, method=RequestMethod.GET)
	public String addNewSala(ModelMap model) {
		model.addAttribute("sala", new Sala());
		model.addAttribute("listaHos", ho.listaHospital());
		return "nuevaSala";
	}
	
	@RequestMapping(value={"/nuevaSala"}, method=RequestMethod.POST)
	public String guardarSala(@Valid @ModelAttribute("sala") Sala sal, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("sala", new Sala());
			model.addAttribute("listaHos", ho.listaHospital());
			return "nuevaSala";
		}if(ss.addSala(sal.getCamaSal(),sal.getHabitacionSal(),sal.getIdHospital().getIdHospital())== true) {
			return "redirect:/sala/";
		}
		return "nuevaSala";
	}
	
	@RequestMapping(value={"/editarSal-{idSala}"}, method=RequestMethod.GET)
	public String recuperarSala(ModelMap model, @PathVariable("idSala") int idSala) {
	Sala sal1=ss.buscarSala(idSala);		
	model.addAttribute("salUnico", sal1);
	model.addAttribute("listaHos", ho.listaHospital());
		return "editarSal";
	}
	
	@RequestMapping(value={"/editarSal-{idSala}"}, method=RequestMethod.POST)
	public String editarSala(@Valid @ModelAttribute("salUnico")  Sala salUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("sala", new Sala());
			model.addAttribute("listaHos", ho.listaHospital());
			return "editarSal";
		}if(ss.udpSala(salUnico.getIdSala(), salUnico.getCamaSal(), salUnico.getHabitacionSal(),
				salUnico.getIdHospital().getIdHospital())== true) {
			return "successSala";
		}
		return "editarSal";
	}
}
