package org.ues.edu.bad115.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.HistorialClinico;
import org.ues.edu.bad115.modelos.Paciente;
import org.ues.edu.bad115.servicios.HistorialClinicoService;
import org.ues.edu.bad115.servicios.RegistroDatosFamiliaresService;
import org.ues.edu.bad115.servicios.RegistroPacienteService;



@Controller
@RequestMapping("/hclinico")
public class HistorialClinicoController {
//@Autowired
//private HistorialClinicoService service;
@Autowired 
private RegistroPacienteService registroPaciente;

@Autowired
private RegistroDatosFamiliaresService registroDatosFamiliares;

@Autowired
private HistorialClinicoService Hclinico;

@Autowired
private RegistroPacienteService pacienteService;


@RequestMapping(value={"/","/listaHistorial"},method=RequestMethod.GET)
public String mostrarPacientes(ModelMap model) {
model.addAttribute("historiales", Hclinico.listaHistorialClinico());
return "ListaHistorial";
}

@RequestMapping(value={"/nuevohclinico/{px}"}, method=RequestMethod.GET)
public String guardarHistorialClinico(ModelMap model, @PathVariable("px") int id_paciente) {
	model.addAttribute("historial",new HistorialClinico());
	model.addAttribute("id_paciente",id_paciente);
	model.addAttribute("lista_datos_familiares",registroDatosFamiliares.listaDatosFamiliares());
	
	return "newHistorialClinico";
}

@RequestMapping(value={"/nuevohclinico/{px}"},method=RequestMethod.POST)
public String guardarHistorialClinico(@ModelAttribute("historial") HistorialClinico historial, BindingResult result, ModelMap model,@PathVariable("px") int px) {
	if(result.hasErrors()) {
		model.addAttribute("historial",new HistorialClinico());
        return "redirect:/hclinico/";
    }
	System.out.println();
	
	if(Hclinico.guardarHistorialDOSClinico(historial.getEnfermedadesPadecidas(), 
			historial.getTratamientoPreventivo(),
			px,
			historial.getIdDatosFamiliares().getApellidoPadre(),
			historial.getIdDatosFamiliares().getDireccion(),
			historial.getIdDatosFamiliares().getFechaNacimiento(),
			historial.getIdDatosFamiliares().getHistorialDeEnfermedades(),
			historial.getIdDatosFamiliares().getNombrePadre())==true) {
					 
			return "redirect:/paciente/";
			}
	
	return "newHistorialClinico1";
	
	
}



}
