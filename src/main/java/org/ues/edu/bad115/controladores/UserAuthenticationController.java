package org.ues.edu.bad115.controladores;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserAuthenticationController {
	
	private static final String HOME_VIEW_REDIRECT = "redirect:/home";
	
	private static final String LOGIN_VIEW = "login";
	
	private static final String LOGOUT_REDIRECT_PATH = "redirect:/login?logout";
	
	private static final String ACCESS_DENIED_PATH = "accessDenied";

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	@GetMapping("/login")
	public String showLoginPage() {
		if (isCurrentAuthenticationAnonymous()) {
			return LOGIN_VIEW;
		} else {
			return HOME_VIEW_REDIRECT;
		}
	}

	@GetMapping("/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			persistentTokenBasedRememberMeServices.logout(request, response, authentication);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return LOGOUT_REDIRECT_PATH;
	}

	@GetMapping("/Access_Denied")
	public String showAccessDeniedPage(ModelMap model, Principal principal) {
		model.addAttribute("loggedinuser", principal.getName());
		return ACCESS_DENIED_PATH;
	}

	private boolean isCurrentAuthenticationAnonymous() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authenticationTrustResolver.isAnonymous(authentication);
	}
}