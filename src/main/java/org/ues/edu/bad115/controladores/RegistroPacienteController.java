package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.ConsultaMedica;
import org.ues.edu.bad115.modelos.Paciente;
import org.ues.edu.bad115.servicios.RegistroPacienteService;

@Controller
@RequestMapping("/paciente")
public class RegistroPacienteController {
	@Autowired
	private RegistroPacienteService pacienteService;
	
	@RequestMapping(value={"/","/listaPacientes"},method=RequestMethod.GET)
	public String mostrarPacientes(ModelMap model) {
	model.addAttribute("pacientes", pacienteService.listaPaciente());
	return "ListaPacientes";
    }
	
	@RequestMapping(value= {"/nuevoPaciente"}, method=RequestMethod.GET)
	public String nuevoPaciente(ModelMap model) {
		model.addAttribute("paciente", new Paciente());
		return "nuevoPaciente";
	}
	@RequestMapping(value= {"/nuevoPaciente"}, method=RequestMethod.POST)
	public String ingresarPaciente(@Valid @ModelAttribute("paciente") Paciente paciente, BindingResult result, ModelMap model) {	
		if(result.hasErrors()) {
			model.addAttribute("paciente", new Paciente());
		}
		if(pacienteService.guardarPaciente(paciente.getApellidoDeCasadoPac(),
		      paciente.getApellidoPac(),paciente.getDireccionPac(),
				paciente.getEmailPac(),	paciente.getEstadoCivilPac(), 
				paciente.getFechaNacimientoPac(),paciente.getGeneroPac(),
				paciente.getNombrePac(),paciente.getNumeroIdentificacion(),
				paciente.getResponsablePorEmergenciaPac(),paciente.getTelefonoPac())==true) {
		
			return "redirect:/paciente/";
		}
		return "nuevoPaciente";
	}
	
	@RequestMapping(value={"/editarPx-{idPaciente}"}, method=RequestMethod.GET)
	public String recuperarPaciente(ModelMap model, @PathVariable("idPaciente") int idPaciente) {
	Paciente px1= pacienteService.buscarPaciente(idPaciente);
	model.addAttribute("pxUnico", px1);
	return "editarPx";
	}
	
	@RequestMapping(value={"/editarPx-{idPaciente}"}, method=RequestMethod.POST)
	public String editarPaciente(@Valid @ModelAttribute("pxUnico")  Paciente pxUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("pxUnico", new Paciente());
			return "editarCon";
		}if(pacienteService.actualizarPaciente(pxUnico.getIdPaciente(), pxUnico.getApellidoDeCasadoPac(), pxUnico.getApellidoPac(), pxUnico.getDireccionPac(), pxUnico.getEmailPac(), pxUnico.getEstadoCivilPac(),
				pxUnico.getFechaNacimientoPac(), pxUnico.getGeneroPac(), pxUnico.getNombrePac(), pxUnico.getNumeroIdentificacion(), pxUnico.getResponsablePorEmergenciaPac(), pxUnico.getTelefonoPac())== true) {
			return "PacienteSuccess";
		}
		return "editarPx";
	}

	
}
