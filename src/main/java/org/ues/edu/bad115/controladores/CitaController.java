package org.ues.edu.bad115.controladores;



import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.CitaMedica;
import org.ues.edu.bad115.modelos.Doctor;
import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.servicios.CatalogoCitasService;
import org.ues.edu.bad115.servicios.CitaService;
import org.ues.edu.bad115.servicios.DoctorService;
import org.ues.edu.bad115.servicios.RegistroPacienteService;


@Controller
@RequestMapping("/cita")
public class CitaController {

	@Autowired
	private CitaService citaServices;
	
	@Autowired
	private DoctorService doctor;
	
	@Autowired
	private CatalogoCitasService CatServices;
	@Autowired
	private RegistroPacienteService pacienteService;
	
	@RequestMapping(value={"/","/ListaCitas"},method=RequestMethod.GET)
	public String mostrarExpediente(ModelMap model) {
	model.addAttribute("Lista", citaServices.listaCitas());
	return "ListaCitas";
    }
	
	
	
	@RequestMapping(value={"/nuevaCita/{px}"}, method=RequestMethod.GET)
	public String addNuevoDoctor(ModelMap model, @PathVariable("px") int id_paciente) {
		model.addAttribute("cita", new CitaMedica());
		model.addAttribute("listaDoc", doctor.listaDoctor());
		model.addAttribute("ListaConsultas",CatServices.listaCatalogoCitas());
		model.addAttribute("id_paciente",id_paciente);
		return "nuevaCita";
	}
	
	@RequestMapping(value={"/nuevaCita/{px}"}, method=RequestMethod.POST)
	public String guardarCita(@Valid @ModelAttribute("cita") CitaMedica cita, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors()) {
			model.addAttribute("cita", new CitaMedica());
			return "nuevaCita";
		}
		if(citaServices.addCita(cita.getFechaCita(),
		cita.getHoraCita(),
		cita.getMedioRegistro(),
		cita.getIdCatalogoCita().getIdCatalogoCita(),
		cita.getIdDoctor().getIdDoctor(), 
		px)) {
			return "redirect:/cita/";
		}	
		return "sucessCita";
	}
	
	
	
	@RequestMapping(value={"/EditarCita/{px}"}, method=RequestMethod.GET)
	public String addNuevoCita(ModelMap model, @PathVariable("px") int idCita) {
		CitaMedica citax = citaServices.buscarCita(idCita);
		model.addAttribute("CitaUnica", citax);
		model.addAttribute("listaDoc", doctor.listaDoctor());
		model.addAttribute("ListaConsultas",CatServices.listaCatalogoCitas());
		return "EditarCita";
	}
	
	@RequestMapping(value={"/EditarCita/{px}"}, method=RequestMethod.POST)
	public String editarCita(@Valid @ModelAttribute("CitaUnica") CitaMedica cita, BindingResult result, ModelMap model,@PathVariable("px") int px ) {
		if(result.hasErrors()) {
			model.addAttribute("cita", new CitaMedica());
			return "EditarCita";
		}
		if(citaServices.EditarCita(px,
				cita.getFechaCita(),
				cita.getHoraCita(),
				cita.getMedioRegistro(),
				cita.getIdDoctor().getIdDoctor(), 
				cita.getIdCatalogoCita().getIdCatalogoCita(),
				cita.getIdPaciente().getIdPaciente())) {
					return "success_cita";
				}	
		return "EditarCita";
	}
}
