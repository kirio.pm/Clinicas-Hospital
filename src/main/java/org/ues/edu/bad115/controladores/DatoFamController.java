package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.DatosFamiliares;
import org.ues.edu.bad115.modelos.TratamientoMedico;
import org.ues.edu.bad115.servicios.RegistroDatosFamiliaresService;

@Controller
@RequestMapping("/DatosFamilia")
public class DatoFamController {
	
	@Autowired 
	private RegistroDatosFamiliaresService DFdao;
	
	@RequestMapping(value={"/","/listaFamilia"}, method=RequestMethod.GET)
	public String mostrarFamilia(ModelMap model) {
		model.addAttribute("lista", DFdao.listaDatosFamiliares());
		return "listaFamilia";
	}
	
	@RequestMapping(value={"/nuevoFamilia"}, method=RequestMethod.GET)
	public String addNewFamilia(ModelMap model) {
		model.addAttribute("familia", new DatosFamiliares());
		/*model.addAttribute("listaDiag", diag.listaDiagnostico());*/
		return "nuevoFamilia";
	}
	
	@RequestMapping(value={"/nuevoFamilia"}, method=RequestMethod.POST)
	public String guardarTratamiento(@Valid @ModelAttribute("tratamiento") DatosFamiliares familia, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("familia", new DatosFamiliares());
			return "nuevoFamilia";
		}if(DFdao.guardarDatosFamiliares(familia.getApellidoPadre(),
				familia.getDireccion(), familia.getFechaNacimiento(),
				familia.getHistorialDeEnfermedades(), familia.getNombrePadre()) == true) {
			return "successFamilia";
		}
		return "nuevoFamilia";
	}
	
	
}
