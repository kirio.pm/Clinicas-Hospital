package org.ues.edu.bad115.controladores;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.servicios.EmpleadoService;

@Controller
@RequestMapping("/empleados")
public class EmpleadoController {
	
	@Autowired
	private EmpleadoService emp;
	
	@RequestMapping(value={"/","/listaEmpleados"},method=RequestMethod.GET)
	public String mostrarEmpleados(ModelMap model) {
	model.addAttribute("lista", emp.listaEmpleados());
	return "listaEmpleados";
    }
	
	@RequestMapping(value={"/nuevoEmpleados"}, method=RequestMethod.GET)
	public String addNewEmpleado(ModelMap model) {
		model.addAttribute("empleado", new Empleado());
		return "nuevoEmpleados";
	}
	
	@RequestMapping(value={"/nuevoEmpleados"}, method=RequestMethod.POST)
	public String guardarNuevoEmpleado(@Valid @ModelAttribute("empleado")  Empleado empleado, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("empleado", new Empleado());
			return "nuevoEmpleados";
		}if((emp.addEmpleado(empleado.getApellidoEmpleado(),empleado.getDuiEmp(),empleado.getEmilEmp(),empleado.getNombreEmpleado(),empleado.getTelefonoEmp())) == true) {
			return "redirect:/empleados/";
		}
		return "nuevoEmpleados";
	}
	
	@RequestMapping(value={"/editarEmp-{idEmpleado}"}, method=RequestMethod.GET)
	public String recuperarEmpleado(ModelMap model, @PathVariable("idEmpleado") int idEmpleado) {
	Empleado  emp1= emp.buscarEmpleado(idEmpleado);
	model.addAttribute("empUnico", emp1);
		return "editarEmp";
	}
	
	@RequestMapping(value={"/editarEmp-{idEmpleado}"}, method=RequestMethod.POST)
	public String editarEmpleado(@Valid @ModelAttribute("empUnico")  Empleado empUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("empUnico", new Empleado());
			return "editarEmp";
		}if((emp.updEmpleado(empUnico.getIdEmpleado(),empUnico.getApellidoEmpleado(), empUnico.getDuiEmp(),empUnico.getEmilEmp(),empUnico.getNombreEmpleado(),empUnico.getTelefonoEmp()))== true) {
			return "successEmpleados";
		}
		return "editarEmp";
	}
	
	
}
