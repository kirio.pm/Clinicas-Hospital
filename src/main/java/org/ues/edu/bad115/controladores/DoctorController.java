package org.ues.edu.bad115.controladores;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.Doctor;
import org.ues.edu.bad115.servicios.DoctorService;
import org.ues.edu.bad115.servicios.EmpleadoService;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

@Controller
@RequestMapping("/doctor")
public class DoctorController {
	
	@Autowired
	private DoctorService doctor;
	
	@Autowired
	private EmpleadoService emp;
	
	@RequestMapping(value={"/","/listaDoctor"}, method=RequestMethod.GET)
	public String mostrarDoctor(ModelMap model) {
		model.addAttribute("lista", doctor.listaDoctor());
		return "listaDoctor";
	}

	@RequestMapping(value={"/nuevoDoctor"}, method=RequestMethod.GET)
	public String addNuevoDoctor(ModelMap model) {
		model.addAttribute("doc", new Doctor());
		model.addAttribute("listaEmp", emp.listaEmpleados());
		return "nuevoDoctor";
	}
	
	@RequestMapping(value={"/nuevoDoctor"}, method=RequestMethod.POST)
	public String guardarNuevoDoctor(@Valid @ModelAttribute("doc") Doctor doc, BindingResult result, ModelMap model ) {
		//System.out.println(doc.getIdEmpleado().getIdEmpleado());
		if(result.hasErrors()) {
			model.addAttribute("doc", new Doctor());
			return "nuevoDoctor";
		}
		if(doctor.addDoctor(doc.getEspecialidadDoc(), doc.getIdEmpleado().getIdEmpleado())==true) {
			return "redirect:/doctor/";
		}
		return "nuevoDoctor";
	}
	
	@RequestMapping(value={"/editarDoc-{idDoctor}"}, method=RequestMethod.GET)
	public String recuperarDoctor(ModelMap model, @PathVariable("idDoctor") int idDoctor) {
	Doctor doc1= doctor.buscarDoctor(idDoctor);
	model.addAttribute("docUnico", doc1);
	model.addAttribute("listaEmp", emp.listaEmpleados());
		return "editarDoc";
	}
	
	@RequestMapping(value={"/editarDoc-{idDoctor}"}, method=RequestMethod.POST)
	public String editarDoctor(@Valid @ModelAttribute("docUnico")  Doctor docUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("docUnico", new Doctor());
			return "editarDoc";
		}if((doctor.udpDoctor(docUnico.getIdDoctor(),docUnico.getEspecialidadDoc(),docUnico.getIdEmpleado().getIdEmpleado()))== true) {
			return "successDoctor";
		}
		return "editarDoc";
	}
}
