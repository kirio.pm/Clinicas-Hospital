package org.ues.edu.bad115.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.ues.edu.bad115.modelos.ConsultaMedica;
import org.ues.edu.bad115.servicios.CatalogoConsultaService;
import org.ues.edu.bad115.servicios.ConsultaMedicaService;
import org.ues.edu.bad115.servicios.DoctorService;
import org.ues.edu.bad115.servicios.ExpedienteClinicoService;


@Controller
@RequestMapping("/consultaMedica")
public class ConsultaMedicaController {
	
	@Autowired
	private ConsultaMedicaService consulta;
	
	@Autowired
	private DoctorService doctor;
	
	@Autowired
	private ExpedienteClinicoService expCli;
	
	@Autowired
	private CatalogoConsultaService catCon;
	
	
	@RequestMapping(value={"/","/listaConsulta"}, method=RequestMethod.GET)
	public String mostrarDoctor(ModelMap model) {
		model.addAttribute("listaCon", consulta.listaConsulta());
		return "listaConsulta";
	}
	
	@RequestMapping(value={"/nuevaConsulta/{px}"}, method=RequestMethod.GET)
	public String addNewConsultaMedica(ModelMap model, @PathVariable("px") int id_ex) {
		model.addAttribute("consulta", new ConsultaMedica());
		model.addAttribute("listaDoc", doctor.listaDoctor());
		model.addAttribute("listaCat", catCon.listaCatalogo());
		model.addAttribute("id_ex",id_ex);
		return "nuevaConsulta";
	}
	
	@RequestMapping(value={"/nuevaConsulta/{px}"}, method=RequestMethod.POST)
	public String guardarConsultaMedica(@Valid @ModelAttribute("consulta") ConsultaMedica con, BindingResult result, ModelMap model,@PathVariable("px") int px) {
		if(result.hasErrors()) {
			model.addAttribute("consulta", new ConsultaMedica());
			return "nuevaConsultaMedica";
		}if(consulta.addConsultaMedica(con.getIdCatCon().getIdCatCon(),
				con.getIdDoctor().getIdDoctor(),
				con.getDescripcionCon(),
				 px)) {
			return "redirect:/consultaMedica/";
		}
		return "nuevaConsulta";
	}
	
	@RequestMapping(value={"/editarCon-{idConsulta}"}, method=RequestMethod.GET)
	public String recuperarConsultaMedica(ModelMap model, @PathVariable("idConsulta") int idConsulta) {
	ConsultaMedica con1= consulta.buscarConsulta(idConsulta);
	model.addAttribute("conUnico", con1);
	model.addAttribute("listaDoc", doctor.listaDoctor());
		return "editarCon";
	}
	
	@RequestMapping(value={"/editarCon-{idConsulta}"}, method=RequestMethod.POST)
	public String editarConsultaMedica(@Valid @ModelAttribute("conUnico")  ConsultaMedica conUnico, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			model.addAttribute("conUnico", new ConsultaMedica());
			return "editarCon";
		}if((consulta.udpConsultaMedica(conUnico.getIdConsulta(),conUnico.getDescripcionCon(),conUnico.getIdDoctor().getIdDoctor()))== true) {
			return "successConsulta";
		}
		return "editarCon";
	}
	
}
