package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoMedicamento;

public interface CatalogoMedicamentoDao {
	public List<CatalogoMedicamento> listaCatalogoMedicamento();
}
