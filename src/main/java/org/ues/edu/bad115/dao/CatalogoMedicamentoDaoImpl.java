package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoMedicamento;

@Repository
public class CatalogoMedicamentoDaoImpl implements CatalogoMedicamentoDao {
	
	@Autowired
	public SessionFactory sessionFactory;

	@Override
	public List<CatalogoMedicamento> listaCatalogoMedicamento() {
		
		return sessionFactory.getCurrentSession().createQuery("from CatalogoMedicamento cm "
				, CatalogoMedicamento.class).getResultList();
	}
}
