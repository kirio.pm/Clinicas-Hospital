package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.ConsultaMedica;

public interface ConsultaMedicaDao {
	public boolean addConsultaMedica(int idCatCon, int idDoctor, String descripcion, int idExpediente);
	public List<ConsultaMedica> listaConsulta();
	public boolean udpConsultaMedica(int idConsulta, String descripcion, int idDoctor);
	public ConsultaMedica buscarConsulta(int idConsulta);
	public void actualizarConsulta(ConsultaMedica consulta);
	public ConsultaMedica buscarConsultaMedica(int id_consulta);
}
