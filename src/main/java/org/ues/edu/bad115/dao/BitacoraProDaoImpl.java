package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.BitacoraProcedimientos;

@Repository
public class BitacoraProDaoImpl implements BitacoraProDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<BitacoraProcedimientos> listaBitacoraPro() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from BitacoraProcedimientos bi"
				+ " join fetch bi.idHospitalizacion ih "
				+ " join fetch ih.idPaciente ", BitacoraProcedimientos.class).getResultList();
	}
	
}
