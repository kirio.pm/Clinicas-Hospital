package org.ues.edu.bad115.dao;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;

import org.ues.edu.bad115.modelos.PersistentLogin;

@Repository("tokenRepositoryDao")
@Transactional
public class HibernateTokenRepositoryImpl extends AbstractDao<String, PersistentLogin> implements PersistentTokenRepository{
	
	static final Logger logger = LoggerFactory.getLogger(HibernateTokenRepositoryImpl.class);

	@Override
	public void createNewToken(PersistentRememberMeToken token) {
		logger.info("Creating token for user: {}",token.getUsername());
		PersistentLogin persistentLogin = new PersistentLogin();
		persistentLogin.setUsername(token.getUsername());
		persistentLogin.setSeries(token.getSeries());
		persistentLogin.setToken(token.getTokenValue());
		persistentLogin.setLast_used(Date.from(Instant.now()));
		persist(persistentLogin);
	}
	@Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
		logger.info("Fetch token if any for seriesId: {}",seriesId);
		try{
			PersistentLogin persistentLogin = selectEntityWhere("series",seriesId);
			return new PersistentRememberMeToken(persistentLogin.getUsername(),persistentLogin.getSeries(),
					persistentLogin.getToken(),Date.from(Instant.now()));
		}catch(Exception e){
			logger.info("Token no found...");
			return null;
		}
	}
	@Override
	public void removeUserTokens(String username) {
		logger.info("Removing token if any for user: {}",username);
		PersistentLogin persistentLogin = selectEntityWhere("username",username);
		if(persistentLogin !=null){
			logger.info("Remenber me was selected");
			delete(persistentLogin);
		}
	}
	@Override
	public void updateToken(String seriesId, String tokenValue, Date lastUsed) {
		logger.info("Updating token for seriesId: {}",seriesId);
		PersistentLogin persistentLogin = getByKey(seriesId);
		persistentLogin.setToken(tokenValue);
		persistentLogin.setLast_used(Date.from(Instant.now()));
		update(persistentLogin);
	}
}
