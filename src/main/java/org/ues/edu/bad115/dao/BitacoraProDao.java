package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.BitacoraProcedimientos;

public interface BitacoraProDao {
	
	public List<BitacoraProcedimientos> listaBitacoraPro();
}
