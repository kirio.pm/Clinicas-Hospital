package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoDiagnostico;

@Repository
public class CatalogoDiagnosticoDaoImpl implements CatalogoDiagnosticoDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CatalogoDiagnostico> listaCataDiag() {
		return sessionFactory.getCurrentSession().createQuery("from CatalogoDiagnostico", 
				CatalogoDiagnostico.class).getResultList();
	}
	
	
}
