package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import org.ues.edu.bad115.modelos.Profile;

@Repository("profileDao")
public class ProfileDaoImpl extends AbstractDao<Integer, Profile> implements ProfileDao{

	@Override
	public Profile findById(int id) {
		return getByKey(id);
	}

	@Override
	public List<Profile> findAll() {
		return selectAll();
	}

	@Override
	public Profile findByType(String type) {
		return selectEntityWhere("type", type);
	}

	@Override
	public Profile findInitializeProfileById(int id) {
		try {
		return getSession().createQuery("select p "
				+ "from Profile p "
				+ "join fetch p.users u "
				+ "where p.id = :id", Profile.class)
				.setParameter("id", id)
				.getSingleResult();
		}catch(NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	

}

