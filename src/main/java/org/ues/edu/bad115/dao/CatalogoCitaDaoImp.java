package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoCitas;


@Repository
public class CatalogoCitaDaoImp  implements CatalogoCitaDao{

	
		
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public List<CatalogoCitas> listaCatalogoCita() {
		return sessionFactory.getCurrentSession().createQuery("from CatalogoCitas", CatalogoCitas.class).getResultList();
	}

}
