package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoDiagnostico;

public interface CatalogoDiagnosticoDao {
	public List<CatalogoDiagnostico> listaCataDiag();
}
