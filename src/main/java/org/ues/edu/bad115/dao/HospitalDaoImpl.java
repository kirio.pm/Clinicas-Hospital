package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Hospital;

@Repository
public class HospitalDaoImpl implements HospitalDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addHospital(String direccionHos, String nombreHos, String telefonoHos, int idRegion) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_HOS_PROCE")
					.registerStoredProcedureParameter(0 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, direccionHos)
            				.setParameter(1, nombreHos)
                            .setParameter(2, telefonoHos)
                            .setParameter(3, idRegion);             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Hospital> listaHospital() {
		
		return sessionFactory.getCurrentSession().createQuery("from Hospital ho "
				+ " join fetch ho.idRegion re", Hospital.class).getResultList();
	}

	@Override
	public boolean udpHospital(int idHospital, String direccionHos, String nombreHos, String telefonoHos,
			int idRegion) {
			try {
				StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_HOS_PROCE")
						.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
						.registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
						.registerStoredProcedureParameter(2 , String.class , ParameterMode.IN)
	                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
	                    .registerStoredProcedureParameter(4 , Integer.class, ParameterMode.IN);                    
	            storedProcedure.setParameter(0, idHospital)
	            				.setParameter(1, direccionHos)
	            				.setParameter(2, nombreHos)
	                            .setParameter(3, telefonoHos)
	                            .setParameter(4, idRegion);          
	            storedProcedure.execute();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		return false;
	}

	@Override
	public Hospital buscarHospital(int idHospital) {
		
		return sessionFactory.getCurrentSession().createQuery("from Hospital ho "
				+ "join fetch ho.idRegion re "
				+ "where ho.idHospital=:idHospital ", Hospital.class).setParameter("idHospital", idHospital).getSingleResult();
	}

}
