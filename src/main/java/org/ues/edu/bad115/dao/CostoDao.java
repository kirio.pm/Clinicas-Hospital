package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;
import org.ues.edu.bad115.modelos.CostosPorServicios;

public interface CostoDao {
	public boolean addCosto(LocalDate fechaCosto, String unidadMonetaria);
	public List<CostosPorServicios> listaCosto();
	public boolean udpCosto(int idCosto, LocalDate fechaCosto, String unidadMonetaria);
	public CostosPorServicios buscarCosto(int idCosto);
}
