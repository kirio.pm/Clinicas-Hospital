package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.DatosFamiliares;
import org.ues.edu.bad115.modelos.Empleado;
 
@Repository
public class RegistroDatosFamiliaresDaoImpl implements RegistroDatosFamiliaresDao {
	
	@Autowired 
	private SessionFactory sessionFactory;	
	@Override
	public boolean guardarDatosFamiliares(String apellidoPadre, 
			String direccion, 
			LocalDate fechaNacimiento,
			String historialDeEnfermedades, 
			String nombrePadre) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_DATA_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , LocalDate.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class, ParameterMode.IN);
             storedProcedure.setParameter(0, apellidoPadre)
                            .setParameter(1, direccion)
                            .setParameter(2, fechaNacimiento)
                            .setParameter(3, historialDeEnfermedades)
                            .setParameter(4, nombrePadre);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return true;
	}

	@Override
	public void actualizarDatosFamiliares(DatosFamiliares datosFamiliares) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DatosFamiliares BuscarDatosFamiliares(int idDatosFamiliares) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DatosFamiliares> listaDatosFamiliares() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from DatosFamiliares", DatosFamiliares.class).getResultList();
	}
	
	 
	 
}
