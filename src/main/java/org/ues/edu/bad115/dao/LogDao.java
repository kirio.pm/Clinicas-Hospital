package org.ues.edu.bad115.dao;

import java.util.List;


import org.ues.edu.bad115.modelos.Log;

public interface LogDao {
	public List<Log> listaLog();
}
