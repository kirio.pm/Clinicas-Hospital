package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.SeguimientoCirugia;

@Repository
public class SeguimientoDaoImpl implements SeguimientoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addSeguimiento(String tipoSeguimiento, int idCirugia) {
		// TODO Auto-generated method stub
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_SEG_CIR_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, tipoSeguimiento)
                            .setParameter(1, idCirugia);
             
            storedProcedure.execute();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<SeguimientoCirugia> listaSeguimiento() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from SeguimientoCirugia sc "
				+ " join fetch sc.idCirugia cr "
				+ " join fetch cr.idCatCiru cc"
				+ " join fetch cr.idBitacora bi "
				+ " join fetch bi.idHospitalizacion hos "
				+ " join fetch hos.idPaciente ", SeguimientoCirugia.class).getResultList();
	}

	@Override
	public boolean udpSeguimiento(int idSeguimiento, String tipoSeguimiento, int idCirugia) {
		// TODO Auto-generated method stub
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_SEG_CIR_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, idSeguimiento)
            				.setParameter(1, tipoSeguimiento)
                            .setParameter(2, idCirugia);
            storedProcedure.execute();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public SeguimientoCirugia buscarSeguimiento(int idSeguimiento) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from SeguimientoCirugia sc "
				+ " join fetch sc.idCirugia ic "
				+ " join fetch ic.idCatCiru cc"
				+ " join fetch ic.idBitacora bi "
				+ " join fetch bi.idHospitalizacion hos "
				+ " join fetch hos.idPaciente "
				+ " where sc.idSeguimiento=:idSeguimiento", SeguimientoCirugia.class).setParameter("idSeguimiento", idSeguimiento).getSingleResult();
	}
}
