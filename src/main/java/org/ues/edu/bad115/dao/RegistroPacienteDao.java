package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.ues.edu.bad115.modelos.Paciente;

public interface RegistroPacienteDao {
		public boolean guardarPaciente(
				String apellidoCasadoPac,
				String apellidoPac, 
				String direccionPac, 
				String emailPac,
				String estadoCivilPac,
				LocalDate fechaNacimientoPac,
				String generoPac,
				String nombrePac, 
				String numeroIdentificacion,
				String responsablePorEmergenciaPac,
				String telefonoPac
				);
		public boolean actualizarPaciente(
				 int idPaciente,
				String apellidoCasadoPac,
				String apellidoPac, 
				String direccionPac, 
				String emailPac,
				String estadoCivilPac,
				LocalDate fechaNacimientoPac,
				String generoPac,
				String nombrePac, 
				String numeroIdentificacion,
				String responsablePorEmergenciaPac,
				String telefonoPac
				);
		public Paciente buscarPaciente(int idPaciente);
		public List<Paciente> listaPaciente();
}
