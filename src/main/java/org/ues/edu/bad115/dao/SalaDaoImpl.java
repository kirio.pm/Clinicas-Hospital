package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Sala;

@Repository
public class SalaDaoImpl implements SalaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addSala(int camaSal, int habitacionSal, int idHospital) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_SAL_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, camaSal)
            				.setParameter(1, habitacionSal)
                            .setParameter(2, idHospital);             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Sala> listaSala() {
		
		return sessionFactory.getCurrentSession().createQuery("from Sala sa"
				+ " join fetch sa.idHospital ih", Sala.class).getResultList();
	}

	@Override
	public boolean udpSala(int idSala, int camaSal, int habitacionSal, int idHospital) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_SAL_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idSala)
            				.setParameter(1, camaSal)
                            .setParameter(2, habitacionSal)
                            .setParameter(3, idHospital);             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Sala buscarSala(int idSala) {
		
		return sessionFactory.getCurrentSession().createQuery("from Sala sa"
				+ " join fetch sa.idHospital ih "
				+ " where sa.idSala=:idSala ", Sala.class).setParameter("idSala", idSala).getSingleResult();
	}

}
