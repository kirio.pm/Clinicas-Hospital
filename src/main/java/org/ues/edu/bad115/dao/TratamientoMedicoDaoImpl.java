package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.TratamientoMedico;

@Repository
public class TratamientoMedicoDaoImpl implements TratamientoMedicoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addTratamientoMedico(int frecuenciaTra, String tipoTra, int idDiagnostico) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_TRA_MED_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, frecuenciaTra)
            				.setParameter(1, tipoTra)
                            .setParameter(2, idDiagnostico);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<TratamientoMedico> listaTratamientoMedico() {
		
		return sessionFactory.getCurrentSession().createQuery("from TratamientoMedico tm "
				+ " join fetch tm.idDiagnostico id "
				+ " join fetch id.idCataDiag ", TratamientoMedico.class).getResultList();
	}

	@Override
	public boolean udpTratamientoMedico(int idTratamiento, int frecuenciaTra, String tipoTra, int idDiagnostico) {
			try {
				StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_TRA_MED_PROCE")
						.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
						.registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN)
						.registerStoredProcedureParameter(2 , String.class , ParameterMode.IN)
	                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);                    
	            storedProcedure.setParameter(0, idTratamiento)
	            			   .setParameter(1, frecuenciaTra)
	            			   .setParameter(2, tipoTra)
	                           .setParameter(3, idDiagnostico);                          
	             
	            storedProcedure.execute();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		return false;
	}

	@Override
	public TratamientoMedico buscarTratamientoMedico(int idTratamiento) {
		
		return sessionFactory.getCurrentSession().createQuery("from TratamientoMedico tm "
				+ " join fetch tm.idDiagnostico id"
				+ " join fetch id.idCataDiag icd "
				+ " where tm.idTratamiento=:idTratamiento", TratamientoMedico.class)
				.setParameter("idTratamiento", idTratamiento).getSingleResult();
	}

}
