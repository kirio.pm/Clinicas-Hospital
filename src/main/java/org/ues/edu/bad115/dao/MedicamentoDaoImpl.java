package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Medicamento;

@Repository
public class MedicamentoDaoImpl implements MedicamentoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addMedicamento(String docisMed,  int idCatMedi,  int idTratamiento) {
		try {
			
			StoredProcedureQuery storedProcedure=sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_MEDCA_PROCE")
					.registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, docisMed)
            				.setParameter(1, idCatMedi)
            				.setParameter(2, idTratamiento);
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Medicamento> listaMedicamento() {
		
		return sessionFactory.getCurrentSession().createQuery("from Medicamento me"
				+ " join fetch me.idCatMedi "
				+ " join fetch me.idTratamiento tr "
				+ " join fetch tr.idDiagnostico di"
				, Medicamento.class).getResultList();
	}

	@Override
	public boolean udpMedicamento(int idMedicamento, String docisMed, int idCatMedi, int idTratamiento) {
		try {
			StoredProcedureQuery storedProcedure=sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_MEDCA_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, idMedicamento)
		            .setParameter(1, docisMed)
		            .setParameter(3, idCatMedi)
		            .setParameter(5, idTratamiento);

			storedProcedure.execute();			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Medicamento buscarMedicamento(int idMedicamento) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Medicamento me "
				+ " join fetch me.idCosto "
				+ " join fetch me.idCatMedi "
				+ " join fetch me.idTratamiento tr "
				+ " join fetch tr.idDiagnostico di"
				+ " where me.idMedicamento=:idMedicamento", Medicamento.class).setParameter("idMedicamento", idMedicamento).getSingleResult();
	}
	
}
