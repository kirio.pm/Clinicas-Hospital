package org.ues.edu.bad115.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.modelos.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {
	
	@Override
	public User findById(int id) {
		return getByKey(id);
	}

	@Override
	public User findByUsername(String username) {
		return selectEntityWhere("username", username);
	}

	@Override
	public List<User> findAll() {		
		getSession().flush();
		getSession().clear();
		System.err.println("limpiando session lista");
		return getSession().createQuery("from User u "
				+ "join fetch u.empleado emp ", User.class)
				.getResultList();
	}

	@Override
	public void updateUser(User user) {
		getSession().flush();
		getSession().clear();
		 update(user);
		
	}

	@Override
	public void deleteUser(User user) {
		delete(user);
	}

	@Override
	public void saveUser(User user) {
		persist(user);
	}

	@Override
	public Empleado getPersonalFromUser(int id) {
		try {
		return getSession().createQuery("select per "
				+ "from User u "
				+ "join u.empleado per "
				+ "where u.id = :id", Empleado.class)
				.setParameter("id", id)
				.getSingleResult();
		}catch(NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Profile> findAllProfilesFromUser(int id) {
		return getSession().createQuery("select prof "
				+ "from User u "
				+ "join u.profiles prof "
				+ "where u.id = :id", Profile.class)
				.setParameter("id", id)
				.getResultList();
	}

	@Override
	public Empleado getPersonalFromUser(String username) {
		getSession().flush();
		getSession().clear();
		try {
		return getSession().createQuery("from Empleado p "
				+ "where p.user.username = :username",Empleado.class)
				.setParameter("username", username)
				.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Profile> findAllProfilesFromUser(String username) {
			return getSession().createQuery("select p "
					+ "from User u "
					+ "join u.profiles p "
					+ "where u.username = :username",Profile.class)
					.setParameter("username", username)
					.getResultList();
	}

	@Override
	public User getInitializeUserByUsername(String username) {
		getSession().flush();
		getSession().clear();
		try {
			return getSession().createQuery("select u "
					+ "from User u "
					+ "join fetch u.empleado per "
					+ "join fetch u.profiles prof "
					+ "where u.username = :username",User.class)
					.setParameter("username", username)
					.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void updateUserLastLogging(int id) {
		getSession().flush();
		getSession().clear();
		getSession().createQuery("update User u " +
			"set u.last_login = :now " +
			"where u.id = :id")
			.setParameter("now", Date.from(Instant.now()))
			.setParameter("id",id)
			.executeUpdate();
	}

	@Override
	public User getInitializeUserById(int id) {
		getSession().flush();
		getSession().clear();
		System.err.println("limpiando session by id");
		try {
			return getSession().createQuery("select u "
					+ "from User u "
					+ "join fetch u.empleado em "
					+ "join fetch u.profiles prof "
					+ "where u.id = :id",User.class)
					.setParameter("id", id)
					.getSingleResult();
			
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
		
	}

}