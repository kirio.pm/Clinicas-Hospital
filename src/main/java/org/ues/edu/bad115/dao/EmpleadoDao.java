package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Empleado;

public interface EmpleadoDao {
	public boolean addEmpleado(String apellidoEmpleado,String duiEmp,
			String emilEmp,String nombreEmpleado,String telefonoEmp);
	public List<Empleado> listaEmpleados();
	public boolean updEmpleado(int idEmpleado,String apellidoEmpleado,String duiEmp,
			String emilEmp,String nombreEmpleado,String telefonoEmp);
	public Empleado buscarEmpleado(int idEmpleado);
}
