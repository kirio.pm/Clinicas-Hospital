package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.ues.edu.bad115.modelos.CitaMedica;
import org.ues.edu.bad115.modelos.HistorialClinico;
import org.ues.edu.bad115.modelos.Paciente;

public interface CitaDao {
	
	public boolean addCita(LocalDate fechaCita,String horaCita,String medioRegistro,int idDoctor,int idCatalogoCita, int idPaciente );
	public boolean EditarCita(int idCita,LocalDate fechaCita,String horaCita,String medioRegistro,int idDoctor,int idCatalogoCita, int idPaciente );
	public List<CitaMedica> listaCitas();
	public CitaMedica buscarCita(int idCita);
}
