package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import org.ues.edu.bad115.modelos.Hospitalizacion;

public interface HospitalizacionDao {
	public boolean addHospitalizacion(LocalDate fechaInicioHosp, LocalDate fechaFinHosp, int idDoctor, int idPaciente, int idSala);
	public List<Hospitalizacion> listaHospitalizacion();
	public boolean udpHospitalizacion(int idHospitalizacion,LocalDate fechaInicioHosp, LocalDate fechaFinHosp, int idDoctor, int idPaciente, int idSala);
	public Hospitalizacion buscarHospitalizacion(int idHospitalizacion);
}
