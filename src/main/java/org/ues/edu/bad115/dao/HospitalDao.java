package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Hospital;

public interface HospitalDao {
	public boolean addHospital(String direccionHos, String nombreHos, String telefonoHos, int idRegion);
	public List<Hospital> listaHospital();
	public boolean udpHospital(int idHospital, String direccionHos, String nombreHos, String telefonoHos, int idRegion);
	public Hospital buscarHospital(int idHospital);
}
