package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoCitas;

public interface CatalogoCitaDao {
	
	public List<CatalogoCitas> listaCatalogoCita();
}
