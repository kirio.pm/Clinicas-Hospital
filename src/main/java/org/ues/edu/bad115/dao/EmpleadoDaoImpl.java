package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Empleado;

@Repository
public class EmpleadoDaoImpl implements EmpleadoDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addEmpleado(String apellidoEmpleado, String duiEmp, String emilEmp, String nombreEmpleado,
			String telefonoEmp) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_EMP_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4 , String.class, ParameterMode.IN);
            storedProcedure.setParameter(0, apellidoEmpleado)
                            .setParameter(1, duiEmp)
                            .setParameter(2, emilEmp)
                            .setParameter(3, nombreEmpleado)
                            .setParameter(4, telefonoEmp);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}

	@Override
	public List<Empleado> listaEmpleados() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Empleado", Empleado.class).getResultList();
	}

	@Override
	public boolean updEmpleado(int idEmpleado, String apellidoEmpleado, String duiEmp, String emilEmp,
			String nombreEmpleado, String telefonoEmp) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_EMP_PROCE")
					.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5 , String.class, ParameterMode.IN);
			storedProcedure.setParameter(0, idEmpleado)
							.setParameter(1, apellidoEmpleado)
				            .setParameter(2, duiEmp)
				            .setParameter(3, emilEmp)
				            .setParameter(4, nombreEmpleado)
				            .setParameter(5, telefonoEmp);
			storedProcedure.execute();
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Empleado buscarEmpleado(int idEmpleado) {
		
		return sessionFactory.getCurrentSession().createQuery("from Empleado e where e.idEmpleado=:idEmpleado", Empleado.class)
				.setParameter("idEmpleado", idEmpleado).getSingleResult();
	}

}
