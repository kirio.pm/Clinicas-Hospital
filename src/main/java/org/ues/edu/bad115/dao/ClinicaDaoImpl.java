package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Clinica;

@Repository
public class ClinicaDaoImpl implements ClinicaDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addClinica(String direccionCli, String nombreCli, String telefonoCli, int idRegion) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_CLINICA_PROCE")
					.registerStoredProcedureParameter(0 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, direccionCli)
            				.setParameter(1, nombreCli)
                            .setParameter(2, telefonoCli)
                            .setParameter(3, idRegion);             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Clinica> listaClinica() {
		
		return sessionFactory.getCurrentSession().createQuery("from Clinica cl "
				+ "join fetch cl.idRegion", Clinica.class).getResultList();
	}

	@Override
	public boolean udpClinica(int idClinica, String direccionCli, String nombreCli, String telefonoCli, int idRegion) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_CLINICA_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idClinica)
            				.setParameter(1, direccionCli)
            				.setParameter(2, nombreCli)
                            .setParameter(3, telefonoCli)
                            .setParameter(4, idRegion);          
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Clinica buscarClinica(int idClinica) {
		
		return sessionFactory.getCurrentSession().createQuery("from Clinica cl "
				+ " join fetch cl.idRegion rg "
				+ " where cl.idClinica=:idClinica", Clinica.class).setParameter("idClinica", idClinica).getSingleResult();
	}
}
