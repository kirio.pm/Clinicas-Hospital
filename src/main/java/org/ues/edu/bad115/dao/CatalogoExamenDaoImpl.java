package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoDeExamenes;

@Repository
public class CatalogoExamenDaoImpl  implements CatalogoExamenDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<CatalogoDeExamenes> listaCatalogoExamen() {
		return sessionFactory.getCurrentSession().createQuery("select catalogo "
				+ "from CatalogoDeExamenes catalogo",CatalogoDeExamenes.class)
				.getResultList();
	}
	
}
