package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;
import org.ues.edu.bad115.modelos.Cirugia;

public interface CirugiaDao {
	public boolean addCirugia(LocalDate fechaCiru, String horaCiru, String procesoCiru,int idBitacora, int idCatCiru);
	public List<Cirugia> listaCirugia();
	public boolean udpCirugia(int idCirugia,LocalDate fechaCiru, String horaCiru, 
			String procesoCiru,int idBitacora, int idCatCiru);
	public Cirugia buscarCirugia(int idCirugia);
}
