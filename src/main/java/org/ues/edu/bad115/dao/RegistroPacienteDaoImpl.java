package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Paciente;

@Repository
public class RegistroPacienteDaoImpl implements RegistroPacienteDao{
	@Autowired 
	private SessionFactory sessionFactory;
	
	@Override
	public Paciente buscarPaciente(int idPaciente) {
		return	sessionFactory.getCurrentSession().createQuery("from  Paciente px"
				+ " where px.idPaciente=:idPaciente",Paciente.class).setParameter("idPaciente", idPaciente).getSingleResult();
		 
	}

	@Override
	public List<Paciente> listaPaciente() {		
		return sessionFactory.getCurrentSession().createQuery("from Paciente pc ",Paciente.class).getResultList();
	}

	@Override
	public boolean guardarPaciente(String apellidoCasadoPac,
			String apellidoPac,String direccionPac, 
			String emailPac,String estadoCivilPac,
			LocalDate fechaNacimientoPac,String generoPac,
			String nombrePac,String numeroIdentificacion,
			String responsablePorEmergenciaPac,String telefonoPac
			) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_PACIENTE_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(5 , LocalDate.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(6 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(7 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(8 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(9 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(10 , String.class, ParameterMode.IN);
             storedProcedure.setParameter(0, apellidoCasadoPac)
                            .setParameter(1, apellidoPac)
                            .setParameter(2, direccionPac)
                            .setParameter(3, emailPac)
                            .setParameter(4, estadoCivilPac)
                            .setParameter(5, fechaNacimientoPac)
                            .setParameter(6, generoPac)
                            .setParameter(7, nombrePac)
                            .setParameter(8, numeroIdentificacion)
                            .setParameter(9, responsablePorEmergenciaPac)
                            .setParameter(10,telefonoPac );
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return true;
}

	@Override
	public boolean actualizarPaciente(
			int idPaciente,
			String apellidoCasadoPac,String apellidoPac,
			String direccionPac,String emailPac,
			String estadoCivilPac,LocalDate fechaNacimientoPac,	
			String generoPac,String nombrePac,
			String numeroIdentificacion,String responsablePorEmergenciaPac,
			String telefonoPac
			) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_PACIENTE_PROCE_UPDATE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(5 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(6 , LocalDate.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(7 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(8 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(9 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(10 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(11 , String.class, ParameterMode.IN);
             storedProcedure.setParameter(0, idPaciente)
             				.setParameter(1, apellidoCasadoPac)
                            .setParameter(2, apellidoPac)
                            .setParameter(3, direccionPac)
                            .setParameter(4, emailPac)
                            .setParameter(5, estadoCivilPac)
                            .setParameter(6, fechaNacimientoPac)
                            .setParameter(7, generoPac)
                            .setParameter(8, nombrePac)
                            .setParameter(9, numeroIdentificacion)
                            .setParameter(10, responsablePorEmergenciaPac)
                            .setParameter(11,telefonoPac );
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}

	

	
}
