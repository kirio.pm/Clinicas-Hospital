package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Cirugia;

@Repository
public class CirugiaDaoImpl implements CirugiaDao{
	
	@Autowired
	public SessionFactory sessionFactory;

	@Override
	public boolean addCirugia(LocalDate fechaCiru, String horaCiru, String procesoCiru, int idBitacora, int idCatCiru) {
		// TODO Auto-generated method stub
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_CIRU_PROCE")
                    .registerStoredProcedureParameter(0 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, fechaCiru)
                            .setParameter(1, horaCiru)
                            .setParameter(2, procesoCiru)
                            .setParameter(3, idBitacora)
                            .setParameter(4, idCatCiru);                          
            storedProcedure.execute();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Cirugia> listaCirugia() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Cirugia ci "
				+ " join fetch ci.idCatCiru cc"
				+ " join fetch ci.idBitacora bi "
				+ " join fetch bi.idHospitalizacion hos "
				+ " join fetch hos.idPaciente ", Cirugia.class).getResultList();
	}

	@Override
	public boolean udpCirugia(int idCirugia, LocalDate fechaCiru, String horaCiru, String procesoCiru,int idBitacora, int idCatCiru) {
		// TODO Auto-generated method stub
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_CIRU_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(5 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idCirugia)
            				.setParameter(1, fechaCiru)
                            .setParameter(2, horaCiru)
                            .setParameter(3, procesoCiru)
                            .setParameter(4, idBitacora)
                            .setParameter(5, idCatCiru);                          
            storedProcedure.execute();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Cirugia buscarCirugia(int idCirugia) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Cirugia ci "
				+ " join fetch ci.idCatCiru cc"
				+ " join fetch ci.idBitacora bi "
				+ " join fetch bi.idHospitalizacion hos "
				+ " join fetch hos.idPaciente "
				+ " where ci.idCirugia=:idCirugia ", Cirugia.class).setParameter("idCirugia", idCirugia).getSingleResult();
	}
}
