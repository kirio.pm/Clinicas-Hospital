package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Sintomatologia;

public interface SintomatologiaDao {
	public boolean addSintomatologia( String sintomas, int idConsulta);
	public List<Sintomatologia> listaSintoma();
	public boolean udpSintomatologia(int idSintomatologia, String sintomas, int idConsulta);
	public Sintomatologia buscarSintoma(int idSintomatologia);
}
