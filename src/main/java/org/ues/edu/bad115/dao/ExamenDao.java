package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Examen;

public interface ExamenDao {
	public void guardarExamen(Examen examen);
	public void editarExamen(Examen examen);
	public void eliminarExamen(Examen examen);
	public Examen buscarExamen(int id);
	public List<Examen> buscarTodosLosExamenesDeConsulta(int id_consulta);

}
