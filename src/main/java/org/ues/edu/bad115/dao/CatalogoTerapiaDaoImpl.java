package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoTerapia;

@Repository
public class CatalogoTerapiaDaoImpl implements CatalogoTerapiaDao {
	
	@Autowired
	public SessionFactory sessionFactory;
	
	@Override
	public List<CatalogoTerapia> listaTerapia() {
		
		return sessionFactory.getCurrentSession().createQuery("from CatalogoTerapia ct", CatalogoTerapia.class).getResultList();
	}

}
