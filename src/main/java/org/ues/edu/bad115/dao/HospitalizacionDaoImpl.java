package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Hospitalizacion;

@Repository
public class HospitalizacionDaoImpl implements HospitalizacionDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addHospitalizacion(LocalDate fechaInicioHosp, LocalDate fechaFinHosp, int idDoctor, int idPaciente,
			int idSala) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_HOSPITALIZACION_PROCE")
					.registerStoredProcedureParameter(0, LocalDate.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, LocalDate.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, fechaInicioHosp)
					       .setParameter(1, fechaFinHosp )
					       .setParameter(2, idDoctor)
					       .setParameter(3, idPaciente)
					       .setParameter(4, idSala);
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Hospitalizacion> listaHospitalizacion() {
		
		return sessionFactory.getCurrentSession().createQuery("from Hospitalizacion hos "
				+ "join fetch hos.idDoctor doc "
				+ "join fetch doc.idEmpleado em "
				+ "join fetch hos.idPaciente pac "
				//+ "join fetch pac.idExpediente ex "
				+ "join fetch hos.idSala sa ", Hospitalizacion.class).getResultList();
	}

	@Override
	public boolean udpHospitalizacion(int idHospitalizacion, LocalDate fechaInicioHosp, LocalDate fechaFinHosp,
			int idDoctor, int idPaciente, int idSala) {
try {
	StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_HOSPITALIZACION_PROCE")
			.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
			.registerStoredProcedureParameter(1, LocalDate.class, ParameterMode.IN)
			.registerStoredProcedureParameter(2, LocalDate.class, ParameterMode.IN)
			.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
			.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN)
			.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN);
	storedProcedure.setParameter(0, idHospitalizacion)
				   .setParameter(1, fechaInicioHosp)
			       .setParameter(2, fechaFinHosp )
			       .setParameter(3, idDoctor)
			       .setParameter(4, idPaciente)
			       .setParameter(5, idSala);
	storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Hospitalizacion buscarHospitalizacion(int idHospitalizacion) {
		
		return sessionFactory.getCurrentSession().createQuery("from Hospitalizacion hos "
				+ "join fetch hos.idDoctor doc "
				+ "join fetch hos.idPaciente pac "
				+ "join fetch hos.idSala sa "
				+ "where hos.idHospitalizacion=:idHospitalizacion", Hospitalizacion.class).setParameter("idHospitalizacion", idHospitalizacion).getSingleResult();
	}
	
}
