package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoTerapia;

public interface CatalogoTerapiaDao {

	public List<CatalogoTerapia> listaTerapia();
}
