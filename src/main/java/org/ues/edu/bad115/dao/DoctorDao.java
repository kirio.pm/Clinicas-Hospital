package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Doctor;

public interface DoctorDao {
	
	public boolean addDoctor(String especialidadDoc, int idEmpleado);
	public List<Doctor> listaDoctor();
	public boolean udpDoctor(int idDoctor, String especialidadDoc, int idEmpleado);
	public Doctor buscarDoctor(int idDoctor);
}
