package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CitaMedica;
import org.ues.edu.bad115.modelos.HistorialClinico;
import org.ues.edu.bad115.modelos.Paciente;

@Repository
public class CitaDaoImpl implements CitaDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addCita(LocalDate fechaCita, String horaCita, String medioRegistro, int idDoctor, int idCatalogoCita,
			int idPaciente) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_CIT_MED_PROCE")
					.registerStoredProcedureParameter(0, LocalDate.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
					;
			storedProcedure.setParameter(0, fechaCita)
					       .setParameter(1, horaCita )
					       .setParameter(2, medioRegistro)
					       .setParameter(3, idDoctor)
					       .setParameter(4, idCatalogoCita)
					       .setParameter(5, idPaciente)
					       ;
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<CitaMedica> listaCitas() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from CitaMedica cx join fetch cx.idPaciente" , CitaMedica.class).getResultList();
	}

	@Override
	public boolean EditarCita(int idCita,LocalDate fechaCita, String horaCita, String medioRegistro, int idDoctor,
			int idCatalogoCita, int idPaciente) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_CIT_MED_UP_PROCE")
					.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, LocalDate.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(6, Integer.class, ParameterMode.IN)
					;
			storedProcedure.setParameter(0, idCita)
						   .setParameter(1, fechaCita)
					       .setParameter(2, horaCita )
					       .setParameter(3, medioRegistro)
					       .setParameter(4, idDoctor)
					       .setParameter(5, idCatalogoCita)
					       .setParameter(6, idPaciente)
					       ;
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public CitaMedica buscarCita(int idCita) {
		// TODO Auto-generated method stub
	return	sessionFactory.getCurrentSession().createQuery("from  CitaMedica CM "
															+ " join fetch CM.idDoctor doc "
															+ " join fetch doc.idEmpleado "
															+ " join  fetch CM.idCatalogoCita "
															+ " join fetch CM.idPaciente "
				+ " where CM.idCita=:idCita",CitaMedica.class).setParameter("idCita", idCita).getSingleResult();
	}

	


	
}
