package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.TratamientoMedico;

public interface TratamientoMedicoDao {
	public boolean addTratamientoMedico(int frecuenciaTra, String tipoTra, int idDiagnostico);
	public List<TratamientoMedico> listaTratamientoMedico();
	public boolean udpTratamientoMedico(int idTratamiento, int frecuenciaTra, String tipoTra, int idDiagnostico);
	public TratamientoMedico buscarTratamientoMedico(int idTratamiento);
}
