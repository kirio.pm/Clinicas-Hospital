package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Sintomatologia;

@Repository
public class SintomatologiaDaoImpl implements SintomatologiaDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addSintomatologia(String sintomas, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_SIN_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, sintomas)
                            .setParameter(1, idConsulta);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Sintomatologia> listaSintoma() {
		return sessionFactory.getCurrentSession().createQuery("from Sintomatologia si "
				+ " join fetch si.idConsulta con "
				+ " join fetch con.idExpediente", Sintomatologia.class).getResultList();
	}

	@Override
	public boolean udpSintomatologia(int idSintomatologia, String sintomas, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UDP_SIN_PROCE")
                    .registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idSintomatologia)
            				.setParameter(1, sintomas)
                            .setParameter(2, idConsulta);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Sintomatologia buscarSintoma(int idSintomatologia) {
		
		return sessionFactory.getCurrentSession().createQuery("from Sintomatologia si"
				+ " join fetch si.idConsulta  "
				+ " where si.idSintomatologia=:idSintomatologia", Sintomatologia.class)
				.setParameter("idSintomatologia", idSintomatologia).getSingleResult();
	}

}
