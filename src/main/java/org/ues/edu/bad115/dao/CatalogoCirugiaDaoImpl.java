package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoCirugia;

@Repository
public class CatalogoCirugiaDaoImpl implements CatalogoCirugiaDao{
	
	@Autowired
	public SessionFactory sessionFactory;

	@Override
	public List<CatalogoCirugia> listaCirugia() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from CatalogoCirugia", CatalogoCirugia.class).getResultList();
	}
	
	
}
