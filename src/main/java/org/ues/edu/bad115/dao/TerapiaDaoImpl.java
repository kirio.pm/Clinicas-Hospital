package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Terapia;

@Repository
public class TerapiaDaoImpl implements TerapiaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addTerapia(LocalDate fechaDeTera, int idCatTera,  int idTratamiento) {
		try {
			StoredProcedureQuery storedProcedure=sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_TERA_PROCE")
					.registerStoredProcedureParameter(0 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, fechaDeTera)
            				.setParameter(1, idCatTera)
            				.setParameter(2, idTratamiento);
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Terapia> listaTerapia() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Terapia te"
				+ " join fetch te.idTratamiento tr "
				+ " join fetch tr.idDiagnostico di "
				+ " join fetch te.idCatTera ct", Terapia.class).getResultList();
	}

	@Override
	public boolean udpTerapia(int idTerapia, LocalDate fechaDeTera,  int idCatTera, 
			int idTratamiento) {
		try {
			StoredProcedureQuery storedProcedure=sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_MEDCA_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, idTerapia)
		            .setParameter(1, fechaDeTera)
		            .setParameter(2, idCatTera)
		            .setParameter(3, idTratamiento);

			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Terapia buscarTerapia(int idTerapia) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Terapia te"
				+ " join fetch te.idTratamiento tr "
				+ " join fetch tr.idDiagnostico di "
				+ " join fetch te.idCatTera ct "
				+ " where te.idTerapia=:idTerapia", Terapia.class).setParameter("idTerapia", idTerapia).getSingleResult();
	}
}
