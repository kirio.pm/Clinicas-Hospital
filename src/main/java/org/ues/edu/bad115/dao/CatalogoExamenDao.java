package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.CatalogoDeExamenes;

public interface CatalogoExamenDao {
	
	public List<CatalogoDeExamenes> listaCatalogoExamen();

}
