package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Sala;

public interface SalaDao {
	public boolean addSala(int camaSal, int habitacionSal, int idHospital);
	public List<Sala> listaSala();
	public boolean udpSala(int idSala, int camaSal, int habitacionSal, int idHospital);
	public Sala buscarSala(int idSala);

}
