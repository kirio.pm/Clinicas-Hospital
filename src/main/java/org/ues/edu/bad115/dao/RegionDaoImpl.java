package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.ues.edu.bad115.modelos.Region;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegionDaoImpl implements RegionDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addRegion(String continente, String pais) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_REG_PROCE")
					.registerStoredProcedureParameter(0 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class , ParameterMode.IN);                    
            storedProcedure.setParameter(0, continente)
            				.setParameter(1, pais);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Region> listaRegion() {
		
		return sessionFactory.getCurrentSession().createQuery("from Region", Region.class).getResultList();
	}

	@Override
	public boolean udpRegion(int idRegion, String continente, String pais) {
		try {
			StoredProcedureQuery storedProcedure = 
					sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_REG_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2 , String.class , ParameterMode.IN);                    
            storedProcedure.setParameter(0, idRegion)
            				.setParameter(1, continente)
            				.setParameter(2, pais);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Region buscarRegion(int idRegion) {
		
		return sessionFactory.getCurrentSession().createQuery("from Region reg"
				+ " where reg.idRegion=:idRegion",Region.class).setParameter("idRegion", idRegion).getSingleResult();
	}
	
	
}
