package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.ExpedienteClinico;

@Repository
public class ExpedienteDaoImp implements ExpedienteDao{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public boolean GuardarExpediente(int ID_PACIENTE) {
		// TODO Auto-generated method stub
		try {
			StoredProcedureQuery storeProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_EXPEDIENTE_PROCE")
					.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
			storeProcedure.setParameter(0, ID_PACIENTE);
			storeProcedure.execute();
		} catch (Exception e) {
			// TODO: handle exception
		e.printStackTrace();
		return false;
		}
		return true;
	}


	@Override
	public List<ExpedienteClinico> ListarExpedientes() {

	return sessionFactory.getCurrentSession().createQuery("from ExpedienteClinico expC "
			+ "join fetch expC.idPaciente px",ExpedienteClinico.class).getResultList();
	}


	@Override
	public ExpedienteClinico BuscarExpediente(int idExpediente) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from ExpedienteClinico expC "
				+ "join fetch expC.idPaciente px where expC.idExpediente=:idExpediente",ExpedienteClinico.class).setParameter("idExpediente",idExpediente).getSingleResult();	
	}

}
