package org.ues.edu.bad115.dao;

import java.util.List;


import org.ues.edu.bad115.modelos.SignosVitales;

public interface SignosVitalesDao {
	public boolean addSignosVitales(double alturaSiv, int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv, double temperaturaSiv, int idConsulta);
	public List<SignosVitales> listaSigno();
	public boolean udpSignoVitales(int idSignosVitales, double alturaSiv,int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv,double temperaturaSiv,int idConsulta);
	public SignosVitales buscarSigno(int idSignosVitales);

}
