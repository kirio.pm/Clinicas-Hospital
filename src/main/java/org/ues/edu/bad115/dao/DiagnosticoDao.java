package org.ues.edu.bad115.dao;

import java.time.LocalDate;
//import java.sql.Date;
import java.util.Date;
import java.util.List;

import org.ues.edu.bad115.modelos.Diagnostico;

public interface DiagnosticoDao {
	public boolean addDiagnostico( int idCataDiag, int idConsulta);
	public List<Diagnostico> listaDiagnostico();
	public boolean udpDiagnostico(int idDiagnostico, LocalDate fechaDiag, int idCataDiag, int idConsulta);
	public Diagnostico buscarDiagnostico(int idDiagnostico);
}
