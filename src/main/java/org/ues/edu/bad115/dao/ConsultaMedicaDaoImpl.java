package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.ConsultaMedica;

@Repository
public class ConsultaMedicaDaoImpl implements ConsultaMedicaDao{
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addConsultaMedica(int idCatCon, int idDoctor, String descripcion, int idExpediente) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_CON_MED_PROCE ")
                    .registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, idCatCon)
                            .setParameter(1, idDoctor)
                            .setParameter(2, descripcion)
                            .setParameter(3, idExpediente);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;	
		}
		return false;
	}

	@Override
	public List<ConsultaMedica> listaConsulta() {
		return sessionFactory.getCurrentSession().createQuery("from ConsultaMedica con"
				+ " join fetch con.idCatCon cat"
				+ " join fetch con.idDoctor doc"
				+ " join fetch doc.idEmpleado emp"
				+ " join fetch con.idExpediente exp", ConsultaMedica.class).getResultList();
	}

	@Override
	public boolean udpConsultaMedica(int idConsulta, String descripcion, int idDoctor) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UDP_CON_MED_PROCE ")
                    .registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, idConsulta)
                            .setParameter(1, descripcion)
                            .setParameter(2, idDoctor);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public ConsultaMedica buscarConsulta(int idConsulta) {
		
		return sessionFactory.getCurrentSession().createQuery("from ConsultaMedica cm"
				+ " join fetch cm.idDoctor dc "
				+ " join fetch dc.idEmpleado em "
				+ " left join fetch cm.examenList examenes "
				+ " where cm.idConsulta=:idConsulta", ConsultaMedica.class)
				.setParameter("idConsulta", idConsulta).getSingleResult();
	}

	@Override
	public void actualizarConsulta(ConsultaMedica consulta) {
		sessionFactory.getCurrentSession().saveOrUpdate(consulta);
	}

	@Override
	public ConsultaMedica buscarConsultaMedica(int id_consulta) {
		// TODO Auto-generated method stub
		return null;
	}

}
