package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CostosPorServicios;

@Repository
public class CostoDaoImpl implements CostoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addCosto(LocalDate fechaCosto, String unidadMonetaria) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_COS_SER_PROCE")
                    .registerStoredProcedureParameter(0 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, fechaCosto)
                            .setParameter(1, unidadMonetaria);          
            storedProcedure.execute();		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<CostosPorServicios> listaCosto() {
		
		return sessionFactory.getCurrentSession().createQuery("from CostosPorServicios", CostosPorServicios.class).getResultList();
	}

	@Override
	public boolean udpCosto(int idCosto, LocalDate fechaCosto, String unidadMonetaria) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UPD_COS_SER_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , String.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idCosto)
            				.setParameter(1, fechaCosto)
                            .setParameter(2, unidadMonetaria);          
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public CostosPorServicios buscarCosto(int idCosto) {
		
		return sessionFactory.getCurrentSession().createQuery("from CostosPorServicios cs"
				+ " where cs.idCosto=:idCosto", CostosPorServicios.class).setParameter("idCosto", idCosto).getSingleResult();
	}
}
