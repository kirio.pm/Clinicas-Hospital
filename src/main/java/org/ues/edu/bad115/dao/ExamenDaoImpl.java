package org.ues.edu.bad115.dao;


import java.util.List;

import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Examen;
@Repository
public class ExamenDaoImpl extends AbstractDao<Integer,Examen> implements ExamenDao {

	@Override
	public void guardarExamen(Examen examen) {
		persist(examen);
		
	}

	@Override
	public void editarExamen(Examen examen) {
		update(examen);
		
	}

	@Override
	public void eliminarExamen(Examen examen) {
		delete(examen);
		
	}

	@Override
	public Examen buscarExamen(int id) {
		return getByKey(id);
	}

	@Override
	public List<Examen> buscarTodosLosExamenesDeConsulta(int id_consulta) {
		return getSession().createQuery("select examen "
				+ "from Examen examen "
				+ "inner join fetch examen.idCatExamen catalogo "
				+ "inner join examen.idConsultaMedica consulta "
				+ "where consulta.idConsulta = :id",Examen.class)
		.setParameter("id", id_consulta)
		.getResultList();
		}
	
}
