package org.ues.edu.bad115.dao;

import java.util.List;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Doctor;

@Repository
public class DoctorDaoImpl implements DoctorDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addDoctor(String especialidadDoc, int idEmpleado) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_DOC_PROCE")
					.registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, especialidadDoc)
					.setParameter(1, idEmpleado);
			storedProcedure.execute();
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Doctor> listaDoctor() {
		return sessionFactory.getCurrentSession().createQuery("from Doctor dc "
				+" join fetch dc.idEmpleado", Doctor.class).getResultList();
	}

	@Override
	public boolean udpDoctor(int idDoctor, String especialidadDoc, int idEmpleado) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UDP_DOC_PROCE")
					.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, idDoctor)
					.setParameter(1, especialidadDoc)
					.setParameter(2, idEmpleado);
			storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Doctor buscarDoctor(int idDoctor) {
		
		return sessionFactory.getCurrentSession().createQuery("from Doctor dc"
				+ " join fetch dc.idEmpleado em "
				+ " where dc.idDoctor=:idDoctor", Doctor.class)
				.setParameter("idDoctor", idDoctor).getSingleResult();
	}

}
