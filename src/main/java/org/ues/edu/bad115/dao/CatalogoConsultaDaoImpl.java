package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.CatalogoConsulta;

@Repository
public class CatalogoConsultaDaoImpl implements CatalogoConsultaDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<CatalogoConsulta> listaCatalogo() {
		return sessionFactory.getCurrentSession().createQuery("from CatalogoConsulta", CatalogoConsulta.class).getResultList();
	}

}
