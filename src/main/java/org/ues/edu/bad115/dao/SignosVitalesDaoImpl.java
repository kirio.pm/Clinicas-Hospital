package org.ues.edu.bad115.dao;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.SignosVitales;

@Repository
public class SignosVitalesDaoImpl implements SignosVitalesDao {
	
	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public boolean addSignosVitales(double alturaSiv, int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv, double temperaturaSiv, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_SIG_VIT_PROCE")
                    .registerStoredProcedureParameter(0 , Double.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Double.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(4 , Double.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, alturaSiv)
                            .setParameter(1, frecuenciaCardiacaSiv)
                            .setParameter(2, pesoSiv)
                            .setParameter(3, presionArterialSiv)
                            .setParameter(4, temperaturaSiv)
                            .setParameter(5, idConsulta);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}


	@Override
	public List<SignosVitales> listaSigno() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from SignosVitales sig "
				+ " join fetch sig.idConsulta con "
				+ " join fetch con.idExpediente ex", SignosVitales.class).getResultList();
	}


	@Override
	public boolean udpSignoVitales(int idSignosVitales, double alturaSiv, int frecuenciaCardiacaSiv, double pesoSiv,
			String presionArterialSiv, double temperaturaSiv, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UDP_SIG_VIT_PROCE")
					.registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
					.registerStoredProcedureParameter(1 , Double.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Double.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class, ParameterMode.IN)
					.registerStoredProcedureParameter(5 , Double.class, ParameterMode.IN)
					.registerStoredProcedureParameter(6 , Integer.class, ParameterMode.IN);
            storedProcedure.setParameter(0, idSignosVitales)
            				.setParameter(1, alturaSiv)
                            .setParameter(2, frecuenciaCardiacaSiv)
                            .setParameter(3, pesoSiv)
                            .setParameter(4, presionArterialSiv)
                            .setParameter(5, temperaturaSiv)
                            .setParameter(6, idConsulta);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}


	@Override
	public SignosVitales buscarSigno(int idSignosVitales) {
		
		return sessionFactory.getCurrentSession().createQuery("from SignosVitales sv"
				+ " join fetch sv.idConsulta  "
				+ " where sv.idSignosVitales=:idSignosVitales", SignosVitales.class)
				.setParameter("idSignosVitales", idSignosVitales).getSingleResult();
	}

}
