package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Profile;

public interface ProfileDao {
	
	public Profile findById(int id);
	
	public List<Profile> findAll();
	
	public Profile findByType(String type);
	
	public Profile findInitializeProfileById(int id);
}
