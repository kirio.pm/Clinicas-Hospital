package org.ues.edu.bad115.dao;

import java.util.List;

import org.ues.edu.bad115.modelos.Empleado;
import org.ues.edu.bad115.modelos.Profile;
import org.ues.edu.bad115.modelos.User;

public interface UserDao {
	public User findById(int id);

	public User findByUsername(String username);

	public void deleteUser(User user);

	public List<User> findAll();

	public void saveUser(User user);
	
	public void updateUser(User user);
	
	public List<Profile> findAllProfilesFromUser(int id);
	
	public List<Profile> findAllProfilesFromUser(String username);
	
	public User getInitializeUserByUsername(String username);

	public void updateUserLastLogging(int id);
	
	public Empleado getPersonalFromUser(int id);
	
	public Empleado getPersonalFromUser(String username);
	
	public User getInitializeUserById(int id);
}
