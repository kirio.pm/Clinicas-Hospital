package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Diagnostico;

@Repository
public class DiagnosticoDaoImpl implements DiagnosticoDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addDiagnostico(int idCataDiag, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_DIAG_PROCE")
                    .registerStoredProcedureParameter(0 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idCataDiag)
                            .setParameter(1, idConsulta);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public List<Diagnostico> listaDiagnostico() {
		
		return sessionFactory.getCurrentSession().createQuery("from Diagnostico di "
				+ " join fetch di.idConsulta co"
				+ " join fetch co.idExpediente ex"
				+ " join fetch co.idCatCon cat "
				+ " join fetch di.idCataDiag cd", Diagnostico.class).getResultList();
	}

	@Override
	public boolean udpDiagnostico(int idDiagnostico, LocalDate fechaDiag, int idCataDiag, int idConsulta) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("UDP_DIAG_PROCE")
                    .registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter(1, LocalDate.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class, ParameterMode.IN);                    
            storedProcedure.setParameter(0, idDiagnostico)
            				.setParameter(1, fechaDiag)
                            .setParameter(2, idCataDiag)
                            .setParameter(2, idConsulta);                          
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public Diagnostico buscarDiagnostico(int idDiagnostico) {
		
		return sessionFactory.getCurrentSession().createQuery("from Diagnostico di "
				+ " join fetch di.idConsulta co "
				+ " join fetch di.idCataDiag cd "
				+ " where di.idDiagnostico=:idDiagnostico", Diagnostico.class)
				.setParameter("idDiagnostico", idDiagnostico).getSingleResult();
	}
	
}
