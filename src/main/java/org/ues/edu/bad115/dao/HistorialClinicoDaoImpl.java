package org.ues.edu.bad115.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.DatosFamiliares;
import org.ues.edu.bad115.modelos.HistorialClinico;

@Repository
public class HistorialClinicoDaoImpl implements HistorialClinicoDao {

	@Autowired 
	private SessionFactory sessionFactory;		
	
	
	@Override
	public boolean guardarHistorialClinico(String enfermedadesPadecidas, 
			String tratamientoPreventivo,
			int idDatosFamiliares,
			int idPaciente) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_HISTO_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , Integer.class , ParameterMode.IN);
             storedProcedure.setParameter(0, enfermedadesPadecidas)
                            .setParameter(1, tratamientoPreventivo)
                            .setParameter(2, idDatosFamiliares)
                            .setParameter(3, idPaciente);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}


	@Override
	public List<HistorialClinico> listaHistorialClinico() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from HistorialClinico Hc join fetch Hc.idDatosFamiliares join fetch Hc.idPaciente" , HistorialClinico.class).getResultList();
	}


	@Override
	public boolean guardarHistorialDOSClinico(String enfermedadesPadecidas,
			String tratamientoPreventivo,
			int idPaciente,
			String apellidoPadre,
			String direccion,
			LocalDate fechaNacimiento,
			String historialDeEnfermedades,
			String nombrePadre) {
		try {
			StoredProcedureQuery storedProcedure = sessionFactory.getCurrentSession().createStoredProcedureQuery("ADD_HISTO_DOS_PROCE")
                    .registerStoredProcedureParameter(0 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(1 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(2 , Integer.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(3 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(4 , String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(5 , LocalDate.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(6 , String.class , ParameterMode.IN)
                    .registerStoredProcedureParameter(7 , String.class, ParameterMode.IN);;
             storedProcedure.setParameter(0, enfermedadesPadecidas)
                            .setParameter(1, tratamientoPreventivo)
                            .setParameter(2, idPaciente)
                            .setParameter(3, apellidoPadre)
                            .setParameter(4, direccion)
                            .setParameter(5, fechaNacimiento)
                            .setParameter(6, historialDeEnfermedades)
                            .setParameter(7, nombrePadre);
             
            storedProcedure.execute();
		} catch (Exception e) {
			e.printStackTrace();
            return false;
		}
		return false;
	}

}
