package org.ues.edu.bad115.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ues.edu.bad115.modelos.Log;

@Repository
public class LogDaoImpl implements LogDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Log> listaLog() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Log" , Log.class).getResultList();
	}

}
