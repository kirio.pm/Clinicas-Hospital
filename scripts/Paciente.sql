create or replace PROCEDURE ADD_PACIENTE_PROCE
(APELLIDO_DE_CASADO_PAC IN VARCHAR2,APELLIDO_PAC IN VARCHAR2,DIRECCION_PAC IN  VARCHAR2,EMAIL_PAC  IN  VARCHAR2,ESTADO_CIVIL_PAC IN  VARCHAR2,ESTADOPACIENTE IN CHAR ,
FECHA_NACIMIENTO_PAC IN DATE ,GENERO_PAC IN  VARCHAR2,NOMBRE_PAC IN  VARCHAR2,NUMERO_IDENTIFICACION IN  VARCHAR2,RESPONSABLE_POR_EMERGENCIA_PAC IN  VARCHAR2,
TELEFONO_PAC IN  VARCHAR2)
IS
BEGIN
    INSERT INTO PACIENTE 
    (APELLIDO_DE_CASADO_PAC,APELLIDO_PAC ,DIRECCION_PAC,EMAIL_PAC,ESTADO_CIVIL_PAC,ESTADOPACIENTE ,FECHA_NACIMIENTO_PAC,GENERO_PAC ,NOMBRE_PAC,NUMERO_IDENTIFICACION ,
RESPONSABLE_POR_EMERGENCIA_PAC,TELEFONO_PAC )
    VALUES (APELLIDO_DE_CASADO_PAC,APELLIDO_PAC ,DIRECCION_PAC,EMAIL_PAC,ESTADO_CIVIL_PAC,ESTADOPACIENTE ,FECHA_NACIMIENTO_PAC,GENERO_PAC ,NOMBRE_PAC,NUMERO_IDENTIFICACION ,
RESPONSABLE_POR_EMERGENCIA_PAC,TELEFONO_PAC );
END;

CREATE SEQUENCE PACIENTE_SEQUENCE
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

create or replace trigger PACIENTE_TRIGGER  
   before insert on "CLINICA"."PACIENTE" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_PACIENTE" is null then 
         select PACIENTE_SEQUENCE.nextval into :NEW."ID_PACIENTE" from dual; 
      end if; 
   end if; 
end;


