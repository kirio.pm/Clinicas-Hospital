CREATE OR REPLACE PROCEDURE ADD_CAT_DIAG_PROCE (COD_ENF in VARCHAR2,NOMBRE_ENF in VARCHAR2,TIPO_ENF IN VARCHAR2) IS
BEGIN
    INSERT INTO CATALOGODIAGNOSTICO (COD_ENF,NOMBRE_ENF,TIPO_ENF ) VALUES (COD_ENF,NOMBRE_ENF,TIPO_ENF );
END;

CREATE SEQUENCE CAT_DIAG_SEQUENCE
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

create or replace trigger CAT_DIAG_TRIGGER  
   before insert on "CLINICA"."CATALOGODIAGNOSTICO" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_CATA_DIAG" is null then 
         select CAT_DIAG_SEQUENCE.nextval into :NEW."ID_CATA_DIAG" from dual; 
      end if; 
   end if; 
end;



