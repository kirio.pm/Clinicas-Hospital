CREATE OR REPLACE PROCEDURE ADD_SINTOMA_PROCE (SINTOMAS IN VARCHAR2) IS
BEGIN
    INSERT INTO SINTOMATOLOGIA (SINTOMAS) VALUES (SINTOMAS);
END;

CREATE SEQUENCE SINTOMA_SEQUENCE
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

create or replace trigger SINTOMATOLOGIA_TRIGGER  
   before insert on "CLINICA"."SINTOMATOLOGIA" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_SINTAMOTOLOGIA" is null then 
         select SINTOMA_SEQUENCE.nextval into :NEW."ID_SINTAMOTOLOGIA" from dual; 
      end if; 
   end if; 
end;



