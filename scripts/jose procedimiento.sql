create or replace PROCEDURE insertarExpediente(ID_PACIENTE in integer) is
    codigo varchar2(7);
    numero integer;
    a�o varchar(2);
    fecha date;
begin
    select max(ID_EXPEDIENTE) into numero from EXPEDIENTECLINICO;  
    numero := numero + 1;					
     SELECT TO_CHAR (SYSDATE, 'YY') "NOW" into A�O FROM DUAL;		
    select sysdate into fecha from dual;			
    if( length(to_char(numero)) = 1) then 			
    codigo:= 'EX' || a�o || '00' || numero;
    elsif (length(to_char(numero))= 2) then
    codigo:= 'EX' || a�o || '0' || numero;
    else
    codigo:= 'EX' || a�o || numero;
    end if;

    insert into expedienteClinico (id_expediente,cod_expediente,fecha_creacion, ID_PACIENTE) 
    values (numero,id_archivo,fecha,codigo);				
end;

select max(id)  from EXPEDIENTECLINICO; 
select * from EXPEDIENTECLINICO;