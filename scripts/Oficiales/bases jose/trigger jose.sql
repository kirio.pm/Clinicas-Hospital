--------------------------------------------------------
-- Archivo creado  - domingo-junio-17-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger BITA_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."BITA_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."BITACORAPROCEDIMIENTOS" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_BITACORA" IS NULL THEN 
         SELECT BITA_SEQUENCE.NEXTVAL INTO :NEW."ID_BITACORA" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."BITA_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CIRU_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."CIRU_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."CIRUGIA" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_CIRUGIA" IS NULL THEN 
         SELECT CIRU_SEQUENCE.NEXTVAL INTO :NEW."ID_CIRUGIA" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."CIRU_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CLINICA_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."CLINICA_TRIGGER" 
   before insert on "CLINICA"."CLINICA" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_CLINICA" is null then 
         select CLINICA_SEQUENCE.nextval into :NEW."ID_CLINICA" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."CLINICA_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger CON_MED_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."CON_MED_TRIGGER" 
   before insert on "CLINICA"."CONSULTAMEDICA" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_CONSULTA" is null then 
         select CON_MED_SEQUENCE.nextval into :NEW."ID_CONSULTA" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."CON_MED_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COS_SER_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."COS_SER_TRIGGER" 
   before insert on "CLINICA"."COSTOSPORSERVICIOS" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_COSTO" is null then 
         select COS_SER_SEQUENCE.nextval into :NEW."ID_COSTO" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."COS_SER_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger DOC_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."DOC_TRIGGER" 
   before insert on "CLINICA"."DOCTOR" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_DOCTOR" is null then 
         select DOC_SEQUENCE.nextval into :NEW."ID_DOCTOR" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."DOC_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger EMP_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."EMP_TRIGGER" 
   before insert on "CLINICA"."EMPLEADO" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_EMPLEADO" is null then 
         select EMP_SEQUENCE.nextval into :NEW."ID_EMPLEADO" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."EMP_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger HOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."HOS_TRIGGER" 
   before insert on "CLINICA"."HOSPITAL" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_HOSPITAL" is null then 
         select HOS_SEQUENCE.nextval into :NEW."ID_HOSPITAL" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."HOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger HOSPITALIZACION_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."HOSPITALIZACION_TRIGGER" 
   before insert on "CLINICA"."HOSPITALIZACION" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_HOSPITALIZACION" is null then 
         select HOSPITALIZACION_SEQUENCE.nextval into :NEW."ID_HOSPITALIZACION" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."HOSPITALIZACION_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MEDCA_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."MEDCA_TRIGGER" 
   before insert on "CLINICA"."MEDICAMENTO" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_MEDICAMENTO" is null then 
         select MEDCA_SEQUENCE.nextval into :NEW."ID_MEDICAMENTO" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."MEDCA_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PACIENTE_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."PACIENTE_TRIGGER" 
   before insert on "CLINICA"."PACIENTE" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_PACIENTE" is null then 
         select PACIENTE_SEQUENCE.nextval into :NEW."ID_PACIENTE" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."PACIENTE_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger REG_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."REG_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."REGION" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_REGION" IS NULL THEN 
         SELECT REG_SEQUENCE.NEXTVAL INTO :NEW."ID_REGION" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."REG_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SAL_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."SAL_TRIGGER" 
   before insert on "CLINICA"."SALA" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_SALA" is null then 
         select SAL_SEQUENCE.nextval into :NEW."ID_SALA" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."SAL_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SEG_CIR_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."SEG_CIR_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."SEGUIMIENTOCIRUGIA" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_SEGUIMIENTO" IS NULL THEN 
         SELECT SEG_CIR_SEQUENCE.NEXTVAL INTO :NEW."ID_SEGUIMIENTO" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."SEG_CIR_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SIG_VIT_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."SIG_VIT_TRIGGER" 
   before insert on "CLINICA"."SIGNOSVITALES" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID_SIGNOS_VITALES" is null then 
         select SIG_VIT_SEQUENCE.nextval into :NEW."ID_SIGNOS_VITALES" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "CLINICA"."SIG_VIT_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SIN_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."SIN_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."SINTOMATOLOGIA" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_SINTOMATOLOGIA" IS NULL THEN 
         SELECT SIN_SEQUENCE.NEXTVAL INTO :NEW."ID_SINTOMATOLOGIA" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."SIN_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TERA_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."TERA_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."TERAPIA" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_TERAPIA" IS NULL THEN 
         SELECT TERA_SEQUENCE.NEXTVAL INTO :NEW."ID_TERAPIA" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."TERA_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRA_MED_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CLINICA"."TRA_MED_TRIGGER" 
   BEFORE INSERT ON "CLINICA"."TRATAMIENTOMEDICO" 
   FOR EACH ROW 
BEGIN  
   IF INSERTING THEN 
      IF :NEW."ID_TRATAMIENTO" IS NULL THEN 
         SELECT TRA_MED_SEQUENCE.NEXTVAL INTO :NEW."ID_TRATAMIENTO" FROM DUAL; 
      END IF; 
   END IF; 
END;
/
ALTER TRIGGER "CLINICA"."TRA_MED_TRIGGER" ENABLE;
