--------------------------------------------------------
-- Archivo creado  - domingo-junio-17-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence BITA_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."BITA_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CIRU_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."CIRU_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CLINICA_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."CLINICA_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CON_MED_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."CON_MED_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 14 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COS_SER_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."COS_SER_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 15 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DIAG_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."DIAG_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 8 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DOC_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."DOC_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence EMP_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."EMP_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 13 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence HOS_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."HOS_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence HOSPITALIZACION_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."HOSPITALIZACION_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MEDCA_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."MEDCA_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 14 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PACIENTE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."PACIENTE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REG_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."REG_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 6 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SAL_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."SAL_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEG_CIR_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."SEG_CIR_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SIG_VIT_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."SIG_VIT_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SIN_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."SIN_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 9 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TERA_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."TERA_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TRA_MED_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "CLINICA"."TRA_MED_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
