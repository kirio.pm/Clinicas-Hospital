----------------------------------------CATALOGO DE DIAGNOSTICO--------------------------------------------------------------------------------------
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('1', 'A00', 'Colera', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('2', 'A01', 'Fiebre tifoidea y paratifoidea', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('3', 'A02', 'Otras Infecciones debidas a Salmonella', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('4', 'A03', 'Shigelosis', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('5', 'A04', 'Otras infecciones intestinales bacterianas', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('6', 'A05', 'Otras intoxicaciones alimentarias bacterianas', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('7', 'A06', 'Amebianisis', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('8', 'A07', 'Otras enfermedades intestinales debidas a protozoarios', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('9', 'A08', 'Infecciones intestinales debidas a virus y otros organismos especificos', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('10', 'A09', 'Diarrea y gastroenteritis de presunto origen infeccioso', 'Enfermedades Infecciosas Intestinales');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('11', 'A15', 'Tuberculosis respiratoria, confirmada bacteriol�gica e histol�gicamente', 'Tuberculosis');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('12', 'A16', 'Tuberculosis respiratoria, no confirmada bacteriol�gica o histol�gicamente', 'Tuberculosis');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('13', 'A17', 'Tuberculosis del sistema nervioso', 'Tuberculosis');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('14', 'A18', 'Tuberculosis de otros �rganos', 'Tuberculosis');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('15', 'A19', 'Tuberculosis miliar', 'Tuberculosis');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('16', 'B00', 'Infecciones herp�ticas [herpes simple]', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('17', 'B01', 'Varicela', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('18', 'B02', 'Herpes zoster', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('19', 'B03', 'Viruela', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('20', 'B04', 'Viruela de los monos', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('21', 'B05', 'Sarampi�n', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('22', 'B06', 'Rubeola [sarampi�n alem�n]', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('23', 'B07', 'Verrugas v�ricas', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('24', 'B08', 'Otras infecciones v�ricas caracterizadas por lesiones de la piel y de las membranas mucosas', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('25', 'B09', 'Infecci�n viral no especificada, caracterizada por lesiones de la piel y de las membranas mucosas', 'Infecciones virales caracterizadas por lesiones de la piel y de las membranas mucosas');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('26', 'B15', 'Hepatitis aguda tipo A', 'Hepatitis viral');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('27', 'B16', 'Hepatitis aguda tipo B', 'Hepatitis viral');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('28', 'B17', 'Otras hepatitis virales agudas', 'Hepatitis viral');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('29', 'B18', 'Hepatitis viral cr�nica', 'Hepatitis viral');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('30', 'B19', 'Hepatitis viral, sin otra especificaci�n', 'Hepatitis viral');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('31', 'C00', 'Tumor maligno del labio', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('32', 'C01', 'Tumor maligno de la base de la lengua', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('33', 'C02', 'Tumor maligno de otras partes y de las no especificadas de la lengua', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('34', 'C03', 'Tumor maligno de la enc�a', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('35', 'C04', 'Tumor maligno del piso de la boca', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('36', 'C05', 'Tumor maligno del paladar', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('37', 'C06', 'Tumor maligno de otras partes y de las no especificadas de la boca', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('38', 'C07', 'Tumor maligno de la gl�ndula par�tida', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('39', 'C08', 'Tumor maligno de otras gl�ndulas salivales mayores y de las no especificadas', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('40', 'C09', 'Tumor maligno de la am�gdala', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('41', 'C10', 'Tumor maligno de la orofaringe', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('42', 'C11', 'Tumor maligno de la nasofaringe', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('43', 'C12', 'Tumor maligno del seno piriforme', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('44', 'C13', 'Tumor maligno de la hipofaringe', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('45', 'C14', 'Tumor maligno de otros sitios y de los mal definidos del labio, de la cavidad bucal y de la faringe', 'Tumores malignos de labio de la cavidad bucal y de la faringe');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('46', 'C15', 'Tumor maligno del es�fago', 'Tumores malignos de los �rganos digestivos');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('47', 'C16', 'Tumor maligno del estomago', 'Tumores malignos de los �rganos digestivos');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('48', 'C17', 'Tumor maligno del intestino delgado', 'Tumores malignos de los �rganos digestivos');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('49', 'C18', 'Tumor maligno del colon', 'Tumores malignos de los �rganos digestivos');
INSERT INTO CATALOGODIAGNOSTICO (ID_CATA_DIAG, COD_ENF, NOMBRE_ENF, TIPO_ENF) VALUES ('50', 'C19', 'Tumor maligno de la uni�n rectosigmoidea', 'Tumores malignos de los �rganos digestivos');

--------------------------------------------------CATALOGO DE CIRUGIA---------------------------------------------------------------------------------------------

INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('1', '500', 'Amputacion de Pierna', 'Se amputara la pierna a la altura de la rodilla', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('2', '750', 'Extirpacion de amicdalas', 'Operacion que extraera amicdalas', 'Cirugia Menor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('3', '5000', 'Extraccion de cancer', 'Extracion de cancer de la zona afectada', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('4', '600', 'Colocacion de marcapaso', 'Se realizara una insicion a la altura de corazon para insertar un marcapaso', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('5', '200', 'Cirugia laser en los ojos', 'Se hara una correccion de la retina', 'Cirugia Menor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('6', '300', 'Cirugia de rodilla', 'Se repara el cartilago da�ana', 'Cirugia Menor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('7', '400', 'Transplante de ri�on', 'Se hara un insicion en el abdomen, para cambio de ri�on', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('8', '1500', 'Transplate de higado', 'Se hara una insicion en el abdomen, para cambio de higado', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('9', '1600', 'Repacion de fractura', 'Se pondra unos clavos para reparacion de fractura', 'Cirugia Menor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('10', '1200', 'Extraccion de apendice', 'Se realizara una incision en el abdomen para extirpara el apendice', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('11', '1000', 'Correcion de hernia', 'Se corregira el area afectada', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('12', '800', 'Extirpacion de aneurismo', 'Se hara una insicion en la cabeza donde se extirpara el aneurisma', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('13', '100', 'Bypass gastrico', 'Se hara una peque�a insicion en el abdomen para cortar el estomago', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('14', '5100', 'Transplante de corazon', 'Se hara una insicion a la altura del corazon para cambiar', 'Cirugia Mayor');
INSERT INTO CATALOGOCIRUGIA (ID_CAT_CIRU, COSTO_CIRUGIA, NOMBRE_CIRU, PROCEDIEMIENTO_CIRU, TIPO_CIRU) VALUES ('15', '540', 'Reparacion de hernia', '', 'Cirugia Mayor');


------------------------------------------CATALOGOCITA-------------------------------------------------------------------------------------------------------------

INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('1', 'General', 'Gerenal', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('2', 'Pediatria', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('3', 'Neurologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('4', 'Ginecologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('5', 'Dermatologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('6', 'Psicologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('7', 'Otorinalarigologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('8', ''Oftalmologia, 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('9', 'Nutricion', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('10', 'Gastroenterologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('11', 'Hematologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('12', 'Neumologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('13', 'Oncologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('14', 'Proctologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('15', 'Reumatologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('16', 'Traumatologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('17', 'Urologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('18', 'Alergologia', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('19', 'Geriatria', 'Especialidad', '1');
INSERT INTO CATALOGOCITAS (ID_CATALOGO_CITA, NOMBRE_CITA, TIPO_CITA, ID_CLINICA) VALUES ('20', 'Odontologia', 'Especialidad', '1');

-----------------------------------------CATALOGOCONSULTA---------------------------------------------------------------------------------------------------------

INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('1', 'C001', 'General' );
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('2', 'C002', 'Pediatria');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('3', 'C003', 'Neurologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('4', 'C004', 'Ginecologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('5', 'C005', 'Dermatologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('6', 'C006', 'Psicologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('7', 'C007', 'Otorrinolaringologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('8', 'C008', 'Oftalmologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('9', 'C009', 'Nutricion');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('10', 'C010', 'Gastroenterologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('11', 'C011', 'Hematologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('12', 'C012', 'Neumologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('13', 'C013', 'Oncologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('14', 'C014', 'Proctologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('15', 'C015', 'Reumatologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('16', 'C016', 'Traumatologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('17', 'C017', 'Urologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('18', 'C018', 'Alergologia');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('19', 'C019', 'Geriatria');
INSERT INTO CATALOGOCONSULTA (ID_CAT_CON, COD_CON, TIPO_CON) VALUES ('20', 'C020', 'Odontologia');

-------------------------------------------------CATALOGODEEXAMENES-----------------------------------------------------------------------------------------------

INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('1', 'EF001', '20', 'Rayos X', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('2', 'EF002', '30', 'Mamografia', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('3', 'EF003', '40', 'Electrocardiograma', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('4', 'EF004', '50', 'Resonancia magnetica', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('5', 'EF005', '10', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('6', 'EF006', '12', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('7', 'EF007', '21', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('8', 'EF008', '25', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('9', 'EF009', '5', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('10', 'EQ010', '21', '', 'Fisico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('11', 'EQ001', '15', 'Sangre', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('12', 'EQ002', '10', 'Heces', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('13', 'EQ003', '22', 'Orina', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('14', 'EQ004', '14', 'ADN', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('15', 'EQ005', '41', 'Bacterelogico', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('16', 'EQ006', '35', 'Cultivo', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('17', 'EQ007', '13', 'Parasitos', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('18', 'EQ008', '24', 'Glucosa', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('19', 'EQ009', '26', 'Hemoglobina', 'Quimico');
INSERT INTO CATALOGODEEXAMENES (ID_CAT_EXAMEN, COD_EXA, COSTO_EXAMEN, NOMBRE_EXAMEN, TIPO_EXAMEN) VALUES ('20', 'EQ010', '16', 'Biopsia', 'Quimico');

------------------------------------------------------CATALOGOMEDICAMENTO--------------------------------------------------------------------------------------

INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('1', '20', 'Metronidasol', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('2', '5,50', 'Omeprasol', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('3', '3', 'Omeprasol', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('4', '20', 'Raniditidina', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('5', '20', 'Raniditidina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('6', '20', 'Atropina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('7', '20', 'Metroclopramina', 'Comprimido');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('8', '20', 'Metroclopramina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('9', '20', 'Lactulosa', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('10', '20', 'Insulina', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('11', '20', 'Acetaminofen', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('12', '20', 'Penicilina', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('13', '20', 'Cofal', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('14', '20', 'Dopamina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('15', '20', 'Epinefrina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('16', '20', 'Paracetamol', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('17', '20', 'Neoban', 'Crema');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('18', '20', 'Clorimazol', 'Crema');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('19', '20', 'Oxitocina', 'Inyectable');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('20', '20', 'Amoxilina', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('21', '20', 'Suero', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('22', '20', 'Purgante', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('23', '20', 'Dicodin', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('24', '20', 'Anfrosol', 'Oral');
INSERT INTO CATALOGOMEDICAMENTO (ID_CAT_MEDI, COSTO_MED, NOMBRE_MEDI, TIPO_MEDI) VALUES ('25', '20', 'Salbutamol', 'Oral');

-------------------------------------------------------CATALOGOTERAPIA--------------------------------------------------------------------------------------

INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('1', 'T001', '5', 'Repiratoria', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('2', 'T002', '10', 'Aromaterapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('3', 'T003', '15', 'Fitoterapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('4', 'T004', '20', 'Geotrarapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('5', 'T005', '25', 'Orinoterapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('6', 'T006', '11', 'Hipnosis', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('7', 'T007', '16', 'Linfoterapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('8', 'T008', '21', 'Kimioterapia', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('9', 'T009', '26', 'Drenaje Linfatico', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('10', 'T010', '7', 'Terapia celular', 'Quimica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('11', 'T011', '13', 'Apulcutura', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('12', 'T012', '17', 'Risoterapia', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('13', 'T013', '24', 'Yoga', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('14', 'T014', '29', 'Masaje', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('15', 'T015', '80', 'Meditacion', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('16', 'T016', '10', 'Cantoterapia', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('17', 'T017', '15', 'Danzaterapia', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('18', 'T018', '35', 'Gestion del estres', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('19', 'T019', '45', 'Fonoterapia', 'Fisica');
INSERT INTO CATALOGOTERAPIA (ID_CAT_TERA, CODIGO_TERA, COSTO_TERA, NOMBRE_TERA, TIPO_TERA) VALUES ('20', 'T020', '50', 'Masaje chino', 'Fisica');
