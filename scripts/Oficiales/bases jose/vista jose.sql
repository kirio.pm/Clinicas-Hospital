CREATE OR REPLACE FORCE VIEW "CLINICA"."VISTA_TRATAMIENTO" ("COD_EXPEDIENTE", "FECHA_CON", "TIPO_CON", "NOMBRE_ENF", "FRECUENCIA_TRA", "TIPO_TRA") AS 
  SELECT 
    Ex.Cod_Expediente,Consu.Fecha_Con,Catcon.Tipo_Con,Catdi.Nombre_Enf,
Tra.Frecuencia_Tra,Tra.Tipo_Tra
FROM 
    Expedienteclinico ex, Consultamedica consu,Catalogoconsulta catcon,Diagnostico diag,
Catalogodiagnostico catdi,Tratamientomedico tra

where Ex.Id_Paciente=Consu.Id_Expediente and
      Consu.Id_Consulta=Diag.Id_Consulta and
      Diag.Id_Diagnostico=Tra.Id_Diagnostico and
      Consu.Id_Cat_Con=Catcon.Id_Cat_Con and
      Diag.Id_Cata_Diag=Catdi.Id_Cata_Diag;